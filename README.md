# DEEP & WIDE RECOMMENDATIONS

**NI-MVI ZS 2021/22 - Semestral Work**  
*Bc. Juraj Kmec* (kmecjura@fit.cvut.cz)

---

## Assignment

Dive into the algorithm, research practical usages/examples, decrypt what “features” are typically used (explainability), evaluate performance on selected dataset.

## Dataset

This project uses the *MovieLens 100K* dataset publicly available at <https://grouplens.org/datasets/movielens/100k/>. It contains 100 000 ratings of movies made by 1000 users and was released in 1998.

All the used data files are in the `data` folder, along with pre-split preprocessed training, validation, and testing sets. All the data preprocessing steps (including splitting) are done in the `process_data.ipynb` notebook.

## Models

All the neural network architectures and subsequent training is in the `model.ipynb` notebook. I have implemented three models:

- Deep Model - Classic MLP network
- Wide Model - A linear model that learns the correlations between movie rankings
- Deep & Wide Model - A combination of both previous models

The exported pretrained models are in the `models` directory.  
Figures of the architecture graphs are in the `img` directory.

## Example Recommendations

A demonstration of the recommendations for a sample batch of users is in the `recommend.ipynb` notebook. It also contains a simple retrieval system that generates candidate movies based on the user's history of liked movies.

The notebook uses the pretrained networks from `models` directory. It displays the occupation, gender, and age of the user, their previously liked movies and displays the top 20 recommendationded movies (the ones the user is mostly likely to enjoy) for each of the trained networks.

## Miscellaneous Files

The file `utils.py` defines various constants used in `model.ipynb` and `recommend.ipynb` that describe the types of columns or the number of distinct values in categorical columns. It also defines a utility function that reads the CSV datasets into a `tf.Tensor`.

The file `vocabulary.json` contains the distinct values for each of the categorical features. This is used for embedding said columns.

## Dependencies

This project only uses standard Python tools and Machine Learning libraries:

- The Python Standard Library
- NumPy
- Pandas
- Matplotlib + Seaborn
- TensorFlow + Keras
- Scikit-learn

Additionally, the Scikit-learn extension for Intel CPUs is used. However, it does not provide any extra functionality and can be commented out in case of problems.
