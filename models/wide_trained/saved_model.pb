═▀
иЏ
B
AssignVariableOp
resource
value"dtype"
dtypetypeѕ
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype
W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(ѕ

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
e
Range
start"Tidx
limit"Tidx
delta"Tidx
output"Tidx"
Tidxtype0:
2		
@
ReadVariableOp
resource
value"dtype"
dtypetypeѕ
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0ѕ
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0ѕ
s
	ScatterNd
indices"Tindices
updates"T
shape"Tindices
output"T"	
Ttype"
Tindicestype:
2	
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
N
Squeeze

input"T
output"T"	
Ttype"
squeeze_dims	list(int)
 (
Й
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ѕ
@
StaticRegexFullMatch	
input

output
"
patternstring
Ш
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
ќ
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 ѕ"serve*2.6.02unknown8лЂ
{
dense_5/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:─ог*
shared_namedense_5/kernel
t
"dense_5/kernel/Read/ReadVariableOpReadVariableOpdense_5/kernel*!
_output_shapes
:─ог*
dtype0
p
dense_5/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_5/bias
i
 dense_5/bias/Read/ReadVariableOpReadVariableOpdense_5/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
u
true_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:╚*
shared_nametrue_positives
n
"true_positives/Read/ReadVariableOpReadVariableOptrue_positives*
_output_shapes	
:╚*
dtype0
u
true_negativesVarHandleOp*
_output_shapes
: *
dtype0*
shape:╚*
shared_nametrue_negatives
n
"true_negatives/Read/ReadVariableOpReadVariableOptrue_negatives*
_output_shapes	
:╚*
dtype0
w
false_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:╚* 
shared_namefalse_positives
p
#false_positives/Read/ReadVariableOpReadVariableOpfalse_positives*
_output_shapes	
:╚*
dtype0
w
false_negativesVarHandleOp*
_output_shapes
: *
dtype0*
shape:╚* 
shared_namefalse_negatives
p
#false_negatives/Read/ReadVariableOpReadVariableOpfalse_negatives*
_output_shapes	
:╚*
dtype0
Ѕ
Adam/dense_5/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:─ог*&
shared_nameAdam/dense_5/kernel/m
ѓ
)Adam/dense_5/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_5/kernel/m*!
_output_shapes
:─ог*
dtype0
~
Adam/dense_5/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_5/bias/m
w
'Adam/dense_5/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_5/bias/m*
_output_shapes
:*
dtype0
Ѕ
Adam/dense_5/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:─ог*&
shared_nameAdam/dense_5/kernel/v
ѓ
)Adam/dense_5/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_5/kernel/v*!
_output_shapes
:─ог*
dtype0
~
Adam/dense_5/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_5/bias/v
w
'Adam/dense_5/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_5/bias/v*
_output_shapes
:*
dtype0

NoOpNoOp
ё
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*┐
valueхB▓ BФ
╠
layer-0
layer-1
layer-2
layer-3
layer_with_weights-0
layer-4
	optimizer
	variables
trainable_variables
	regularization_losses

	keras_api

signatures
 

	keras_api
 
R
	variables
regularization_losses
trainable_variables
	keras_api
h

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
d
iter

beta_1

beta_2
	decay
learning_ratem=m>v?v@

0
1

0
1
 
Г
non_trainable_variables
layer_regularization_losses
	variables
metrics
layer_metrics
trainable_variables
	regularization_losses

 layers
 
 
 
 
 
Г
!non_trainable_variables
"layer_regularization_losses
	variables
#metrics
regularization_losses
trainable_variables
$layer_metrics

%layers
ZX
VARIABLE_VALUEdense_5/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_5/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
Г
&non_trainable_variables
'layer_regularization_losses
	variables
(metrics
regularization_losses
trainable_variables
)layer_metrics

*layers
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
 
 

+0
,1
-2
 
#
0
1
2
3
4
 
 
 
 
 
 
 
 
 
 
4
	.total
	/count
0	variables
1	keras_api
D
	2total
	3count
4
_fn_kwargs
5	variables
6	keras_api
p
7true_positives
8true_negatives
9false_positives
:false_negatives
;	variables
<	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

.0
/1

0	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

20
31

5	variables
a_
VARIABLE_VALUEtrue_positives=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEtrue_negatives=keras_api/metrics/2/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_positives>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_negatives>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUE

70
81
92
:3

;	variables
}{
VARIABLE_VALUEAdam/dense_5/kernel/mRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_5/bias/mPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_5/kernel/vRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_5/bias/vPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
|
serving_default_historyPlaceholder*(
_output_shapes
:         њ*
dtype0*
shape:         њ
r
serving_default_item_idPlaceholder*#
_output_shapes
:         *
dtype0*
shape:         
­
StatefulPartitionedCallStatefulPartitionedCallserving_default_historyserving_default_item_iddense_5/kerneldense_5/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *,
f'R%
#__inference_signature_wrapper_76265
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
╔
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename"dense_5/kernel/Read/ReadVariableOp dense_5/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp"true_positives/Read/ReadVariableOp"true_negatives/Read/ReadVariableOp#false_positives/Read/ReadVariableOp#false_negatives/Read/ReadVariableOp)Adam/dense_5/kernel/m/Read/ReadVariableOp'Adam/dense_5/bias/m/Read/ReadVariableOp)Adam/dense_5/kernel/v/Read/ReadVariableOp'Adam/dense_5/bias/v/Read/ReadVariableOpConst* 
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8ѓ *'
f"R 
__inference__traced_save_76476
╚
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamedense_5/kerneldense_5/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1true_positivestrue_negativesfalse_positivesfalse_negativesAdam/dense_5/kernel/mAdam/dense_5/bias/mAdam/dense_5/kernel/vAdam/dense_5/bias/v*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8ѓ **
f%R#
!__inference__traced_restore_76543┬│
§R
Ж

!__inference__traced_restore_76543
file_prefix4
assignvariableop_dense_5_kernel:─ог-
assignvariableop_1_dense_5_bias:&
assignvariableop_2_adam_iter:	 (
assignvariableop_3_adam_beta_1: (
assignvariableop_4_adam_beta_2: '
assignvariableop_5_adam_decay: /
%assignvariableop_6_adam_learning_rate: "
assignvariableop_7_total: "
assignvariableop_8_count: $
assignvariableop_9_total_1: %
assignvariableop_10_count_1: 1
"assignvariableop_11_true_positives:	╚1
"assignvariableop_12_true_negatives:	╚2
#assignvariableop_13_false_positives:	╚2
#assignvariableop_14_false_negatives:	╚>
)assignvariableop_15_adam_dense_5_kernel_m:─ог5
'assignvariableop_16_adam_dense_5_bias_m:>
)assignvariableop_17_adam_dense_5_kernel_v:─ог5
'assignvariableop_18_adam_dense_5_bias_v:
identity_20ѕбAssignVariableOpбAssignVariableOp_1бAssignVariableOp_10бAssignVariableOp_11бAssignVariableOp_12бAssignVariableOp_13бAssignVariableOp_14бAssignVariableOp_15бAssignVariableOp_16бAssignVariableOp_17бAssignVariableOp_18бAssignVariableOp_2бAssignVariableOp_3бAssignVariableOp_4бAssignVariableOp_5бAssignVariableOp_6бAssignVariableOp_7бAssignVariableOp_8бAssignVariableOp_9ю

RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*е	
valueъ	BЏ	B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_namesХ
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*;
value2B0B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slicesЈ
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*d
_output_shapesR
P::::::::::::::::::::*"
dtypes
2	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identityъ
AssignVariableOpAssignVariableOpassignvariableop_dense_5_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1ц
AssignVariableOp_1AssignVariableOpassignvariableop_1_dense_5_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0	*
_output_shapes
:2

Identity_2А
AssignVariableOp_2AssignVariableOpassignvariableop_2_adam_iterIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3Б
AssignVariableOp_3AssignVariableOpassignvariableop_3_adam_beta_1Identity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4Б
AssignVariableOp_4AssignVariableOpassignvariableop_4_adam_beta_2Identity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5б
AssignVariableOp_5AssignVariableOpassignvariableop_5_adam_decayIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6ф
AssignVariableOp_6AssignVariableOp%assignvariableop_6_adam_learning_rateIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7Ю
AssignVariableOp_7AssignVariableOpassignvariableop_7_totalIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8Ю
AssignVariableOp_8AssignVariableOpassignvariableop_8_countIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9Ъ
AssignVariableOp_9AssignVariableOpassignvariableop_9_total_1Identity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10Б
AssignVariableOp_10AssignVariableOpassignvariableop_10_count_1Identity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11ф
AssignVariableOp_11AssignVariableOp"assignvariableop_11_true_positivesIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12ф
AssignVariableOp_12AssignVariableOp"assignvariableop_12_true_negativesIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13Ф
AssignVariableOp_13AssignVariableOp#assignvariableop_13_false_positivesIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14Ф
AssignVariableOp_14AssignVariableOp#assignvariableop_14_false_negativesIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15▒
AssignVariableOp_15AssignVariableOp)assignvariableop_15_adam_dense_5_kernel_mIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16»
AssignVariableOp_16AssignVariableOp'assignvariableop_16_adam_dense_5_bias_mIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17▒
AssignVariableOp_17AssignVariableOp)assignvariableop_17_adam_dense_5_kernel_vIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18»
AssignVariableOp_18AssignVariableOp'assignvariableop_18_adam_dense_5_bias_vIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_189
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOpђ
Identity_19Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_19f
Identity_20IdentityIdentity_19:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_20У
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_20Identity_20:output:0*;
_input_shapes*
(: : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
ј
Ш
B__inference_dense_5_layer_call_and_return_conditional_losses_76386

inputs3
matmul_readvariableop_resource:─ог-
biasadd_readvariableop_resource:
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpљ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*!
_output_shapes
:─ог*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:         2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:         2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:         ─ог: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:R N
*
_output_shapes
:         ─ог
 
_user_specified_nameinputs
╣
¤
?__inference_wide_layer_call_and_return_conditional_losses_76247
history
item_id"
dense_5_76241:─ог
dense_5_76243:
identityѕбdense_5/StatefulPartitionedCallЇ
tf.expand_dims_4/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2!
tf.expand_dims_4/ExpandDims/dimГ
tf.expand_dims_4/ExpandDims
ExpandDimsitem_id(tf.expand_dims_4/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2
tf.expand_dims_4/ExpandDimsљ
cross_product/PartitionedCallPartitionedCall$tf.expand_dims_4/ExpandDims:output:0history*
Tin
2*
Tout
2*
_collective_manager_ids
 **
_output_shapes
:         ─ог* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8ѓ *Q
fLRJ
H__inference_cross_product_layer_call_and_return_conditional_losses_761342
cross_product/PartitionedCall»
dense_5/StatefulPartitionedCallStatefulPartitionedCall&cross_product/PartitionedCall:output:0dense_5_76241dense_5_76243*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *K
fFRD
B__inference_dense_5_layer_call_and_return_conditional_losses_761472!
dense_5/StatefulPartitionedCallЃ
IdentityIdentity(dense_5/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityp
NoOpNoOp ^dense_5/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall:Q M
(
_output_shapes
:         њ
!
_user_specified_name	history:LH
#
_output_shapes
:         
!
_user_specified_name	item_id
К/
Н
__inference__traced_save_76476
file_prefix-
)savev2_dense_5_kernel_read_readvariableop+
'savev2_dense_5_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop-
)savev2_true_positives_read_readvariableop-
)savev2_true_negatives_read_readvariableop.
*savev2_false_positives_read_readvariableop.
*savev2_false_negatives_read_readvariableop4
0savev2_adam_dense_5_kernel_m_read_readvariableop2
.savev2_adam_dense_5_bias_m_read_readvariableop4
0savev2_adam_dense_5_kernel_v_read_readvariableop2
.savev2_adam_dense_5_bias_v_read_readvariableop
savev2_const

identity_1ѕбMergeV2CheckpointsЈ
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1І
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardд
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameќ

SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*е	
valueъ	BЏ	B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names░
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*;
value2B0B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slicesв
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0)savev2_dense_5_kernel_read_readvariableop'savev2_dense_5_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop)savev2_true_positives_read_readvariableop)savev2_true_negatives_read_readvariableop*savev2_false_positives_read_readvariableop*savev2_false_negatives_read_readvariableop0savev2_adam_dense_5_kernel_m_read_readvariableop.savev2_adam_dense_5_bias_m_read_readvariableop0savev2_adam_dense_5_kernel_v_read_readvariableop.savev2_adam_dense_5_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *"
dtypes
2	2
SaveV2║
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesА
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*~
_input_shapesm
k: :─ог:: : : : : : : : : :╚:╚:╚:╚:─ог::─ог:: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:'#
!
_output_shapes
:─ог: 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :!

_output_shapes	
:╚:!

_output_shapes	
:╚:!

_output_shapes	
:╚:!

_output_shapes	
:╚:'#
!
_output_shapes
:─ог: 

_output_shapes
::'#
!
_output_shapes
:─ог: 

_output_shapes
::

_output_shapes
: 
љ	
░
$__inference_wide_layer_call_fn_76347
inputs_history
inputs_item_id
unknown:─ог
	unknown_0:
identityѕбStatefulPartitionedCallІ
StatefulPartitionedCallStatefulPartitionedCallinputs_historyinputs_item_idunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *H
fCRA
?__inference_wide_layer_call_and_return_conditional_losses_762042
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 22
StatefulPartitionedCallStatefulPartitionedCall:X T
(
_output_shapes
:         њ
(
_user_specified_nameinputs/history:SO
#
_output_shapes
:         
(
_user_specified_nameinputs/item_id
Т
б
$__inference_wide_layer_call_fn_76221
history
item_id
unknown:─ог
	unknown_0:
identityѕбStatefulPartitionedCall§
StatefulPartitionedCallStatefulPartitionedCallhistoryitem_idunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *H
fCRA
?__inference_wide_layer_call_and_return_conditional_losses_762042
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 22
StatefulPartitionedCallStatefulPartitionedCall:Q M
(
_output_shapes
:         њ
!
_user_specified_name	history:LH
#
_output_shapes
:         
!
_user_specified_name	item_id
├
t
H__inference_cross_product_layer_call_and_return_conditional_losses_76369
inputs_0
inputs_1
identityF
ShapeShapeinputs_0*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2Р
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
range/startConst*
_output_shapes
: *
dtype0*
value	B : 2
range/start\
range/deltaConst*
_output_shapes
: *
dtype0*
value	B :2
range/deltaђ
rangeRangerange/start:output:0strided_slice:output:0range/delta:output:0*#
_output_shapes
:         2
rangeJ
SqueezeSqueezeinputs_0*
T0*
_output_shapes
:2	
Squeeze
stackPackrange:output:0Squeeze:output:0*
N*
T0*'
_output_shapes
:         *

axis2
stacki
ScatterNd/shape/1Const*
_output_shapes
: *
dtype0*
value
B :њ2
ScatterNd/shape/1i
ScatterNd/shape/2Const*
_output_shapes
: *
dtype0*
value
B :њ2
ScatterNd/shape/2е
ScatterNd/shapePackstrided_slice:output:0ScatterNd/shape/1:output:0ScatterNd/shape/2:output:0*
N*
T0*
_output_shapes
:2
ScatterNd/shapeЪ
	ScatterNd	ScatterNdstack:output:0inputs_1ScatterNd/shape:output:0*
T0*
Tindices0*-
_output_shapes
:         њњ2
	ScatterNdg
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
valueB
 :─ог2
Reshape/shape/1є
Reshape/shapePackstrided_slice:output:0Reshape/shape/1:output:0*
N*
T0*
_output_shapes
:2
Reshape/shape~
ReshapeReshapeScatterNd:output:0Reshape/shape:output:0*
T0**
_output_shapes
:         ─ог2	
Reshapeg
IdentityIdentityReshape:output:0*
T0**
_output_shapes
:         ─ог2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         :         њ:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:         њ
"
_user_specified_name
inputs/1
ј
Ш
B__inference_dense_5_layer_call_and_return_conditional_losses_76147

inputs3
matmul_readvariableop_resource:─ог-
biasadd_readvariableop_resource:
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpљ
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*!
_output_shapes
:─ог*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
MatMulї
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOpЂ
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:         2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:         2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:         ─ог: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:R N
*
_output_shapes
:         ─ог
 
_user_specified_nameinputs
и
¤
?__inference_wide_layer_call_and_return_conditional_losses_76154

inputs
inputs_1"
dense_5_76148:─ог
dense_5_76150:
identityѕбdense_5/StatefulPartitionedCallЇ
tf.expand_dims_4/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2!
tf.expand_dims_4/ExpandDims/dim«
tf.expand_dims_4/ExpandDims
ExpandDimsinputs_1(tf.expand_dims_4/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2
tf.expand_dims_4/ExpandDimsЈ
cross_product/PartitionedCallPartitionedCall$tf.expand_dims_4/ExpandDims:output:0inputs*
Tin
2*
Tout
2*
_collective_manager_ids
 **
_output_shapes
:         ─ог* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8ѓ *Q
fLRJ
H__inference_cross_product_layer_call_and_return_conditional_losses_761342
cross_product/PartitionedCall»
dense_5/StatefulPartitionedCallStatefulPartitionedCall&cross_product/PartitionedCall:output:0dense_5_76148dense_5_76150*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *K
fFRD
B__inference_dense_5_layer_call_and_return_conditional_losses_761472!
dense_5/StatefulPartitionedCallЃ
IdentityIdentity(dense_5/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityp
NoOpNoOp ^dense_5/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall:P L
(
_output_shapes
:         њ
 
_user_specified_nameinputs:KG
#
_output_shapes
:         
 
_user_specified_nameinputs
к
А
#__inference_signature_wrapper_76265
history
item_id
unknown:─ог
	unknown_0:
identityѕбStatefulPartitionedCallя
StatefulPartitionedCallStatefulPartitionedCallhistoryitem_idunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *)
f$R"
 __inference__wrapped_model_761012
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 22
StatefulPartitionedCallStatefulPartitionedCall:Q M
(
_output_shapes
:         њ
!
_user_specified_name	history:LH
#
_output_shapes
:         
!
_user_specified_name	item_id
╣
r
H__inference_cross_product_layer_call_and_return_conditional_losses_76134

inputs
inputs_1
identityD
ShapeShapeinputs*
T0*
_output_shapes
:2
Shapet
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2
strided_slice/stackx
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_1x
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2
strided_slice/stack_2Р
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
strided_slice\
range/startConst*
_output_shapes
: *
dtype0*
value	B : 2
range/start\
range/deltaConst*
_output_shapes
: *
dtype0*
value	B :2
range/deltaђ
rangeRangerange/start:output:0strided_slice:output:0range/delta:output:0*#
_output_shapes
:         2
rangeH
SqueezeSqueezeinputs*
T0*
_output_shapes
:2	
Squeeze
stackPackrange:output:0Squeeze:output:0*
N*
T0*'
_output_shapes
:         *

axis2
stacki
ScatterNd/shape/1Const*
_output_shapes
: *
dtype0*
value
B :њ2
ScatterNd/shape/1i
ScatterNd/shape/2Const*
_output_shapes
: *
dtype0*
value
B :њ2
ScatterNd/shape/2е
ScatterNd/shapePackstrided_slice:output:0ScatterNd/shape/1:output:0ScatterNd/shape/2:output:0*
N*
T0*
_output_shapes
:2
ScatterNd/shapeЪ
	ScatterNd	ScatterNdstack:output:0inputs_1ScatterNd/shape:output:0*
T0*
Tindices0*-
_output_shapes
:         њњ2
	ScatterNdg
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
valueB
 :─ог2
Reshape/shape/1є
Reshape/shapePackstrided_slice:output:0Reshape/shape/1:output:0*
N*
T0*
_output_shapes
:2
Reshape/shape~
ReshapeReshapeScatterNd:output:0Reshape/shape:output:0*
T0**
_output_shapes
:         ─ог2	
Reshapeg
IdentityIdentityReshape:output:0*
T0**
_output_shapes
:         ─ог2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         :         њ:O K
'
_output_shapes
:         
 
_user_specified_nameinputs:PL
(
_output_shapes
:         њ
 
_user_specified_nameinputs
╣
¤
?__inference_wide_layer_call_and_return_conditional_losses_76234
history
item_id"
dense_5_76228:─ог
dense_5_76230:
identityѕбdense_5/StatefulPartitionedCallЇ
tf.expand_dims_4/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2!
tf.expand_dims_4/ExpandDims/dimГ
tf.expand_dims_4/ExpandDims
ExpandDimsitem_id(tf.expand_dims_4/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2
tf.expand_dims_4/ExpandDimsљ
cross_product/PartitionedCallPartitionedCall$tf.expand_dims_4/ExpandDims:output:0history*
Tin
2*
Tout
2*
_collective_manager_ids
 **
_output_shapes
:         ─ог* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8ѓ *Q
fLRJ
H__inference_cross_product_layer_call_and_return_conditional_losses_761342
cross_product/PartitionedCall»
dense_5/StatefulPartitionedCallStatefulPartitionedCall&cross_product/PartitionedCall:output:0dense_5_76228dense_5_76230*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *K
fFRD
B__inference_dense_5_layer_call_and_return_conditional_losses_761472!
dense_5/StatefulPartitionedCallЃ
IdentityIdentity(dense_5/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityp
NoOpNoOp ^dense_5/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall:Q M
(
_output_shapes
:         њ
!
_user_specified_name	history:LH
#
_output_shapes
:         
!
_user_specified_name	item_id
Ъ'
»
?__inference_wide_layer_call_and_return_conditional_losses_76296
inputs_history
inputs_item_id;
&dense_5_matmul_readvariableop_resource:─ог5
'dense_5_biasadd_readvariableop_resource:
identityѕбdense_5/BiasAdd/ReadVariableOpбdense_5/MatMul/ReadVariableOpЇ
tf.expand_dims_4/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2!
tf.expand_dims_4/ExpandDims/dim┤
tf.expand_dims_4/ExpandDims
ExpandDimsinputs_item_id(tf.expand_dims_4/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2
tf.expand_dims_4/ExpandDims~
cross_product/ShapeShape$tf.expand_dims_4/ExpandDims:output:0*
T0*
_output_shapes
:2
cross_product/Shapeљ
!cross_product/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2#
!cross_product/strided_slice/stackћ
#cross_product/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2%
#cross_product/strided_slice/stack_1ћ
#cross_product/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2%
#cross_product/strided_slice/stack_2Х
cross_product/strided_sliceStridedSlicecross_product/Shape:output:0*cross_product/strided_slice/stack:output:0,cross_product/strided_slice/stack_1:output:0,cross_product/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
cross_product/strided_slicex
cross_product/range/startConst*
_output_shapes
: *
dtype0*
value	B : 2
cross_product/range/startx
cross_product/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :2
cross_product/range/deltaк
cross_product/rangeRange"cross_product/range/start:output:0$cross_product/strided_slice:output:0"cross_product/range/delta:output:0*#
_output_shapes
:         2
cross_product/rangeѓ
cross_product/SqueezeSqueeze$tf.expand_dims_4/ExpandDims:output:0*
T0*
_output_shapes
:2
cross_product/Squeezeи
cross_product/stackPackcross_product/range:output:0cross_product/Squeeze:output:0*
N*
T0*'
_output_shapes
:         *

axis2
cross_product/stackЁ
cross_product/ScatterNd/shape/1Const*
_output_shapes
: *
dtype0*
value
B :њ2!
cross_product/ScatterNd/shape/1Ё
cross_product/ScatterNd/shape/2Const*
_output_shapes
: *
dtype0*
value
B :њ2!
cross_product/ScatterNd/shape/2Ь
cross_product/ScatterNd/shapePack$cross_product/strided_slice:output:0(cross_product/ScatterNd/shape/1:output:0(cross_product/ScatterNd/shape/2:output:0*
N*
T0*
_output_shapes
:2
cross_product/ScatterNd/shapeП
cross_product/ScatterNd	ScatterNdcross_product/stack:output:0inputs_history&cross_product/ScatterNd/shape:output:0*
T0*
Tindices0*-
_output_shapes
:         њњ2
cross_product/ScatterNdЃ
cross_product/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
valueB
 :─ог2
cross_product/Reshape/shape/1Й
cross_product/Reshape/shapePack$cross_product/strided_slice:output:0&cross_product/Reshape/shape/1:output:0*
N*
T0*
_output_shapes
:2
cross_product/Reshape/shapeХ
cross_product/ReshapeReshape cross_product/ScatterNd:output:0$cross_product/Reshape/shape:output:0*
T0**
_output_shapes
:         ─ог2
cross_product/Reshapeе
dense_5/MatMul/ReadVariableOpReadVariableOp&dense_5_matmul_readvariableop_resource*!
_output_shapes
:─ог*
dtype02
dense_5/MatMul/ReadVariableOpБ
dense_5/MatMulMatMulcross_product/Reshape:output:0%dense_5/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_5/MatMulц
dense_5/BiasAdd/ReadVariableOpReadVariableOp'dense_5_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02 
dense_5/BiasAdd/ReadVariableOpА
dense_5/BiasAddBiasAdddense_5/MatMul:product:0&dense_5/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_5/BiasAddy
dense_5/SigmoidSigmoiddense_5/BiasAdd:output:0*
T0*'
_output_shapes
:         2
dense_5/Sigmoidn
IdentityIdentitydense_5/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:         2

IdentityЈ
NoOpNoOp^dense_5/BiasAdd/ReadVariableOp^dense_5/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 2@
dense_5/BiasAdd/ReadVariableOpdense_5/BiasAdd/ReadVariableOp2>
dense_5/MatMul/ReadVariableOpdense_5/MatMul/ReadVariableOp:X T
(
_output_shapes
:         њ
(
_user_specified_nameinputs/history:SO
#
_output_shapes
:         
(
_user_specified_nameinputs/item_id
и
¤
?__inference_wide_layer_call_and_return_conditional_losses_76204

inputs
inputs_1"
dense_5_76198:─ог
dense_5_76200:
identityѕбdense_5/StatefulPartitionedCallЇ
tf.expand_dims_4/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2!
tf.expand_dims_4/ExpandDims/dim«
tf.expand_dims_4/ExpandDims
ExpandDimsinputs_1(tf.expand_dims_4/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2
tf.expand_dims_4/ExpandDimsЈ
cross_product/PartitionedCallPartitionedCall$tf.expand_dims_4/ExpandDims:output:0inputs*
Tin
2*
Tout
2*
_collective_manager_ids
 **
_output_shapes
:         ─ог* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8ѓ *Q
fLRJ
H__inference_cross_product_layer_call_and_return_conditional_losses_761342
cross_product/PartitionedCall»
dense_5/StatefulPartitionedCallStatefulPartitionedCall&cross_product/PartitionedCall:output:0dense_5_76198dense_5_76200*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *K
fFRD
B__inference_dense_5_layer_call_and_return_conditional_losses_761472!
dense_5/StatefulPartitionedCallЃ
IdentityIdentity(dense_5/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityp
NoOpNoOp ^dense_5/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 2B
dense_5/StatefulPartitionedCalldense_5/StatefulPartitionedCall:P L
(
_output_shapes
:         њ
 
_user_specified_nameinputs:KG
#
_output_shapes
:         
 
_user_specified_nameinputs
Ъ'
»
?__inference_wide_layer_call_and_return_conditional_losses_76327
inputs_history
inputs_item_id;
&dense_5_matmul_readvariableop_resource:─ог5
'dense_5_biasadd_readvariableop_resource:
identityѕбdense_5/BiasAdd/ReadVariableOpбdense_5/MatMul/ReadVariableOpЇ
tf.expand_dims_4/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2!
tf.expand_dims_4/ExpandDims/dim┤
tf.expand_dims_4/ExpandDims
ExpandDimsinputs_item_id(tf.expand_dims_4/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2
tf.expand_dims_4/ExpandDims~
cross_product/ShapeShape$tf.expand_dims_4/ExpandDims:output:0*
T0*
_output_shapes
:2
cross_product/Shapeљ
!cross_product/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2#
!cross_product/strided_slice/stackћ
#cross_product/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2%
#cross_product/strided_slice/stack_1ћ
#cross_product/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2%
#cross_product/strided_slice/stack_2Х
cross_product/strided_sliceStridedSlicecross_product/Shape:output:0*cross_product/strided_slice/stack:output:0,cross_product/strided_slice/stack_1:output:0,cross_product/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2
cross_product/strided_slicex
cross_product/range/startConst*
_output_shapes
: *
dtype0*
value	B : 2
cross_product/range/startx
cross_product/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :2
cross_product/range/deltaк
cross_product/rangeRange"cross_product/range/start:output:0$cross_product/strided_slice:output:0"cross_product/range/delta:output:0*#
_output_shapes
:         2
cross_product/rangeѓ
cross_product/SqueezeSqueeze$tf.expand_dims_4/ExpandDims:output:0*
T0*
_output_shapes
:2
cross_product/Squeezeи
cross_product/stackPackcross_product/range:output:0cross_product/Squeeze:output:0*
N*
T0*'
_output_shapes
:         *

axis2
cross_product/stackЁ
cross_product/ScatterNd/shape/1Const*
_output_shapes
: *
dtype0*
value
B :њ2!
cross_product/ScatterNd/shape/1Ё
cross_product/ScatterNd/shape/2Const*
_output_shapes
: *
dtype0*
value
B :њ2!
cross_product/ScatterNd/shape/2Ь
cross_product/ScatterNd/shapePack$cross_product/strided_slice:output:0(cross_product/ScatterNd/shape/1:output:0(cross_product/ScatterNd/shape/2:output:0*
N*
T0*
_output_shapes
:2
cross_product/ScatterNd/shapeП
cross_product/ScatterNd	ScatterNdcross_product/stack:output:0inputs_history&cross_product/ScatterNd/shape:output:0*
T0*
Tindices0*-
_output_shapes
:         њњ2
cross_product/ScatterNdЃ
cross_product/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
valueB
 :─ог2
cross_product/Reshape/shape/1Й
cross_product/Reshape/shapePack$cross_product/strided_slice:output:0&cross_product/Reshape/shape/1:output:0*
N*
T0*
_output_shapes
:2
cross_product/Reshape/shapeХ
cross_product/ReshapeReshape cross_product/ScatterNd:output:0$cross_product/Reshape/shape:output:0*
T0**
_output_shapes
:         ─ог2
cross_product/Reshapeе
dense_5/MatMul/ReadVariableOpReadVariableOp&dense_5_matmul_readvariableop_resource*!
_output_shapes
:─ог*
dtype02
dense_5/MatMul/ReadVariableOpБ
dense_5/MatMulMatMulcross_product/Reshape:output:0%dense_5/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_5/MatMulц
dense_5/BiasAdd/ReadVariableOpReadVariableOp'dense_5_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02 
dense_5/BiasAdd/ReadVariableOpА
dense_5/BiasAddBiasAdddense_5/MatMul:product:0&dense_5/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
dense_5/BiasAddy
dense_5/SigmoidSigmoiddense_5/BiasAdd:output:0*
T0*'
_output_shapes
:         2
dense_5/Sigmoidn
IdentityIdentitydense_5/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:         2

IdentityЈ
NoOpNoOp^dense_5/BiasAdd/ReadVariableOp^dense_5/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 2@
dense_5/BiasAdd/ReadVariableOpdense_5/BiasAdd/ReadVariableOp2>
dense_5/MatMul/ReadVariableOpdense_5/MatMul/ReadVariableOp:X T
(
_output_shapes
:         њ
(
_user_specified_nameinputs/history:SO
#
_output_shapes
:         
(
_user_specified_nameinputs/item_id
щ
Ќ
'__inference_dense_5_layer_call_fn_76395

inputs
unknown:─ог
	unknown_0:
identityѕбStatefulPartitionedCallш
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *K
fFRD
B__inference_dense_5_layer_call_and_return_conditional_losses_761472
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:         ─ог: : 22
StatefulPartitionedCallStatefulPartitionedCall:R N
*
_output_shapes
:         ─ог
 
_user_specified_nameinputs
Ї*
ќ
 __inference__wrapped_model_76101
history
item_id@
+wide_dense_5_matmul_readvariableop_resource:─ог:
,wide_dense_5_biasadd_readvariableop_resource:
identityѕб#wide/dense_5/BiasAdd/ReadVariableOpб"wide/dense_5/MatMul/ReadVariableOpЌ
$wide/tf.expand_dims_4/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
         2&
$wide/tf.expand_dims_4/ExpandDims/dim╝
 wide/tf.expand_dims_4/ExpandDims
ExpandDimsitem_id-wide/tf.expand_dims_4/ExpandDims/dim:output:0*
T0*'
_output_shapes
:         2"
 wide/tf.expand_dims_4/ExpandDimsЇ
wide/cross_product/ShapeShape)wide/tf.expand_dims_4/ExpandDims:output:0*
T0*
_output_shapes
:2
wide/cross_product/Shapeџ
&wide/cross_product/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 2(
&wide/cross_product/strided_slice/stackъ
(wide/cross_product/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:2*
(wide/cross_product/strided_slice/stack_1ъ
(wide/cross_product/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:2*
(wide/cross_product/strided_slice/stack_2н
 wide/cross_product/strided_sliceStridedSlice!wide/cross_product/Shape:output:0/wide/cross_product/strided_slice/stack:output:01wide/cross_product/strided_slice/stack_1:output:01wide/cross_product/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask2"
 wide/cross_product/strided_sliceѓ
wide/cross_product/range/startConst*
_output_shapes
: *
dtype0*
value	B : 2 
wide/cross_product/range/startѓ
wide/cross_product/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :2 
wide/cross_product/range/delta▀
wide/cross_product/rangeRange'wide/cross_product/range/start:output:0)wide/cross_product/strided_slice:output:0'wide/cross_product/range/delta:output:0*#
_output_shapes
:         2
wide/cross_product/rangeЉ
wide/cross_product/SqueezeSqueeze)wide/tf.expand_dims_4/ExpandDims:output:0*
T0*
_output_shapes
:2
wide/cross_product/Squeeze╦
wide/cross_product/stackPack!wide/cross_product/range:output:0#wide/cross_product/Squeeze:output:0*
N*
T0*'
_output_shapes
:         *

axis2
wide/cross_product/stackЈ
$wide/cross_product/ScatterNd/shape/1Const*
_output_shapes
: *
dtype0*
value
B :њ2&
$wide/cross_product/ScatterNd/shape/1Ј
$wide/cross_product/ScatterNd/shape/2Const*
_output_shapes
: *
dtype0*
value
B :њ2&
$wide/cross_product/ScatterNd/shape/2Є
"wide/cross_product/ScatterNd/shapePack)wide/cross_product/strided_slice:output:0-wide/cross_product/ScatterNd/shape/1:output:0-wide/cross_product/ScatterNd/shape/2:output:0*
N*
T0*
_output_shapes
:2$
"wide/cross_product/ScatterNd/shapeЖ
wide/cross_product/ScatterNd	ScatterNd!wide/cross_product/stack:output:0history+wide/cross_product/ScatterNd/shape:output:0*
T0*
Tindices0*-
_output_shapes
:         њњ2
wide/cross_product/ScatterNdЇ
"wide/cross_product/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
valueB
 :─ог2$
"wide/cross_product/Reshape/shape/1м
 wide/cross_product/Reshape/shapePack)wide/cross_product/strided_slice:output:0+wide/cross_product/Reshape/shape/1:output:0*
N*
T0*
_output_shapes
:2"
 wide/cross_product/Reshape/shape╩
wide/cross_product/ReshapeReshape%wide/cross_product/ScatterNd:output:0)wide/cross_product/Reshape/shape:output:0*
T0**
_output_shapes
:         ─ог2
wide/cross_product/Reshapeи
"wide/dense_5/MatMul/ReadVariableOpReadVariableOp+wide_dense_5_matmul_readvariableop_resource*!
_output_shapes
:─ог*
dtype02$
"wide/dense_5/MatMul/ReadVariableOpи
wide/dense_5/MatMulMatMul#wide/cross_product/Reshape:output:0*wide/dense_5/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
wide/dense_5/MatMul│
#wide/dense_5/BiasAdd/ReadVariableOpReadVariableOp,wide_dense_5_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02%
#wide/dense_5/BiasAdd/ReadVariableOpх
wide/dense_5/BiasAddBiasAddwide/dense_5/MatMul:product:0+wide/dense_5/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         2
wide/dense_5/BiasAddѕ
wide/dense_5/SigmoidSigmoidwide/dense_5/BiasAdd:output:0*
T0*'
_output_shapes
:         2
wide/dense_5/Sigmoids
IdentityIdentitywide/dense_5/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:         2

IdentityЎ
NoOpNoOp$^wide/dense_5/BiasAdd/ReadVariableOp#^wide/dense_5/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 2J
#wide/dense_5/BiasAdd/ReadVariableOp#wide/dense_5/BiasAdd/ReadVariableOp2H
"wide/dense_5/MatMul/ReadVariableOp"wide/dense_5/MatMul/ReadVariableOp:Q M
(
_output_shapes
:         њ
!
_user_specified_name	history:LH
#
_output_shapes
:         
!
_user_specified_name	item_id
Т
б
$__inference_wide_layer_call_fn_76161
history
item_id
unknown:─ог
	unknown_0:
identityѕбStatefulPartitionedCall§
StatefulPartitionedCallStatefulPartitionedCallhistoryitem_idunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *H
fCRA
?__inference_wide_layer_call_and_return_conditional_losses_761542
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 22
StatefulPartitionedCallStatefulPartitionedCall:Q M
(
_output_shapes
:         њ
!
_user_specified_name	history:LH
#
_output_shapes
:         
!
_user_specified_name	item_id
н
Y
-__inference_cross_product_layer_call_fn_76375
inputs_0
inputs_1
identity┘
PartitionedCallPartitionedCallinputs_0inputs_1*
Tin
2*
Tout
2*
_collective_manager_ids
 **
_output_shapes
:         ─ог* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8ѓ *Q
fLRJ
H__inference_cross_product_layer_call_and_return_conditional_losses_761342
PartitionedCallo
IdentityIdentityPartitionedCall:output:0*
T0**
_output_shapes
:         ─ог2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         :         њ:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:RN
(
_output_shapes
:         њ
"
_user_specified_name
inputs/1
љ	
░
$__inference_wide_layer_call_fn_76337
inputs_history
inputs_item_id
unknown:─ог
	unknown_0:
identityѕбStatefulPartitionedCallІ
StatefulPartitionedCallStatefulPartitionedCallinputs_historyinputs_item_idunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8ѓ *H
fCRA
?__inference_wide_layer_call_and_return_conditional_losses_761542
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*:
_input_shapes)
':         њ:         : : 22
StatefulPartitionedCallStatefulPartitionedCall:X T
(
_output_shapes
:         њ
(
_user_specified_nameinputs/history:SO
#
_output_shapes
:         
(
_user_specified_nameinputs/item_id"еL
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*С
serving_defaultл
<
history1
serving_default_history:0         њ
7
item_id,
serving_default_item_id:0         ;
dense_50
StatefulPartitionedCall:0         tensorflow/serving/predict:дG
Й
layer-0
layer-1
layer-2
layer-3
layer_with_weights-0
layer-4
	optimizer
	variables
trainable_variables
	regularization_losses

	keras_api

signatures
A_default_save_signature
*B&call_and_return_all_conditional_losses
C__call__"
_tf_keras_network
"
_tf_keras_input_layer
(
	keras_api"
_tf_keras_layer
"
_tf_keras_input_layer
Ц
	variables
regularization_losses
trainable_variables
	keras_api
*D&call_and_return_all_conditional_losses
E__call__"
_tf_keras_layer
╗

kernel
bias
	variables
regularization_losses
trainable_variables
	keras_api
*F&call_and_return_all_conditional_losses
G__call__"
_tf_keras_layer
Ё
iter

beta_1

beta_2
	decay
learning_ratem=m>v?v@"
tf_deprecated_optimizer
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
╩
non_trainable_variables
layer_regularization_losses
	variables
metrics
layer_metrics
trainable_variables
	regularization_losses

 layers
C__call__
A_default_save_signature
*B&call_and_return_all_conditional_losses
&B"call_and_return_conditional_losses"
_generic_user_object
,
Hserving_default"
signature_map
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
Г
!non_trainable_variables
"layer_regularization_losses
	variables
#metrics
regularization_losses
trainable_variables
$layer_metrics

%layers
E__call__
*D&call_and_return_all_conditional_losses
&D"call_and_return_conditional_losses"
_generic_user_object
#:!─ог2dense_5/kernel
:2dense_5/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
Г
&non_trainable_variables
'layer_regularization_losses
	variables
(metrics
regularization_losses
trainable_variables
)layer_metrics

*layers
G__call__
*F&call_and_return_all_conditional_losses
&F"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
 "
trackable_list_wrapper
5
+0
,1
-2"
trackable_list_wrapper
 "
trackable_dict_wrapper
C
0
1
2
3
4"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
N
	.total
	/count
0	variables
1	keras_api"
_tf_keras_metric
^
	2total
	3count
4
_fn_kwargs
5	variables
6	keras_api"
_tf_keras_metric
і
7true_positives
8true_negatives
9false_positives
:false_negatives
;	variables
<	keras_api"
_tf_keras_metric
:  (2total
:  (2count
.
.0
/1"
trackable_list_wrapper
-
0	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
.
20
31"
trackable_list_wrapper
-
5	variables"
_generic_user_object
:╚ (2true_positives
:╚ (2true_negatives
 :╚ (2false_positives
 :╚ (2false_negatives
<
70
81
92
:3"
trackable_list_wrapper
-
;	variables"
_generic_user_object
(:&─ог2Adam/dense_5/kernel/m
:2Adam/dense_5/bias/m
(:&─ог2Adam/dense_5/kernel/v
:2Adam/dense_5/bias/v
Ў2ќ
 __inference__wrapped_model_76101ы
І▓Є
FullArgSpec
argsџ 
varargsjargs
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *aб^
\фY
-
history"і
history         њ
(
item_idі
item_id         
╩2К
?__inference_wide_layer_call_and_return_conditional_losses_76296
?__inference_wide_layer_call_and_return_conditional_losses_76327
?__inference_wide_layer_call_and_return_conditional_losses_76234
?__inference_wide_layer_call_and_return_conditional_losses_76247└
и▓│
FullArgSpec1
args)џ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsџ
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
я2█
$__inference_wide_layer_call_fn_76161
$__inference_wide_layer_call_fn_76337
$__inference_wide_layer_call_fn_76347
$__inference_wide_layer_call_fn_76221└
и▓│
FullArgSpec1
args)џ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsџ
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
Ы2№
H__inference_cross_product_layer_call_and_return_conditional_losses_76369б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
О2н
-__inference_cross_product_layer_call_fn_76375б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
В2ж
B__inference_dense_5_layer_call_and_return_conditional_losses_76386б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
Л2╬
'__inference_dense_5_layer_call_fn_76395б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
ЛB╬
#__inference_signature_wrapper_76265historyitem_id"ћ
Ї▓Ѕ
FullArgSpec
argsџ 
varargs
 
varkwjkwargs
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 ╔
 __inference__wrapped_model_76101цkбh
aб^
\фY
-
history"і
history         њ
(
item_idі
item_id         
ф "1ф.
,
dense_5!і
dense_5         н
H__inference_cross_product_layer_call_and_return_conditional_losses_76369Є[бX
QбN
LбI
"і
inputs/0         
#і 
inputs/1         њ
ф "(б%
і
0         ─ог
џ Ф
-__inference_cross_product_layer_call_fn_76375z[бX
QбN
LбI
"і
inputs/0         
#і 
inputs/1         њ
ф "і         ─огЦ
B__inference_dense_5_layer_call_and_return_conditional_losses_76386_2б/
(б%
#і 
inputs         ─ог
ф "%б"
і
0         
џ }
'__inference_dense_5_layer_call_fn_76395R2б/
(б%
#і 
inputs         ─ог
ф "і         К
#__inference_signature_wrapper_76265Ъfбc
б 
\фY
-
history"і
history         њ
(
item_idі
item_id         "1ф.
,
dense_5!і
dense_5         С
?__inference_wide_layer_call_and_return_conditional_losses_76234аsбp
iбf
\фY
-
history"і
history         њ
(
item_idі
item_id         
p 

 
ф "%б"
і
0         
џ С
?__inference_wide_layer_call_and_return_conditional_losses_76247аsбp
iбf
\фY
-
history"і
history         њ
(
item_idі
item_id         
p

 
ф "%б"
і
0         
џ з
?__inference_wide_layer_call_and_return_conditional_losses_76296»Ђб~
wбt
jфg
4
history)і&
inputs/history         њ
/
item_id$і!
inputs/item_id         
p 

 
ф "%б"
і
0         
џ з
?__inference_wide_layer_call_and_return_conditional_losses_76327»Ђб~
wбt
jфg
4
history)і&
inputs/history         њ
/
item_id$і!
inputs/item_id         
p

 
ф "%б"
і
0         
џ ╝
$__inference_wide_layer_call_fn_76161Њsбp
iбf
\фY
-
history"і
history         њ
(
item_idі
item_id         
p 

 
ф "і         ╝
$__inference_wide_layer_call_fn_76221Њsбp
iбf
\фY
-
history"і
history         њ
(
item_idі
item_id         
p

 
ф "і         ╦
$__inference_wide_layer_call_fn_76337бЂб~
wбt
jфg
4
history)і&
inputs/history         њ
/
item_id$і!
inputs/item_id         
p 

 
ф "і         ╦
$__inference_wide_layer_call_fn_76347бЂб~
wбt
jфg
4
history)і&
inputs/history         њ
/
item_id$і!
inputs/item_id         
p

 
ф "і         