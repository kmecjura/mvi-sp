��
��
D
AddV2
x"T
y"T
z"T"
Ttype:
2	��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
�
DenseBincount
input"Tidx
size"Tidx
weights"T
output"T"
Tidxtype:
2	"
Ttype:
2	"
binary_outputbool( 
W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
=
Greater
x"T
y"T
z
"
Ttype:
2	
.
Identity

input"T
output"T"	
Ttype
l
LookupTableExportV2
table_handle
keys"Tkeys
values"Tvalues"
Tkeystype"
Tvaluestype�
w
LookupTableFindV2
table_handle
keys"Tin
default_value"Tout
values"Tout"
Tintype"
Touttype�
b
LookupTableImportV2
table_handle
keys"Tin
values"Tout"
Tintype"
Touttype�
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
�
Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
>
Maximum
x"T
y"T
z"T"
Ttype:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�
>
Minimum
x"T
y"T
z"T"
Ttype:
2	
?
Mul
x"T
y"T
z"T"
Ttype:
2	�
�
MutableHashTableV2
table_handle"
	containerstring "
shared_namestring "!
use_node_name_sharingbool( "
	key_dtypetype"
value_dtypetype�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
�
PartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
�
Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
�
ResourceGather
resource
indices"Tindices
output"dtype"

batch_dimsint "
validate_indicesbool("
dtypetype"
Tindicestype:
2	�
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
.
Rsqrt
x"T
y"T"
Ttype:

2
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
<
Sub
x"T
y"T
z"T"
Ttype:
2	
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.6.02unknown8Ģ
�
string_lookup_2_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_nametable_25875*
value_dtype0	
�
integer_lookup_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0	*
shared_nametable_25928*
value_dtype0	
�
string_lookup_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_nametable_25797*
value_dtype0	
�
string_lookup_1_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_nametable_25836*
value_dtype0	
�
embedding/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*%
shared_nameembedding/embeddings
~
(embedding/embeddings/Read/ReadVariableOpReadVariableOpembedding/embeddings*
_output_shapes
:	�*
dtype0
u
dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�)*
shared_namedense/kernel
n
 dense/kernel/Read/ReadVariableOpReadVariableOpdense/kernel*
_output_shapes
:	�)*
dtype0
�
embedding_1/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�)*'
shared_nameembedding_1/embeddings
�
*embedding_1/embeddings/Read/ReadVariableOpReadVariableOpembedding_1/embeddings*
_output_shapes
:	�)*
dtype0
�
batch_normalization/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:�**
shared_namebatch_normalization/gamma
�
-batch_normalization/gamma/Read/ReadVariableOpReadVariableOpbatch_normalization/gamma*
_output_shapes	
:�*
dtype0
�
batch_normalization/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*)
shared_namebatch_normalization/beta
�
,batch_normalization/beta/Read/ReadVariableOpReadVariableOpbatch_normalization/beta*
_output_shapes	
:�*
dtype0
�
batch_normalization/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*0
shared_name!batch_normalization/moving_mean
�
3batch_normalization/moving_mean/Read/ReadVariableOpReadVariableOpbatch_normalization/moving_mean*
_output_shapes	
:�*
dtype0
�
#batch_normalization/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*4
shared_name%#batch_normalization/moving_variance
�
7batch_normalization/moving_variance/Read/ReadVariableOpReadVariableOp#batch_normalization/moving_variance*
_output_shapes	
:�*
dtype0
z
dense_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*
shared_namedense_1/kernel
s
"dense_1/kernel/Read/ReadVariableOpReadVariableOpdense_1/kernel* 
_output_shapes
:
��*
dtype0
q
dense_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_namedense_1/bias
j
 dense_1/bias/Read/ReadVariableOpReadVariableOpdense_1/bias*
_output_shapes	
:�*
dtype0
�
batch_normalization_1/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namebatch_normalization_1/gamma
�
/batch_normalization_1/gamma/Read/ReadVariableOpReadVariableOpbatch_normalization_1/gamma*
_output_shapes	
:�*
dtype0
�
batch_normalization_1/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namebatch_normalization_1/beta
�
.batch_normalization_1/beta/Read/ReadVariableOpReadVariableOpbatch_normalization_1/beta*
_output_shapes	
:�*
dtype0
�
!batch_normalization_1/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*2
shared_name#!batch_normalization_1/moving_mean
�
5batch_normalization_1/moving_mean/Read/ReadVariableOpReadVariableOp!batch_normalization_1/moving_mean*
_output_shapes	
:�*
dtype0
�
%batch_normalization_1/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*6
shared_name'%batch_normalization_1/moving_variance
�
9batch_normalization_1/moving_variance/Read/ReadVariableOpReadVariableOp%batch_normalization_1/moving_variance*
_output_shapes	
:�*
dtype0
z
dense_2/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*
shared_namedense_2/kernel
s
"dense_2/kernel/Read/ReadVariableOpReadVariableOpdense_2/kernel* 
_output_shapes
:
��*
dtype0
q
dense_2/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_namedense_2/bias
j
 dense_2/bias/Read/ReadVariableOpReadVariableOpdense_2/bias*
_output_shapes	
:�*
dtype0
�
batch_normalization_2/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namebatch_normalization_2/gamma
�
/batch_normalization_2/gamma/Read/ReadVariableOpReadVariableOpbatch_normalization_2/gamma*
_output_shapes	
:�*
dtype0
�
batch_normalization_2/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*+
shared_namebatch_normalization_2/beta
�
.batch_normalization_2/beta/Read/ReadVariableOpReadVariableOpbatch_normalization_2/beta*
_output_shapes	
:�*
dtype0
�
!batch_normalization_2/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*2
shared_name#!batch_normalization_2/moving_mean
�
5batch_normalization_2/moving_mean/Read/ReadVariableOpReadVariableOp!batch_normalization_2/moving_mean*
_output_shapes	
:�*
dtype0
�
%batch_normalization_2/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*6
shared_name'%batch_normalization_2/moving_variance
�
9batch_normalization_2/moving_variance/Read/ReadVariableOpReadVariableOp%batch_normalization_2/moving_variance*
_output_shapes	
:�*
dtype0
y
dense_3/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�@*
shared_namedense_3/kernel
r
"dense_3/kernel/Read/ReadVariableOpReadVariableOpdense_3/kernel*
_output_shapes
:	�@*
dtype0
p
dense_3/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*
shared_namedense_3/bias
i
 dense_3/bias/Read/ReadVariableOpReadVariableOpdense_3/bias*
_output_shapes
:@*
dtype0
x
dense_4/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*
shared_namedense_4/kernel
q
"dense_4/kernel/Read/ReadVariableOpReadVariableOpdense_4/kernel*
_output_shapes

:@*
dtype0
p
dense_4/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_4/bias
i
 dense_4/bias/Read/ReadVariableOpReadVariableOpdense_4/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
u
true_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_nametrue_positives
n
"true_positives/Read/ReadVariableOpReadVariableOptrue_positives*
_output_shapes	
:�*
dtype0
u
true_negativesVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_nametrue_negatives
n
"true_negatives/Read/ReadVariableOpReadVariableOptrue_negatives*
_output_shapes	
:�*
dtype0
w
false_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:�* 
shared_namefalse_positives
p
#false_positives/Read/ReadVariableOpReadVariableOpfalse_positives*
_output_shapes	
:�*
dtype0
w
false_negativesVarHandleOp*
_output_shapes
: *
dtype0*
shape:�* 
shared_namefalse_negatives
p
#false_negatives/Read/ReadVariableOpReadVariableOpfalse_negatives*
_output_shapes	
:�*
dtype0
�
Adam/embedding/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*,
shared_nameAdam/embedding/embeddings/m
�
/Adam/embedding/embeddings/m/Read/ReadVariableOpReadVariableOpAdam/embedding/embeddings/m*
_output_shapes
:	�*
dtype0
�
Adam/dense/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�)*$
shared_nameAdam/dense/kernel/m
|
'Adam/dense/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/m*
_output_shapes
:	�)*
dtype0
�
Adam/embedding_1/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�)*.
shared_nameAdam/embedding_1/embeddings/m
�
1Adam/embedding_1/embeddings/m/Read/ReadVariableOpReadVariableOpAdam/embedding_1/embeddings/m*
_output_shapes
:	�)*
dtype0
�
 Adam/batch_normalization/gamma/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*1
shared_name" Adam/batch_normalization/gamma/m
�
4Adam/batch_normalization/gamma/m/Read/ReadVariableOpReadVariableOp Adam/batch_normalization/gamma/m*
_output_shapes	
:�*
dtype0
�
Adam/batch_normalization/beta/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*0
shared_name!Adam/batch_normalization/beta/m
�
3Adam/batch_normalization/beta/m/Read/ReadVariableOpReadVariableOpAdam/batch_normalization/beta/m*
_output_shapes	
:�*
dtype0
�
Adam/dense_1/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*&
shared_nameAdam/dense_1/kernel/m
�
)Adam/dense_1/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/m* 
_output_shapes
:
��*
dtype0

Adam/dense_1/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*$
shared_nameAdam/dense_1/bias/m
x
'Adam/dense_1/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/m*
_output_shapes	
:�*
dtype0
�
"Adam/batch_normalization_1/gamma/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/batch_normalization_1/gamma/m
�
6Adam/batch_normalization_1/gamma/m/Read/ReadVariableOpReadVariableOp"Adam/batch_normalization_1/gamma/m*
_output_shapes	
:�*
dtype0
�
!Adam/batch_normalization_1/beta/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*2
shared_name#!Adam/batch_normalization_1/beta/m
�
5Adam/batch_normalization_1/beta/m/Read/ReadVariableOpReadVariableOp!Adam/batch_normalization_1/beta/m*
_output_shapes	
:�*
dtype0
�
Adam/dense_2/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*&
shared_nameAdam/dense_2/kernel/m
�
)Adam/dense_2/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_2/kernel/m* 
_output_shapes
:
��*
dtype0

Adam/dense_2/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*$
shared_nameAdam/dense_2/bias/m
x
'Adam/dense_2/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_2/bias/m*
_output_shapes	
:�*
dtype0
�
"Adam/batch_normalization_2/gamma/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/batch_normalization_2/gamma/m
�
6Adam/batch_normalization_2/gamma/m/Read/ReadVariableOpReadVariableOp"Adam/batch_normalization_2/gamma/m*
_output_shapes	
:�*
dtype0
�
!Adam/batch_normalization_2/beta/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*2
shared_name#!Adam/batch_normalization_2/beta/m
�
5Adam/batch_normalization_2/beta/m/Read/ReadVariableOpReadVariableOp!Adam/batch_normalization_2/beta/m*
_output_shapes	
:�*
dtype0
�
Adam/dense_3/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�@*&
shared_nameAdam/dense_3/kernel/m
�
)Adam/dense_3/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_3/kernel/m*
_output_shapes
:	�@*
dtype0
~
Adam/dense_3/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_3/bias/m
w
'Adam/dense_3/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_3/bias/m*
_output_shapes
:@*
dtype0
�
Adam/dense_4/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*&
shared_nameAdam/dense_4/kernel/m

)Adam/dense_4/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_4/kernel/m*
_output_shapes

:@*
dtype0
~
Adam/dense_4/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_4/bias/m
w
'Adam/dense_4/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_4/bias/m*
_output_shapes
:*
dtype0
�
Adam/embedding/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*,
shared_nameAdam/embedding/embeddings/v
�
/Adam/embedding/embeddings/v/Read/ReadVariableOpReadVariableOpAdam/embedding/embeddings/v*
_output_shapes
:	�*
dtype0
�
Adam/dense/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�)*$
shared_nameAdam/dense/kernel/v
|
'Adam/dense/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/v*
_output_shapes
:	�)*
dtype0
�
Adam/embedding_1/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�)*.
shared_nameAdam/embedding_1/embeddings/v
�
1Adam/embedding_1/embeddings/v/Read/ReadVariableOpReadVariableOpAdam/embedding_1/embeddings/v*
_output_shapes
:	�)*
dtype0
�
 Adam/batch_normalization/gamma/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*1
shared_name" Adam/batch_normalization/gamma/v
�
4Adam/batch_normalization/gamma/v/Read/ReadVariableOpReadVariableOp Adam/batch_normalization/gamma/v*
_output_shapes	
:�*
dtype0
�
Adam/batch_normalization/beta/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*0
shared_name!Adam/batch_normalization/beta/v
�
3Adam/batch_normalization/beta/v/Read/ReadVariableOpReadVariableOpAdam/batch_normalization/beta/v*
_output_shapes	
:�*
dtype0
�
Adam/dense_1/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*&
shared_nameAdam/dense_1/kernel/v
�
)Adam/dense_1/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/v* 
_output_shapes
:
��*
dtype0

Adam/dense_1/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*$
shared_nameAdam/dense_1/bias/v
x
'Adam/dense_1/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/v*
_output_shapes	
:�*
dtype0
�
"Adam/batch_normalization_1/gamma/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/batch_normalization_1/gamma/v
�
6Adam/batch_normalization_1/gamma/v/Read/ReadVariableOpReadVariableOp"Adam/batch_normalization_1/gamma/v*
_output_shapes	
:�*
dtype0
�
!Adam/batch_normalization_1/beta/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*2
shared_name#!Adam/batch_normalization_1/beta/v
�
5Adam/batch_normalization_1/beta/v/Read/ReadVariableOpReadVariableOp!Adam/batch_normalization_1/beta/v*
_output_shapes	
:�*
dtype0
�
Adam/dense_2/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*&
shared_nameAdam/dense_2/kernel/v
�
)Adam/dense_2/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_2/kernel/v* 
_output_shapes
:
��*
dtype0

Adam/dense_2/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*$
shared_nameAdam/dense_2/bias/v
x
'Adam/dense_2/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_2/bias/v*
_output_shapes	
:�*
dtype0
�
"Adam/batch_normalization_2/gamma/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*3
shared_name$"Adam/batch_normalization_2/gamma/v
�
6Adam/batch_normalization_2/gamma/v/Read/ReadVariableOpReadVariableOp"Adam/batch_normalization_2/gamma/v*
_output_shapes	
:�*
dtype0
�
!Adam/batch_normalization_2/beta/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*2
shared_name#!Adam/batch_normalization_2/beta/v
�
5Adam/batch_normalization_2/beta/v/Read/ReadVariableOpReadVariableOp!Adam/batch_normalization_2/beta/v*
_output_shapes	
:�*
dtype0
�
Adam/dense_3/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�@*&
shared_nameAdam/dense_3/kernel/v
�
)Adam/dense_3/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_3/kernel/v*
_output_shapes
:	�@*
dtype0
~
Adam/dense_3/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*$
shared_nameAdam/dense_3/bias/v
w
'Adam/dense_3/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_3/bias/v*
_output_shapes
:@*
dtype0
�
Adam/dense_4/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@*&
shared_nameAdam/dense_4/kernel/v

)Adam/dense_4/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_4/kernel/v*
_output_shapes

:@*
dtype0
~
Adam/dense_4/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_4/bias/v
w
'Adam/dense_4/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_4/bias/v*
_output_shapes
:*
dtype0
G
ConstConst*
_output_shapes
: *
dtype0	*
value	B	 R 
I
Const_1Const*
_output_shapes
: *
dtype0	*
value	B	 R 
I
Const_2Const*
_output_shapes
: *
dtype0	*
value	B	 R 
I
Const_3Const*
_output_shapes
: *
dtype0	*
value	B	 R 
�
PartitionedCallPartitionedCall*	
Tin
 *
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *#
fR
__inference_<lambda>_53018
�
PartitionedCall_1PartitionedCall*	
Tin
 *
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *#
fR
__inference_<lambda>_53023
�
PartitionedCall_2PartitionedCall*	
Tin
 *
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *#
fR
__inference_<lambda>_53028
�
PartitionedCall_3PartitionedCall*	
Tin
 *
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *#
fR
__inference_<lambda>_53033
Z
NoOpNoOp^PartitionedCall^PartitionedCall_1^PartitionedCall_2^PartitionedCall_3
�
Jstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2string_lookup_2_index_table*
Tkeys0*
Tvalues0	*.
_class$
" loc:@string_lookup_2_index_table*
_output_shapes

::
�
Iinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2integer_lookup_index_table*
Tkeys0	*
Tvalues0	*-
_class#
!loc:@integer_lookup_index_table*
_output_shapes

::
�
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2string_lookup_index_table*
Tkeys0*
Tvalues0	*,
_class"
 loc:@string_lookup_index_table*
_output_shapes

::
�
Jstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2string_lookup_1_index_table*
Tkeys0*
Tvalues0	*.
_class$
" loc:@string_lookup_1_index_table*
_output_shapes

::
�z
Const_4Const"/device:CPU:0*
_output_shapes
: *
dtype0*�z
value�zB�z B�z
�
layer-0
layer-1
layer-2
layer-3
layer-4
layer-5
layer_with_weights-0
layer-6
layer-7
	layer-8

layer_with_weights-1

layer-9
layer-10
layer_with_weights-2
layer-11
layer-12
layer_with_weights-3
layer-13
layer_with_weights-4
layer-14
layer_with_weights-5
layer-15
layer-16
layer_with_weights-6
layer-17
layer-18
layer-19
layer_with_weights-7
layer-20
layer_with_weights-8
layer-21
layer-22
layer_with_weights-9
layer-23
layer_with_weights-10
layer-24
layer-25
layer_with_weights-11
layer-26
layer_with_weights-12
layer-27
layer-28
layer_with_weights-13
layer-29
	optimizer
 	variables
!trainable_variables
"regularization_losses
#	keras_api
$
signatures
 
 
 
 

%	keras_api

&	keras_api
0
'state_variables

(_table
)	keras_api
 
 
0
*state_variables

+_table
,	keras_api
 
0
-state_variables

._table
/	keras_api
 
0
0state_variables

1_table
2	keras_api
b
3
embeddings
4	variables
5regularization_losses
6trainable_variables
7	keras_api
^

8kernel
9	variables
:regularization_losses
;trainable_variables
<	keras_api

=	keras_api
b
>
embeddings
?	variables
@regularization_losses
Atrainable_variables
B	keras_api

C	keras_api
R
D	variables
Eregularization_losses
Ftrainable_variables
G	keras_api
�
Haxis
	Igamma
Jbeta
Kmoving_mean
Lmoving_variance
M	variables
Nregularization_losses
Otrainable_variables
P	keras_api
h

Qkernel
Rbias
S	variables
Tregularization_losses
Utrainable_variables
V	keras_api
R
W	variables
Xregularization_losses
Ytrainable_variables
Z	keras_api
�
[axis
	\gamma
]beta
^moving_mean
_moving_variance
`	variables
aregularization_losses
btrainable_variables
c	keras_api
h

dkernel
ebias
f	variables
gregularization_losses
htrainable_variables
i	keras_api
R
j	variables
kregularization_losses
ltrainable_variables
m	keras_api
�
naxis
	ogamma
pbeta
qmoving_mean
rmoving_variance
s	variables
tregularization_losses
utrainable_variables
v	keras_api
h

wkernel
xbias
y	variables
zregularization_losses
{trainable_variables
|	keras_api
S
}	variables
~regularization_losses
trainable_variables
�	keras_api
n
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
�
	�iter
�beta_1
�beta_2

�decay
�learning_rate3m�8m�>m�Im�Jm�Qm�Rm�\m�]m�dm�em�om�pm�wm�xm�	�m�	�m�3v�8v�>v�Iv�Jv�Qv�Rv�\v�]v�dv�ev�ov�pv�wv�xv�	�v�	�v�
�
34
85
>6
I7
J8
K9
L10
Q11
R12
\13
]14
^15
_16
d17
e18
o19
p20
q21
r22
w23
x24
�25
�26
�
30
81
>2
I3
J4
Q5
R6
\7
]8
d9
e10
o11
p12
w13
x14
�15
�16
 
�
�non_trainable_variables
 �layer_regularization_losses
 	variables
�metrics
�layer_metrics
!trainable_variables
"regularization_losses
�layers
 
 
 
 
86
table-layer_with_weights-0/_table/.ATTRIBUTES/table
 
 
86
table-layer_with_weights-1/_table/.ATTRIBUTES/table
 
 
86
table-layer_with_weights-2/_table/.ATTRIBUTES/table
 
 
86
table-layer_with_weights-3/_table/.ATTRIBUTES/table
 
db
VARIABLE_VALUEembedding/embeddings:layer_with_weights-4/embeddings/.ATTRIBUTES/VARIABLE_VALUE

30
 

30
�
�non_trainable_variables
 �layer_regularization_losses
4	variables
�metrics
5regularization_losses
6trainable_variables
�layer_metrics
�layers
XV
VARIABLE_VALUEdense/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE

80
 

80
�
�non_trainable_variables
 �layer_regularization_losses
9	variables
�metrics
:regularization_losses
;trainable_variables
�layer_metrics
�layers
 
fd
VARIABLE_VALUEembedding_1/embeddings:layer_with_weights-6/embeddings/.ATTRIBUTES/VARIABLE_VALUE

>0
 

>0
�
�non_trainable_variables
 �layer_regularization_losses
?	variables
�metrics
@regularization_losses
Atrainable_variables
�layer_metrics
�layers
 
 
 
 
�
�non_trainable_variables
 �layer_regularization_losses
D	variables
�metrics
Eregularization_losses
Ftrainable_variables
�layer_metrics
�layers
 
db
VARIABLE_VALUEbatch_normalization/gamma5layer_with_weights-7/gamma/.ATTRIBUTES/VARIABLE_VALUE
b`
VARIABLE_VALUEbatch_normalization/beta4layer_with_weights-7/beta/.ATTRIBUTES/VARIABLE_VALUE
pn
VARIABLE_VALUEbatch_normalization/moving_mean;layer_with_weights-7/moving_mean/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUE#batch_normalization/moving_variance?layer_with_weights-7/moving_variance/.ATTRIBUTES/VARIABLE_VALUE

I0
J1
K2
L3
 

I0
J1
�
�non_trainable_variables
 �layer_regularization_losses
M	variables
�metrics
Nregularization_losses
Otrainable_variables
�layer_metrics
�layers
ZX
VARIABLE_VALUEdense_1/kernel6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_1/bias4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUE

Q0
R1
 

Q0
R1
�
�non_trainable_variables
 �layer_regularization_losses
S	variables
�metrics
Tregularization_losses
Utrainable_variables
�layer_metrics
�layers
 
 
 
�
�non_trainable_variables
 �layer_regularization_losses
W	variables
�metrics
Xregularization_losses
Ytrainable_variables
�layer_metrics
�layers
 
fd
VARIABLE_VALUEbatch_normalization_1/gamma5layer_with_weights-9/gamma/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEbatch_normalization_1/beta4layer_with_weights-9/beta/.ATTRIBUTES/VARIABLE_VALUE
rp
VARIABLE_VALUE!batch_normalization_1/moving_mean;layer_with_weights-9/moving_mean/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUE%batch_normalization_1/moving_variance?layer_with_weights-9/moving_variance/.ATTRIBUTES/VARIABLE_VALUE

\0
]1
^2
_3
 

\0
]1
�
�non_trainable_variables
 �layer_regularization_losses
`	variables
�metrics
aregularization_losses
btrainable_variables
�layer_metrics
�layers
[Y
VARIABLE_VALUEdense_2/kernel7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_2/bias5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUE

d0
e1
 

d0
e1
�
�non_trainable_variables
 �layer_regularization_losses
f	variables
�metrics
gregularization_losses
htrainable_variables
�layer_metrics
�layers
 
 
 
�
�non_trainable_variables
 �layer_regularization_losses
j	variables
�metrics
kregularization_losses
ltrainable_variables
�layer_metrics
�layers
 
ge
VARIABLE_VALUEbatch_normalization_2/gamma6layer_with_weights-11/gamma/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEbatch_normalization_2/beta5layer_with_weights-11/beta/.ATTRIBUTES/VARIABLE_VALUE
sq
VARIABLE_VALUE!batch_normalization_2/moving_mean<layer_with_weights-11/moving_mean/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUE%batch_normalization_2/moving_variance@layer_with_weights-11/moving_variance/.ATTRIBUTES/VARIABLE_VALUE

o0
p1
q2
r3
 

o0
p1
�
�non_trainable_variables
 �layer_regularization_losses
s	variables
�metrics
tregularization_losses
utrainable_variables
�layer_metrics
�layers
[Y
VARIABLE_VALUEdense_3/kernel7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_3/bias5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUE

w0
x1
 

w0
x1
�
�non_trainable_variables
 �layer_regularization_losses
y	variables
�metrics
zregularization_losses
{trainable_variables
�layer_metrics
�layers
 
 
 
�
�non_trainable_variables
 �layer_regularization_losses
}	variables
�metrics
~regularization_losses
trainable_variables
�layer_metrics
�layers
[Y
VARIABLE_VALUEdense_4/kernel7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_4/bias5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
 

�0
�1
�
�non_trainable_variables
 �layer_regularization_losses
�	variables
�metrics
�regularization_losses
�trainable_variables
�layer_metrics
�layers
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
*
K4
L5
^6
_7
q8
r9
 

�0
�1
�2
 
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

K0
L1
 
 
 
 
 
 
 
 
 
 
 
 
 
 

^0
_1
 
 
 
 
 
 
 
 
 
 
 
 
 
 

q0
r1
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
v
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
a_
VARIABLE_VALUEtrue_positives=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEtrue_negatives=keras_api/metrics/2/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_positives>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_negatives>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUE
 
�0
�1
�2
�3

�	variables
��
VARIABLE_VALUEAdam/embedding/embeddings/mVlayer_with_weights-4/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/dense/kernel/mRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEAdam/embedding_1/embeddings/mVlayer_with_weights-6/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE Adam/batch_normalization/gamma/mQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEAdam/batch_normalization/beta/mPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/mRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/mPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/batch_normalization_1/gamma/mQlayer_with_weights-9/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE!Adam/batch_normalization_1/beta/mPlayer_with_weights-9/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_2/kernel/mSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_2/bias/mQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/batch_normalization_2/gamma/mRlayer_with_weights-11/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE!Adam/batch_normalization_2/beta/mQlayer_with_weights-11/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_3/kernel/mSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_3/bias/mQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_4/kernel/mSlayer_with_weights-13/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_4/bias/mQlayer_with_weights-13/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEAdam/embedding/embeddings/vVlayer_with_weights-4/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/dense/kernel/vRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEAdam/embedding_1/embeddings/vVlayer_with_weights-6/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE Adam/batch_normalization/gamma/vQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEAdam/batch_normalization/beta/vPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/vRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/vPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/batch_normalization_1/gamma/vQlayer_with_weights-9/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE!Adam/batch_normalization_1/beta/vPlayer_with_weights-9/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_2/kernel/vSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_2/bias/vQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/batch_normalization_2/gamma/vRlayer_with_weights-11/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE!Adam/batch_normalization_2/beta/vQlayer_with_weights-11/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_3/kernel/vSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_3/bias/vQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_4/kernel/vSlayer_with_weights-13/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_4/bias/vQlayer_with_weights-13/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
n
serving_default_agePlaceholder*#
_output_shapes
:���������*
dtype0*
shape:���������
q
serving_default_genderPlaceholder*#
_output_shapes
:���������*
dtype0*
shape:���������
x
serving_default_genrePlaceholder*'
_output_shapes
:���������*
dtype0*
shape:���������
|
serving_default_historyPlaceholder*(
_output_shapes
:����������*
dtype0*
shape:����������
r
serving_default_item_idPlaceholder*#
_output_shapes
:���������*
dtype0*
shape:���������
u
serving_default_occupationPlaceholder*#
_output_shapes
:���������*
dtype0*
shape:���������
w
serving_default_release_datePlaceholder*#
_output_shapes
:���������*
dtype0*
shape:���������
s
serving_default_zip_codePlaceholder*#
_output_shapes
:���������*
dtype0*
shape:���������
�	
StatefulPartitionedCallStatefulPartitionedCallserving_default_ageserving_default_genderserving_default_genreserving_default_historyserving_default_item_idserving_default_occupationserving_default_release_dateserving_default_zip_codeinteger_lookup_index_tableConststring_lookup_2_index_tableConst_1string_lookup_index_tableConst_2string_lookup_1_index_tableConst_3embedding/embeddingsdense/kernelembedding_1/embeddings#batch_normalization/moving_variancebatch_normalization/gammabatch_normalization/moving_meanbatch_normalization/betadense_1/kerneldense_1/bias%batch_normalization_1/moving_variancebatch_normalization_1/gamma!batch_normalization_1/moving_meanbatch_normalization_1/betadense_2/kerneldense_2/bias%batch_normalization_2/moving_variancebatch_normalization_2/gamma!batch_normalization_2/moving_meanbatch_normalization_2/betadense_3/kerneldense_3/biasdense_4/kerneldense_4/bias*2
Tin+
)2'				*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*9
_read_only_resource_inputs
 !"#$%&*0
config_proto 

CPU

GPU2*0J 8� *,
f'R%
#__inference_signature_wrapper_51838
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filenameJstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2Lstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2:1Iinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2Kinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2:1Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2Jstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:1Jstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2Lstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2:1(embedding/embeddings/Read/ReadVariableOp dense/kernel/Read/ReadVariableOp*embedding_1/embeddings/Read/ReadVariableOp-batch_normalization/gamma/Read/ReadVariableOp,batch_normalization/beta/Read/ReadVariableOp3batch_normalization/moving_mean/Read/ReadVariableOp7batch_normalization/moving_variance/Read/ReadVariableOp"dense_1/kernel/Read/ReadVariableOp dense_1/bias/Read/ReadVariableOp/batch_normalization_1/gamma/Read/ReadVariableOp.batch_normalization_1/beta/Read/ReadVariableOp5batch_normalization_1/moving_mean/Read/ReadVariableOp9batch_normalization_1/moving_variance/Read/ReadVariableOp"dense_2/kernel/Read/ReadVariableOp dense_2/bias/Read/ReadVariableOp/batch_normalization_2/gamma/Read/ReadVariableOp.batch_normalization_2/beta/Read/ReadVariableOp5batch_normalization_2/moving_mean/Read/ReadVariableOp9batch_normalization_2/moving_variance/Read/ReadVariableOp"dense_3/kernel/Read/ReadVariableOp dense_3/bias/Read/ReadVariableOp"dense_4/kernel/Read/ReadVariableOp dense_4/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp"true_positives/Read/ReadVariableOp"true_negatives/Read/ReadVariableOp#false_positives/Read/ReadVariableOp#false_negatives/Read/ReadVariableOp/Adam/embedding/embeddings/m/Read/ReadVariableOp'Adam/dense/kernel/m/Read/ReadVariableOp1Adam/embedding_1/embeddings/m/Read/ReadVariableOp4Adam/batch_normalization/gamma/m/Read/ReadVariableOp3Adam/batch_normalization/beta/m/Read/ReadVariableOp)Adam/dense_1/kernel/m/Read/ReadVariableOp'Adam/dense_1/bias/m/Read/ReadVariableOp6Adam/batch_normalization_1/gamma/m/Read/ReadVariableOp5Adam/batch_normalization_1/beta/m/Read/ReadVariableOp)Adam/dense_2/kernel/m/Read/ReadVariableOp'Adam/dense_2/bias/m/Read/ReadVariableOp6Adam/batch_normalization_2/gamma/m/Read/ReadVariableOp5Adam/batch_normalization_2/beta/m/Read/ReadVariableOp)Adam/dense_3/kernel/m/Read/ReadVariableOp'Adam/dense_3/bias/m/Read/ReadVariableOp)Adam/dense_4/kernel/m/Read/ReadVariableOp'Adam/dense_4/bias/m/Read/ReadVariableOp/Adam/embedding/embeddings/v/Read/ReadVariableOp'Adam/dense/kernel/v/Read/ReadVariableOp1Adam/embedding_1/embeddings/v/Read/ReadVariableOp4Adam/batch_normalization/gamma/v/Read/ReadVariableOp3Adam/batch_normalization/beta/v/Read/ReadVariableOp)Adam/dense_1/kernel/v/Read/ReadVariableOp'Adam/dense_1/bias/v/Read/ReadVariableOp6Adam/batch_normalization_1/gamma/v/Read/ReadVariableOp5Adam/batch_normalization_1/beta/v/Read/ReadVariableOp)Adam/dense_2/kernel/v/Read/ReadVariableOp'Adam/dense_2/bias/v/Read/ReadVariableOp6Adam/batch_normalization_2/gamma/v/Read/ReadVariableOp5Adam/batch_normalization_2/beta/v/Read/ReadVariableOp)Adam/dense_3/kernel/v/Read/ReadVariableOp'Adam/dense_3/bias/v/Read/ReadVariableOp)Adam/dense_4/kernel/v/Read/ReadVariableOp'Adam/dense_4/bias/v/Read/ReadVariableOpConst_4*[
TinT
R2P						*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *'
f"R 
__inference__traced_save_53301
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamestring_lookup_2_index_tableinteger_lookup_index_tablestring_lookup_index_tablestring_lookup_1_index_tableembedding/embeddingsdense/kernelembedding_1/embeddingsbatch_normalization/gammabatch_normalization/betabatch_normalization/moving_mean#batch_normalization/moving_variancedense_1/kerneldense_1/biasbatch_normalization_1/gammabatch_normalization_1/beta!batch_normalization_1/moving_mean%batch_normalization_1/moving_variancedense_2/kerneldense_2/biasbatch_normalization_2/gammabatch_normalization_2/beta!batch_normalization_2/moving_mean%batch_normalization_2/moving_variancedense_3/kerneldense_3/biasdense_4/kerneldense_4/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1true_positivestrue_negativesfalse_positivesfalse_negativesAdam/embedding/embeddings/mAdam/dense/kernel/mAdam/embedding_1/embeddings/m Adam/batch_normalization/gamma/mAdam/batch_normalization/beta/mAdam/dense_1/kernel/mAdam/dense_1/bias/m"Adam/batch_normalization_1/gamma/m!Adam/batch_normalization_1/beta/mAdam/dense_2/kernel/mAdam/dense_2/bias/m"Adam/batch_normalization_2/gamma/m!Adam/batch_normalization_2/beta/mAdam/dense_3/kernel/mAdam/dense_3/bias/mAdam/dense_4/kernel/mAdam/dense_4/bias/mAdam/embedding/embeddings/vAdam/dense/kernel/vAdam/embedding_1/embeddings/v Adam/batch_normalization/gamma/vAdam/batch_normalization/beta/vAdam/dense_1/kernel/vAdam/dense_1/bias/v"Adam/batch_normalization_1/gamma/v!Adam/batch_normalization_1/beta/vAdam/dense_2/kernel/vAdam/dense_2/bias/v"Adam/batch_normalization_2/gamma/v!Adam/batch_normalization_2/beta/vAdam/dense_3/kernel/vAdam/dense_3/bias/vAdam/dense_4/kernel/vAdam/dense_4/bias/v*V
TinO
M2K*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� **
f%R#
!__inference__traced_restore_53533۾
�
�
B__inference_dense_4_layer_call_and_return_conditional_losses_50932

inputs0
matmul_readvariableop_resource:@-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
c
D__inference_dropout_1_layer_call_and_return_conditional_losses_51067

inputs
identity�c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *���?2
dropout/Constt
dropout/MulMulinputsdropout/Const:output:0*
T0*(
_output_shapes
:����������2
dropout/MulT
dropout/ShapeShapeinputs*
T0*
_output_shapes
:2
dropout/Shape�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L=2
dropout/GreaterEqual/y�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������2
dropout/GreaterEqual�
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������2
dropout/Cast{
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*(
_output_shapes
:����������2
dropout/Mul_1f
IdentityIdentitydropout/Mul_1:z:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�
?__inference_deep_layer_call_and_return_conditional_losses_51756
age

gender	
genre
history
item_id

occupation
release_date
zip_codeH
Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleI
Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value	G
Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handleH
Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value	"
embedding_51691:	�
dense_51694:	�)$
embedding_1_51699:	�)(
batch_normalization_51705:	�(
batch_normalization_51707:	�(
batch_normalization_51709:	�(
batch_normalization_51711:	�!
dense_1_51714:
��
dense_1_51716:	�*
batch_normalization_1_51720:	�*
batch_normalization_1_51722:	�*
batch_normalization_1_51724:	�*
batch_normalization_1_51726:	�!
dense_2_51729:
��
dense_2_51731:	�*
batch_normalization_2_51735:	�*
batch_normalization_2_51737:	�*
batch_normalization_2_51739:	�*
batch_normalization_2_51741:	� 
dense_3_51744:	�@
dense_3_51746:@
dense_4_51750:@
dense_4_51752:
identity��+batch_normalization/StatefulPartitionedCall�-batch_normalization_1/StatefulPartitionedCall�-batch_normalization_2/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�dense_2/StatefulPartitionedCall�dense_3/StatefulPartitionedCall�dense_4/StatefulPartitionedCall�dropout/StatefulPartitionedCall�!dropout_1/StatefulPartitionedCall�!dropout_2/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�#embedding_1/StatefulPartitionedCall�7integer_lookup/None_lookup_table_find/LookupTableFindV2�6string_lookup/None_lookup_table_find/LookupTableFindV2�8string_lookup_1/None_lookup_table_find/LookupTableFindV2�8string_lookup_2/None_lookup_table_find/LookupTableFindV2x
integer_lookup/CastCastitem_id*

DstT0	*

SrcT0*#
_output_shapes
:���������2
integer_lookup/Cast�
7integer_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleinteger_lookup/Cast:y:0Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0	*

Tout0	*#
_output_shapes
:���������29
7integer_lookup/None_lookup_table_find/LookupTableFindV2�
integer_lookup/IdentityIdentity@integer_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
integer_lookup/Identity�
8string_lookup_2/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handlezip_codeFstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:���������2:
8string_lookup_2/None_lookup_table_find/LookupTableFindV2�
string_lookup_2/IdentityIdentityAstring_lookup_2/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
string_lookup_2/Identity�
tf.expand_dims_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_1/ExpandDims/dim�
tf.expand_dims_1/ExpandDims
ExpandDimsgender(tf.expand_dims_1/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_1/ExpandDims�
tf.expand_dims/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2
tf.expand_dims/ExpandDims/dim�
tf.expand_dims/ExpandDims
ExpandDims
occupation&tf.expand_dims/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims/ExpandDims�
6string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handle"tf.expand_dims/ExpandDims:output:0Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������28
6string_lookup/None_lookup_table_find/LookupTableFindV2�
string_lookup/bincount/ShapeShape?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2
string_lookup/bincount/Shape�
string_lookup/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
string_lookup/bincount/Const�
string_lookup/bincount/ProdProd%string_lookup/bincount/Shape:output:0%string_lookup/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup/bincount/Prod�
 string_lookup/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2"
 string_lookup/bincount/Greater/y�
string_lookup/bincount/GreaterGreater$string_lookup/bincount/Prod:output:0)string_lookup/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2 
string_lookup/bincount/Greater�
string_lookup/bincount/CastCast"string_lookup/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup/bincount/Cast�
string_lookup/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2 
string_lookup/bincount/Const_1�
string_lookup/bincount/MaxMax?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0'string_lookup/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/Max~
string_lookup/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2
string_lookup/bincount/add/y�
string_lookup/bincount/addAddV2#string_lookup/bincount/Max:output:0%string_lookup/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/add�
string_lookup/bincount/mulMulstring_lookup/bincount/Cast:y:0string_lookup/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/mul�
 string_lookup/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/minlength�
string_lookup/bincount/MaximumMaximum)string_lookup/bincount/minlength:output:0string_lookup/bincount/mul:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Maximum�
 string_lookup/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/maxlength�
string_lookup/bincount/MinimumMinimum)string_lookup/bincount/maxlength:output:0"string_lookup/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Minimum�
string_lookup/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2 
string_lookup/bincount/Const_2�
$string_lookup/bincount/DenseBincountDenseBincount?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0"string_lookup/bincount/Minimum:z:0'string_lookup/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2&
$string_lookup/bincount/DenseBincount�
8string_lookup_1/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handle$tf.expand_dims_1/ExpandDims:output:0Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������2:
8string_lookup_1/None_lookup_table_find/LookupTableFindV2�
string_lookup_1/bincount/ShapeShapeAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2 
string_lookup_1/bincount/Shape�
string_lookup_1/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2 
string_lookup_1/bincount/Const�
string_lookup_1/bincount/ProdProd'string_lookup_1/bincount/Shape:output:0'string_lookup_1/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup_1/bincount/Prod�
"string_lookup_1/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2$
"string_lookup_1/bincount/Greater/y�
 string_lookup_1/bincount/GreaterGreater&string_lookup_1/bincount/Prod:output:0+string_lookup_1/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2"
 string_lookup_1/bincount/Greater�
string_lookup_1/bincount/CastCast$string_lookup_1/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup_1/bincount/Cast�
 string_lookup_1/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2"
 string_lookup_1/bincount/Const_1�
string_lookup_1/bincount/MaxMaxAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0)string_lookup_1/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/Max�
string_lookup_1/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2 
string_lookup_1/bincount/add/y�
string_lookup_1/bincount/addAddV2%string_lookup_1/bincount/Max:output:0'string_lookup_1/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/add�
string_lookup_1/bincount/mulMul!string_lookup_1/bincount/Cast:y:0 string_lookup_1/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/mul�
"string_lookup_1/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/minlength�
 string_lookup_1/bincount/MaximumMaximum+string_lookup_1/bincount/minlength:output:0 string_lookup_1/bincount/mul:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Maximum�
"string_lookup_1/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/maxlength�
 string_lookup_1/bincount/MinimumMinimum+string_lookup_1/bincount/maxlength:output:0$string_lookup_1/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Minimum�
 string_lookup_1/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2"
 string_lookup_1/bincount/Const_2�
&string_lookup_1/bincount/DenseBincountDenseBincountAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0$string_lookup_1/bincount/Minimum:z:0)string_lookup_1/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2(
&string_lookup_1/bincount/DenseBincount�
!embedding/StatefulPartitionedCallStatefulPartitionedCall!string_lookup_2/Identity:output:0embedding_51691*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_507752#
!embedding/StatefulPartitionedCall�
dense/StatefulPartitionedCallStatefulPartitionedCallhistorydense_51694*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_507862
dense/StatefulPartitionedCall�
tf.expand_dims_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_2/ExpandDims/dim�
tf.expand_dims_2/ExpandDims
ExpandDimsage(tf.expand_dims_2/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_2/ExpandDims�
#embedding_1/StatefulPartitionedCallStatefulPartitionedCall integer_lookup/Identity:output:0embedding_1_51699*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_embedding_1_layer_call_and_return_conditional_losses_508012%
#embedding_1/StatefulPartitionedCall�
tf.expand_dims_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_3/ExpandDims/dim�
tf.expand_dims_3/ExpandDims
ExpandDimsrelease_date(tf.expand_dims_3/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_3/ExpandDims�
concatenate/PartitionedCallPartitionedCall-string_lookup/bincount/DenseBincount:output:0genre/string_lookup_1/bincount/DenseBincount:output:0*embedding/StatefulPartitionedCall:output:0&dense/StatefulPartitionedCall:output:0$tf.expand_dims_2/ExpandDims:output:0,embedding_1/StatefulPartitionedCall:output:0$tf.expand_dims_3/ExpandDims:output:0*
Tin

2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_concatenate_layer_call_and_return_conditional_losses_508202
concatenate/PartitionedCall�
+batch_normalization/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0batch_normalization_51705batch_normalization_51707batch_normalization_51709batch_normalization_51711*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_batch_normalization_layer_call_and_return_conditional_losses_502902-
+batch_normalization/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCall4batch_normalization/StatefulPartitionedCall:output:0dense_1_51714dense_1_51716*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_508422!
dense_1/StatefulPartitionedCall�
dropout/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_511002!
dropout/StatefulPartitionedCall�
-batch_normalization_1/StatefulPartitionedCallStatefulPartitionedCall(dropout/StatefulPartitionedCall:output:0batch_normalization_1_51720batch_normalization_1_51722batch_normalization_1_51724batch_normalization_1_51726*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_504522/
-batch_normalization_1/StatefulPartitionedCall�
dense_2/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_1/StatefulPartitionedCall:output:0dense_2_51729dense_2_51731*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_2_layer_call_and_return_conditional_losses_508752!
dense_2/StatefulPartitionedCall�
!dropout_1/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0 ^dropout/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_510672#
!dropout_1/StatefulPartitionedCall�
-batch_normalization_2/StatefulPartitionedCallStatefulPartitionedCall*dropout_1/StatefulPartitionedCall:output:0batch_normalization_2_51735batch_normalization_2_51737batch_normalization_2_51739batch_normalization_2_51741*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_506142/
-batch_normalization_2/StatefulPartitionedCall�
dense_3/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_2/StatefulPartitionedCall:output:0dense_3_51744dense_3_51746*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_3_layer_call_and_return_conditional_losses_509082!
dense_3/StatefulPartitionedCall�
!dropout_2/StatefulPartitionedCallStatefulPartitionedCall(dense_3/StatefulPartitionedCall:output:0"^dropout_1/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_2_layer_call_and_return_conditional_losses_510342#
!dropout_2/StatefulPartitionedCall�
dense_4/StatefulPartitionedCallStatefulPartitionedCall*dropout_2/StatefulPartitionedCall:output:0dense_4_51750dense_4_51752*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_4_layer_call_and_return_conditional_losses_509322!
dense_4/StatefulPartitionedCall�
IdentityIdentity(dense_4/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp,^batch_normalization/StatefulPartitionedCall.^batch_normalization_1/StatefulPartitionedCall.^batch_normalization_2/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dropout/StatefulPartitionedCall"^dropout_1/StatefulPartitionedCall"^dropout_2/StatefulPartitionedCall"^embedding/StatefulPartitionedCall$^embedding_1/StatefulPartitionedCall8^integer_lookup/None_lookup_table_find/LookupTableFindV27^string_lookup/None_lookup_table_find/LookupTableFindV29^string_lookup_1/None_lookup_table_find/LookupTableFindV29^string_lookup_2/None_lookup_table_find/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2Z
+batch_normalization/StatefulPartitionedCall+batch_normalization/StatefulPartitionedCall2^
-batch_normalization_1/StatefulPartitionedCall-batch_normalization_1/StatefulPartitionedCall2^
-batch_normalization_2/StatefulPartitionedCall-batch_normalization_2/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dropout/StatefulPartitionedCalldropout/StatefulPartitionedCall2F
!dropout_1/StatefulPartitionedCall!dropout_1/StatefulPartitionedCall2F
!dropout_2/StatefulPartitionedCall!dropout_2/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall2J
#embedding_1/StatefulPartitionedCall#embedding_1/StatefulPartitionedCall2r
7integer_lookup/None_lookup_table_find/LookupTableFindV27integer_lookup/None_lookup_table_find/LookupTableFindV22p
6string_lookup/None_lookup_table_find/LookupTableFindV26string_lookup/None_lookup_table_find/LookupTableFindV22t
8string_lookup_1/None_lookup_table_find/LookupTableFindV28string_lookup_1/None_lookup_table_find/LookupTableFindV22t
8string_lookup_2/None_lookup_table_find/LookupTableFindV28string_lookup_2/None_lookup_table_find/LookupTableFindV2:H D
#
_output_shapes
:���������

_user_specified_nameage:KG
#
_output_shapes
:���������
 
_user_specified_namegender:NJ
'
_output_shapes
:���������

_user_specified_namegenre:QM
(
_output_shapes
:����������
!
_user_specified_name	history:LH
#
_output_shapes
:���������
!
_user_specified_name	item_id:OK
#
_output_shapes
:���������
$
_user_specified_name
occupation:QM
#
_output_shapes
:���������
&
_user_specified_namerelease_date:MI
#
_output_shapes
:���������
"
_user_specified_name
zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
$__inference_deep_layer_call_fn_52299

inputs_age
inputs_gender
inputs_genre
inputs_history
inputs_item_id
inputs_occupation
inputs_release_date
inputs_zip_code
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3
	unknown_4	
	unknown_5
	unknown_6	
	unknown_7:	�
	unknown_8:	�)
	unknown_9:	�)

unknown_10:	�

unknown_11:	�

unknown_12:	�

unknown_13:	�

unknown_14:
��

unknown_15:	�

unknown_16:	�

unknown_17:	�

unknown_18:	�

unknown_19:	�

unknown_20:
��

unknown_21:	�

unknown_22:	�

unknown_23:	�

unknown_24:	�

unknown_25:	�

unknown_26:	�@

unknown_27:@

unknown_28:@

unknown_29:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCall
inputs_ageinputs_genderinputs_genreinputs_historyinputs_item_idinputs_occupationinputs_release_dateinputs_zip_codeunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29*2
Tin+
)2'				*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*9
_read_only_resource_inputs
 !"#$%&*0
config_proto 

CPU

GPU2*0J 8� *H
fCRA
?__inference_deep_layer_call_and_return_conditional_losses_509392
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
#
_output_shapes
:���������
$
_user_specified_name
inputs/age:RN
#
_output_shapes
:���������
'
_user_specified_nameinputs/gender:UQ
'
_output_shapes
:���������
&
_user_specified_nameinputs/genre:XT
(
_output_shapes
:����������
(
_user_specified_nameinputs/history:SO
#
_output_shapes
:���������
(
_user_specified_nameinputs/item_id:VR
#
_output_shapes
:���������
+
_user_specified_nameinputs/occupation:XT
#
_output_shapes
:���������
-
_user_specified_nameinputs/release_date:TP
#
_output_shapes
:���������
)
_user_specified_nameinputs/zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
a
B__inference_dropout_layer_call_and_return_conditional_losses_51100

inputs
identity�c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *���?2
dropout/Constt
dropout/MulMulinputsdropout/Const:output:0*
T0*(
_output_shapes
:����������2
dropout/MulT
dropout/ShapeShapeinputs*
T0*
_output_shapes
:2
dropout/Shape�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L=2
dropout/GreaterEqual/y�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������2
dropout/GreaterEqual�
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������2
dropout/Cast{
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*(
_output_shapes
:����������2
dropout/Mul_1f
IdentityIdentitydropout/Mul_1:z:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�*
�
N__inference_batch_normalization_layer_call_and_return_conditional_losses_50290

inputs6
'assignmovingavg_readvariableop_resource:	�8
)assignmovingavg_1_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�0
!batchnorm_readvariableop_resource:	�
identity��AssignMovingAvg�AssignMovingAvg/ReadVariableOp�AssignMovingAvg_1� AssignMovingAvg_1/ReadVariableOp�batchnorm/ReadVariableOp�batchnorm/mul/ReadVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/mean}
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes
:	�2
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*(
_output_shapes
:����������2
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze_1s
AssignMovingAvg/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOp'assignmovingavg_readvariableop_resource*
_output_shapes	
:�*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/mul�
AssignMovingAvgAssignSubVariableOp'assignmovingavg_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvgw
AssignMovingAvg_1/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp)assignmovingavg_1_readvariableop_resource*
_output_shapes	
:�*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/mul�
AssignMovingAvg_1AssignSubVariableOp)assignmovingavg_1_readvariableop_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvg_1g
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1|
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^AssignMovingAvg^AssignMovingAvg/ReadVariableOp^AssignMovingAvg_1!^AssignMovingAvg_1/ReadVariableOp^batchnorm/ReadVariableOp^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 2"
AssignMovingAvgAssignMovingAvg2@
AssignMovingAvg/ReadVariableOpAssignMovingAvg/ReadVariableOp2&
AssignMovingAvg_1AssignMovingAvg_12D
 AssignMovingAvg_1/ReadVariableOp AssignMovingAvg_1/ReadVariableOp24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp2<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
.
__inference__initializer_52870
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�	
�
F__inference_embedding_1_layer_call_and_return_conditional_losses_52412

inputs	)
embedding_lookup_52406:	�)
identity��embedding_lookup�
embedding_lookupResourceGatherembedding_lookup_52406inputs",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*)
_class
loc:@embedding_lookup/52406*'
_output_shapes
:���������)*
dtype02
embedding_lookup�
embedding_lookup/IdentityIdentityembedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*)
_class
loc:@embedding_lookup/52406*'
_output_shapes
:���������)2
embedding_lookup/Identity�
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������)2
embedding_lookup/Identity_1
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*'
_output_shapes
:���������)2

Identitya
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*$
_input_shapes
:���������: 2$
embedding_lookupembedding_lookup:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs
�
Q
__inference__creator_52895
identity: ��string_lookup_1_index_table�
string_lookup_1_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_nametable_25836*
value_dtype0	2
string_lookup_1_index_tablet
IdentityIdentity*string_lookup_1_index_table:table_handle:0^NoOp*
T0*
_output_shapes
: 2

Identityl
NoOpNoOp^string_lookup_1_index_table*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 2:
string_lookup_1_index_tablestring_lookup_1_index_table
�	
�
D__inference_embedding_layer_call_and_return_conditional_losses_50775

inputs	)
embedding_lookup_50769:	�
identity��embedding_lookup�
embedding_lookupResourceGatherembedding_lookup_50769inputs",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*)
_class
loc:@embedding_lookup/50769*'
_output_shapes
:���������*
dtype02
embedding_lookup�
embedding_lookup/IdentityIdentityembedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*)
_class
loc:@embedding_lookup/50769*'
_output_shapes
:���������2
embedding_lookup/Identity�
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������2
embedding_lookup/Identity_1
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identitya
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*$
_input_shapes
:���������: 2$
embedding_lookupembedding_lookup:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs
�
,
__inference__destroyer_52860
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
ѥ
�
?__inference_deep_layer_call_and_return_conditional_losses_51628
age

gender	
genre
history
item_id

occupation
release_date
zip_codeH
Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleI
Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value	G
Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handleH
Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value	"
embedding_51563:	�
dense_51566:	�)$
embedding_1_51571:	�)(
batch_normalization_51577:	�(
batch_normalization_51579:	�(
batch_normalization_51581:	�(
batch_normalization_51583:	�!
dense_1_51586:
��
dense_1_51588:	�*
batch_normalization_1_51592:	�*
batch_normalization_1_51594:	�*
batch_normalization_1_51596:	�*
batch_normalization_1_51598:	�!
dense_2_51601:
��
dense_2_51603:	�*
batch_normalization_2_51607:	�*
batch_normalization_2_51609:	�*
batch_normalization_2_51611:	�*
batch_normalization_2_51613:	� 
dense_3_51616:	�@
dense_3_51618:@
dense_4_51622:@
dense_4_51624:
identity��+batch_normalization/StatefulPartitionedCall�-batch_normalization_1/StatefulPartitionedCall�-batch_normalization_2/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�dense_2/StatefulPartitionedCall�dense_3/StatefulPartitionedCall�dense_4/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�#embedding_1/StatefulPartitionedCall�7integer_lookup/None_lookup_table_find/LookupTableFindV2�6string_lookup/None_lookup_table_find/LookupTableFindV2�8string_lookup_1/None_lookup_table_find/LookupTableFindV2�8string_lookup_2/None_lookup_table_find/LookupTableFindV2x
integer_lookup/CastCastitem_id*

DstT0	*

SrcT0*#
_output_shapes
:���������2
integer_lookup/Cast�
7integer_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleinteger_lookup/Cast:y:0Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0	*

Tout0	*#
_output_shapes
:���������29
7integer_lookup/None_lookup_table_find/LookupTableFindV2�
integer_lookup/IdentityIdentity@integer_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
integer_lookup/Identity�
8string_lookup_2/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handlezip_codeFstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:���������2:
8string_lookup_2/None_lookup_table_find/LookupTableFindV2�
string_lookup_2/IdentityIdentityAstring_lookup_2/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
string_lookup_2/Identity�
tf.expand_dims_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_1/ExpandDims/dim�
tf.expand_dims_1/ExpandDims
ExpandDimsgender(tf.expand_dims_1/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_1/ExpandDims�
tf.expand_dims/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2
tf.expand_dims/ExpandDims/dim�
tf.expand_dims/ExpandDims
ExpandDims
occupation&tf.expand_dims/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims/ExpandDims�
6string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handle"tf.expand_dims/ExpandDims:output:0Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������28
6string_lookup/None_lookup_table_find/LookupTableFindV2�
string_lookup/bincount/ShapeShape?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2
string_lookup/bincount/Shape�
string_lookup/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
string_lookup/bincount/Const�
string_lookup/bincount/ProdProd%string_lookup/bincount/Shape:output:0%string_lookup/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup/bincount/Prod�
 string_lookup/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2"
 string_lookup/bincount/Greater/y�
string_lookup/bincount/GreaterGreater$string_lookup/bincount/Prod:output:0)string_lookup/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2 
string_lookup/bincount/Greater�
string_lookup/bincount/CastCast"string_lookup/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup/bincount/Cast�
string_lookup/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2 
string_lookup/bincount/Const_1�
string_lookup/bincount/MaxMax?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0'string_lookup/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/Max~
string_lookup/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2
string_lookup/bincount/add/y�
string_lookup/bincount/addAddV2#string_lookup/bincount/Max:output:0%string_lookup/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/add�
string_lookup/bincount/mulMulstring_lookup/bincount/Cast:y:0string_lookup/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/mul�
 string_lookup/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/minlength�
string_lookup/bincount/MaximumMaximum)string_lookup/bincount/minlength:output:0string_lookup/bincount/mul:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Maximum�
 string_lookup/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/maxlength�
string_lookup/bincount/MinimumMinimum)string_lookup/bincount/maxlength:output:0"string_lookup/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Minimum�
string_lookup/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2 
string_lookup/bincount/Const_2�
$string_lookup/bincount/DenseBincountDenseBincount?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0"string_lookup/bincount/Minimum:z:0'string_lookup/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2&
$string_lookup/bincount/DenseBincount�
8string_lookup_1/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handle$tf.expand_dims_1/ExpandDims:output:0Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������2:
8string_lookup_1/None_lookup_table_find/LookupTableFindV2�
string_lookup_1/bincount/ShapeShapeAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2 
string_lookup_1/bincount/Shape�
string_lookup_1/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2 
string_lookup_1/bincount/Const�
string_lookup_1/bincount/ProdProd'string_lookup_1/bincount/Shape:output:0'string_lookup_1/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup_1/bincount/Prod�
"string_lookup_1/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2$
"string_lookup_1/bincount/Greater/y�
 string_lookup_1/bincount/GreaterGreater&string_lookup_1/bincount/Prod:output:0+string_lookup_1/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2"
 string_lookup_1/bincount/Greater�
string_lookup_1/bincount/CastCast$string_lookup_1/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup_1/bincount/Cast�
 string_lookup_1/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2"
 string_lookup_1/bincount/Const_1�
string_lookup_1/bincount/MaxMaxAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0)string_lookup_1/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/Max�
string_lookup_1/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2 
string_lookup_1/bincount/add/y�
string_lookup_1/bincount/addAddV2%string_lookup_1/bincount/Max:output:0'string_lookup_1/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/add�
string_lookup_1/bincount/mulMul!string_lookup_1/bincount/Cast:y:0 string_lookup_1/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/mul�
"string_lookup_1/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/minlength�
 string_lookup_1/bincount/MaximumMaximum+string_lookup_1/bincount/minlength:output:0 string_lookup_1/bincount/mul:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Maximum�
"string_lookup_1/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/maxlength�
 string_lookup_1/bincount/MinimumMinimum+string_lookup_1/bincount/maxlength:output:0$string_lookup_1/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Minimum�
 string_lookup_1/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2"
 string_lookup_1/bincount/Const_2�
&string_lookup_1/bincount/DenseBincountDenseBincountAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0$string_lookup_1/bincount/Minimum:z:0)string_lookup_1/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2(
&string_lookup_1/bincount/DenseBincount�
!embedding/StatefulPartitionedCallStatefulPartitionedCall!string_lookup_2/Identity:output:0embedding_51563*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_507752#
!embedding/StatefulPartitionedCall�
dense/StatefulPartitionedCallStatefulPartitionedCallhistorydense_51566*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_507862
dense/StatefulPartitionedCall�
tf.expand_dims_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_2/ExpandDims/dim�
tf.expand_dims_2/ExpandDims
ExpandDimsage(tf.expand_dims_2/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_2/ExpandDims�
#embedding_1/StatefulPartitionedCallStatefulPartitionedCall integer_lookup/Identity:output:0embedding_1_51571*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_embedding_1_layer_call_and_return_conditional_losses_508012%
#embedding_1/StatefulPartitionedCall�
tf.expand_dims_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_3/ExpandDims/dim�
tf.expand_dims_3/ExpandDims
ExpandDimsrelease_date(tf.expand_dims_3/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_3/ExpandDims�
concatenate/PartitionedCallPartitionedCall-string_lookup/bincount/DenseBincount:output:0genre/string_lookup_1/bincount/DenseBincount:output:0*embedding/StatefulPartitionedCall:output:0&dense/StatefulPartitionedCall:output:0$tf.expand_dims_2/ExpandDims:output:0,embedding_1/StatefulPartitionedCall:output:0$tf.expand_dims_3/ExpandDims:output:0*
Tin

2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_concatenate_layer_call_and_return_conditional_losses_508202
concatenate/PartitionedCall�
+batch_normalization/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0batch_normalization_51577batch_normalization_51579batch_normalization_51581batch_normalization_51583*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_batch_normalization_layer_call_and_return_conditional_losses_502302-
+batch_normalization/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCall4batch_normalization/StatefulPartitionedCall:output:0dense_1_51586dense_1_51588*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_508422!
dense_1/StatefulPartitionedCall�
dropout/PartitionedCallPartitionedCall(dense_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_508532
dropout/PartitionedCall�
-batch_normalization_1/StatefulPartitionedCallStatefulPartitionedCall dropout/PartitionedCall:output:0batch_normalization_1_51592batch_normalization_1_51594batch_normalization_1_51596batch_normalization_1_51598*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_503922/
-batch_normalization_1/StatefulPartitionedCall�
dense_2/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_1/StatefulPartitionedCall:output:0dense_2_51601dense_2_51603*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_2_layer_call_and_return_conditional_losses_508752!
dense_2/StatefulPartitionedCall�
dropout_1/PartitionedCallPartitionedCall(dense_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_508862
dropout_1/PartitionedCall�
-batch_normalization_2/StatefulPartitionedCallStatefulPartitionedCall"dropout_1/PartitionedCall:output:0batch_normalization_2_51607batch_normalization_2_51609batch_normalization_2_51611batch_normalization_2_51613*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_505542/
-batch_normalization_2/StatefulPartitionedCall�
dense_3/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_2/StatefulPartitionedCall:output:0dense_3_51616dense_3_51618*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_3_layer_call_and_return_conditional_losses_509082!
dense_3/StatefulPartitionedCall�
dropout_2/PartitionedCallPartitionedCall(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_2_layer_call_and_return_conditional_losses_509192
dropout_2/PartitionedCall�
dense_4/StatefulPartitionedCallStatefulPartitionedCall"dropout_2/PartitionedCall:output:0dense_4_51622dense_4_51624*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_4_layer_call_and_return_conditional_losses_509322!
dense_4/StatefulPartitionedCall�
IdentityIdentity(dense_4/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp,^batch_normalization/StatefulPartitionedCall.^batch_normalization_1/StatefulPartitionedCall.^batch_normalization_2/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall"^embedding/StatefulPartitionedCall$^embedding_1/StatefulPartitionedCall8^integer_lookup/None_lookup_table_find/LookupTableFindV27^string_lookup/None_lookup_table_find/LookupTableFindV29^string_lookup_1/None_lookup_table_find/LookupTableFindV29^string_lookup_2/None_lookup_table_find/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2Z
+batch_normalization/StatefulPartitionedCall+batch_normalization/StatefulPartitionedCall2^
-batch_normalization_1/StatefulPartitionedCall-batch_normalization_1/StatefulPartitionedCall2^
-batch_normalization_2/StatefulPartitionedCall-batch_normalization_2/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall2J
#embedding_1/StatefulPartitionedCall#embedding_1/StatefulPartitionedCall2r
7integer_lookup/None_lookup_table_find/LookupTableFindV27integer_lookup/None_lookup_table_find/LookupTableFindV22p
6string_lookup/None_lookup_table_find/LookupTableFindV26string_lookup/None_lookup_table_find/LookupTableFindV22t
8string_lookup_1/None_lookup_table_find/LookupTableFindV28string_lookup_1/None_lookup_table_find/LookupTableFindV22t
8string_lookup_2/None_lookup_table_find/LookupTableFindV28string_lookup_2/None_lookup_table_find/LookupTableFindV2:H D
#
_output_shapes
:���������

_user_specified_nameage:KG
#
_output_shapes
:���������
 
_user_specified_namegender:NJ
'
_output_shapes
:���������

_user_specified_namegenre:QM
(
_output_shapes
:����������
!
_user_specified_name	history:LH
#
_output_shapes
:���������
!
_user_specified_name	item_id:OK
#
_output_shapes
:���������
$
_user_specified_name
occupation:QM
#
_output_shapes
:���������
&
_user_specified_namerelease_date:MI
#
_output_shapes
:���������
"
_user_specified_name
zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
*
__inference_<lambda>_53018
identityS
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
�
F__inference_concatenate_layer_call_and_return_conditional_losses_50820

inputs
inputs_1
inputs_2
inputs_3
inputs_4
inputs_5
inputs_6
inputs_7
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputsinputs_1inputs_2inputs_3inputs_4inputs_5inputs_6inputs_7concat/axis:output:0*
N*
T0*(
_output_shapes
:����������2
concatd
IdentityIdentityconcat:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:���������:���������):���������:���������):���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������)
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������)
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs
��
�
?__inference_deep_layer_call_and_return_conditional_losses_51361

inputs
inputs_1
inputs_2
inputs_3
inputs_4
inputs_5
inputs_6
inputs_7H
Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleI
Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value	G
Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handleH
Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value	"
embedding_51296:	�
dense_51299:	�)$
embedding_1_51304:	�)(
batch_normalization_51310:	�(
batch_normalization_51312:	�(
batch_normalization_51314:	�(
batch_normalization_51316:	�!
dense_1_51319:
��
dense_1_51321:	�*
batch_normalization_1_51325:	�*
batch_normalization_1_51327:	�*
batch_normalization_1_51329:	�*
batch_normalization_1_51331:	�!
dense_2_51334:
��
dense_2_51336:	�*
batch_normalization_2_51340:	�*
batch_normalization_2_51342:	�*
batch_normalization_2_51344:	�*
batch_normalization_2_51346:	� 
dense_3_51349:	�@
dense_3_51351:@
dense_4_51355:@
dense_4_51357:
identity��+batch_normalization/StatefulPartitionedCall�-batch_normalization_1/StatefulPartitionedCall�-batch_normalization_2/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�dense_2/StatefulPartitionedCall�dense_3/StatefulPartitionedCall�dense_4/StatefulPartitionedCall�dropout/StatefulPartitionedCall�!dropout_1/StatefulPartitionedCall�!dropout_2/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�#embedding_1/StatefulPartitionedCall�7integer_lookup/None_lookup_table_find/LookupTableFindV2�6string_lookup/None_lookup_table_find/LookupTableFindV2�8string_lookup_1/None_lookup_table_find/LookupTableFindV2�8string_lookup_2/None_lookup_table_find/LookupTableFindV2y
integer_lookup/CastCastinputs_4*

DstT0	*

SrcT0*#
_output_shapes
:���������2
integer_lookup/Cast�
7integer_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleinteger_lookup/Cast:y:0Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0	*

Tout0	*#
_output_shapes
:���������29
7integer_lookup/None_lookup_table_find/LookupTableFindV2�
integer_lookup/IdentityIdentity@integer_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
integer_lookup/Identity�
8string_lookup_2/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleinputs_7Fstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:���������2:
8string_lookup_2/None_lookup_table_find/LookupTableFindV2�
string_lookup_2/IdentityIdentityAstring_lookup_2/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
string_lookup_2/Identity�
tf.expand_dims_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_1/ExpandDims/dim�
tf.expand_dims_1/ExpandDims
ExpandDimsinputs_1(tf.expand_dims_1/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_1/ExpandDims�
tf.expand_dims/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2
tf.expand_dims/ExpandDims/dim�
tf.expand_dims/ExpandDims
ExpandDimsinputs_5&tf.expand_dims/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims/ExpandDims�
6string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handle"tf.expand_dims/ExpandDims:output:0Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������28
6string_lookup/None_lookup_table_find/LookupTableFindV2�
string_lookup/bincount/ShapeShape?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2
string_lookup/bincount/Shape�
string_lookup/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
string_lookup/bincount/Const�
string_lookup/bincount/ProdProd%string_lookup/bincount/Shape:output:0%string_lookup/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup/bincount/Prod�
 string_lookup/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2"
 string_lookup/bincount/Greater/y�
string_lookup/bincount/GreaterGreater$string_lookup/bincount/Prod:output:0)string_lookup/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2 
string_lookup/bincount/Greater�
string_lookup/bincount/CastCast"string_lookup/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup/bincount/Cast�
string_lookup/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2 
string_lookup/bincount/Const_1�
string_lookup/bincount/MaxMax?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0'string_lookup/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/Max~
string_lookup/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2
string_lookup/bincount/add/y�
string_lookup/bincount/addAddV2#string_lookup/bincount/Max:output:0%string_lookup/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/add�
string_lookup/bincount/mulMulstring_lookup/bincount/Cast:y:0string_lookup/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/mul�
 string_lookup/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/minlength�
string_lookup/bincount/MaximumMaximum)string_lookup/bincount/minlength:output:0string_lookup/bincount/mul:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Maximum�
 string_lookup/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/maxlength�
string_lookup/bincount/MinimumMinimum)string_lookup/bincount/maxlength:output:0"string_lookup/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Minimum�
string_lookup/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2 
string_lookup/bincount/Const_2�
$string_lookup/bincount/DenseBincountDenseBincount?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0"string_lookup/bincount/Minimum:z:0'string_lookup/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2&
$string_lookup/bincount/DenseBincount�
8string_lookup_1/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handle$tf.expand_dims_1/ExpandDims:output:0Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������2:
8string_lookup_1/None_lookup_table_find/LookupTableFindV2�
string_lookup_1/bincount/ShapeShapeAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2 
string_lookup_1/bincount/Shape�
string_lookup_1/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2 
string_lookup_1/bincount/Const�
string_lookup_1/bincount/ProdProd'string_lookup_1/bincount/Shape:output:0'string_lookup_1/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup_1/bincount/Prod�
"string_lookup_1/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2$
"string_lookup_1/bincount/Greater/y�
 string_lookup_1/bincount/GreaterGreater&string_lookup_1/bincount/Prod:output:0+string_lookup_1/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2"
 string_lookup_1/bincount/Greater�
string_lookup_1/bincount/CastCast$string_lookup_1/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup_1/bincount/Cast�
 string_lookup_1/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2"
 string_lookup_1/bincount/Const_1�
string_lookup_1/bincount/MaxMaxAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0)string_lookup_1/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/Max�
string_lookup_1/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2 
string_lookup_1/bincount/add/y�
string_lookup_1/bincount/addAddV2%string_lookup_1/bincount/Max:output:0'string_lookup_1/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/add�
string_lookup_1/bincount/mulMul!string_lookup_1/bincount/Cast:y:0 string_lookup_1/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/mul�
"string_lookup_1/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/minlength�
 string_lookup_1/bincount/MaximumMaximum+string_lookup_1/bincount/minlength:output:0 string_lookup_1/bincount/mul:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Maximum�
"string_lookup_1/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/maxlength�
 string_lookup_1/bincount/MinimumMinimum+string_lookup_1/bincount/maxlength:output:0$string_lookup_1/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Minimum�
 string_lookup_1/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2"
 string_lookup_1/bincount/Const_2�
&string_lookup_1/bincount/DenseBincountDenseBincountAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0$string_lookup_1/bincount/Minimum:z:0)string_lookup_1/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2(
&string_lookup_1/bincount/DenseBincount�
!embedding/StatefulPartitionedCallStatefulPartitionedCall!string_lookup_2/Identity:output:0embedding_51296*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_507752#
!embedding/StatefulPartitionedCall�
dense/StatefulPartitionedCallStatefulPartitionedCallinputs_3dense_51299*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_507862
dense/StatefulPartitionedCall�
tf.expand_dims_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_2/ExpandDims/dim�
tf.expand_dims_2/ExpandDims
ExpandDimsinputs(tf.expand_dims_2/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_2/ExpandDims�
#embedding_1/StatefulPartitionedCallStatefulPartitionedCall integer_lookup/Identity:output:0embedding_1_51304*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_embedding_1_layer_call_and_return_conditional_losses_508012%
#embedding_1/StatefulPartitionedCall�
tf.expand_dims_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_3/ExpandDims/dim�
tf.expand_dims_3/ExpandDims
ExpandDimsinputs_6(tf.expand_dims_3/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_3/ExpandDims�
concatenate/PartitionedCallPartitionedCall-string_lookup/bincount/DenseBincount:output:0inputs_2/string_lookup_1/bincount/DenseBincount:output:0*embedding/StatefulPartitionedCall:output:0&dense/StatefulPartitionedCall:output:0$tf.expand_dims_2/ExpandDims:output:0,embedding_1/StatefulPartitionedCall:output:0$tf.expand_dims_3/ExpandDims:output:0*
Tin

2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_concatenate_layer_call_and_return_conditional_losses_508202
concatenate/PartitionedCall�
+batch_normalization/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0batch_normalization_51310batch_normalization_51312batch_normalization_51314batch_normalization_51316*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_batch_normalization_layer_call_and_return_conditional_losses_502902-
+batch_normalization/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCall4batch_normalization/StatefulPartitionedCall:output:0dense_1_51319dense_1_51321*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_508422!
dense_1/StatefulPartitionedCall�
dropout/StatefulPartitionedCallStatefulPartitionedCall(dense_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_511002!
dropout/StatefulPartitionedCall�
-batch_normalization_1/StatefulPartitionedCallStatefulPartitionedCall(dropout/StatefulPartitionedCall:output:0batch_normalization_1_51325batch_normalization_1_51327batch_normalization_1_51329batch_normalization_1_51331*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_504522/
-batch_normalization_1/StatefulPartitionedCall�
dense_2/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_1/StatefulPartitionedCall:output:0dense_2_51334dense_2_51336*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_2_layer_call_and_return_conditional_losses_508752!
dense_2/StatefulPartitionedCall�
!dropout_1/StatefulPartitionedCallStatefulPartitionedCall(dense_2/StatefulPartitionedCall:output:0 ^dropout/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_510672#
!dropout_1/StatefulPartitionedCall�
-batch_normalization_2/StatefulPartitionedCallStatefulPartitionedCall*dropout_1/StatefulPartitionedCall:output:0batch_normalization_2_51340batch_normalization_2_51342batch_normalization_2_51344batch_normalization_2_51346*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_506142/
-batch_normalization_2/StatefulPartitionedCall�
dense_3/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_2/StatefulPartitionedCall:output:0dense_3_51349dense_3_51351*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_3_layer_call_and_return_conditional_losses_509082!
dense_3/StatefulPartitionedCall�
!dropout_2/StatefulPartitionedCallStatefulPartitionedCall(dense_3/StatefulPartitionedCall:output:0"^dropout_1/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_2_layer_call_and_return_conditional_losses_510342#
!dropout_2/StatefulPartitionedCall�
dense_4/StatefulPartitionedCallStatefulPartitionedCall*dropout_2/StatefulPartitionedCall:output:0dense_4_51355dense_4_51357*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_4_layer_call_and_return_conditional_losses_509322!
dense_4/StatefulPartitionedCall�
IdentityIdentity(dense_4/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp,^batch_normalization/StatefulPartitionedCall.^batch_normalization_1/StatefulPartitionedCall.^batch_normalization_2/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall ^dropout/StatefulPartitionedCall"^dropout_1/StatefulPartitionedCall"^dropout_2/StatefulPartitionedCall"^embedding/StatefulPartitionedCall$^embedding_1/StatefulPartitionedCall8^integer_lookup/None_lookup_table_find/LookupTableFindV27^string_lookup/None_lookup_table_find/LookupTableFindV29^string_lookup_1/None_lookup_table_find/LookupTableFindV29^string_lookup_2/None_lookup_table_find/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2Z
+batch_normalization/StatefulPartitionedCall+batch_normalization/StatefulPartitionedCall2^
-batch_normalization_1/StatefulPartitionedCall-batch_normalization_1/StatefulPartitionedCall2^
-batch_normalization_2/StatefulPartitionedCall-batch_normalization_2/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2B
dropout/StatefulPartitionedCalldropout/StatefulPartitionedCall2F
!dropout_1/StatefulPartitionedCall!dropout_1/StatefulPartitionedCall2F
!dropout_2/StatefulPartitionedCall!dropout_2/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall2J
#embedding_1/StatefulPartitionedCall#embedding_1/StatefulPartitionedCall2r
7integer_lookup/None_lookup_table_find/LookupTableFindV27integer_lookup/None_lookup_table_find/LookupTableFindV22p
6string_lookup/None_lookup_table_find/LookupTableFindV26string_lookup/None_lookup_table_find/LookupTableFindV22t
8string_lookup_1/None_lookup_table_find/LookupTableFindV28string_lookup_1/None_lookup_table_find/LookupTableFindV22t
8string_lookup_2/None_lookup_table_find/LookupTableFindV28string_lookup_2/None_lookup_table_find/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
$__inference_deep_layer_call_fn_51004
age

gender	
genre
history
item_id

occupation
release_date
zip_code
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3
	unknown_4	
	unknown_5
	unknown_6	
	unknown_7:	�
	unknown_8:	�)
	unknown_9:	�)

unknown_10:	�

unknown_11:	�

unknown_12:	�

unknown_13:	�

unknown_14:
��

unknown_15:	�

unknown_16:	�

unknown_17:	�

unknown_18:	�

unknown_19:	�

unknown_20:
��

unknown_21:	�

unknown_22:	�

unknown_23:	�

unknown_24:	�

unknown_25:	�

unknown_26:	�@

unknown_27:@

unknown_28:@

unknown_29:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallagegendergenrehistoryitem_id
occupationrelease_datezip_codeunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29*2
Tin+
)2'				*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*9
_read_only_resource_inputs
 !"#$%&*0
config_proto 

CPU

GPU2*0J 8� *H
fCRA
?__inference_deep_layer_call_and_return_conditional_losses_509392
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:H D
#
_output_shapes
:���������

_user_specified_nameage:KG
#
_output_shapes
:���������
 
_user_specified_namegender:NJ
'
_output_shapes
:���������

_user_specified_namegenre:QM
(
_output_shapes
:����������
!
_user_specified_name	history:LH
#
_output_shapes
:���������
!
_user_specified_name	item_id:OK
#
_output_shapes
:���������
$
_user_specified_name
occupation:QM
#
_output_shapes
:���������
&
_user_specified_namerelease_date:MI
#
_output_shapes
:���������
"
_user_specified_name
zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
~
)__inference_embedding_layer_call_fn_52389

inputs	
unknown:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_507752
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*$
_input_shapes
:���������: 22
StatefulPartitionedCallStatefulPartitionedCall:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs
�
O
__inference__creator_52880
identity: ��string_lookup_index_table�
string_lookup_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_nametable_25797*
value_dtype0	2
string_lookup_index_tabler
IdentityIdentity(string_lookup_index_table:table_handle:0^NoOp*
T0*
_output_shapes
: 2

Identityj
NoOpNoOp^string_lookup_index_table*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 26
string_lookup_index_tablestring_lookup_index_table
��
�
 __inference__wrapped_model_50206
age

gender	
genre
history
item_id

occupation
release_date
zip_codeM
Ideep_integer_lookup_none_lookup_table_find_lookuptablefindv2_table_handleN
Jdeep_integer_lookup_none_lookup_table_find_lookuptablefindv2_default_value	N
Jdeep_string_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleO
Kdeep_string_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value	L
Hdeep_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handleM
Ideep_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value	N
Jdeep_string_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handleO
Kdeep_string_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value	8
%deep_embedding_embedding_lookup_50107:	�<
)deep_dense_matmul_readvariableop_resource:	�):
'deep_embedding_1_embedding_lookup_50117:	�)I
:deep_batch_normalization_batchnorm_readvariableop_resource:	�M
>deep_batch_normalization_batchnorm_mul_readvariableop_resource:	�K
<deep_batch_normalization_batchnorm_readvariableop_1_resource:	�K
<deep_batch_normalization_batchnorm_readvariableop_2_resource:	�?
+deep_dense_1_matmul_readvariableop_resource:
��;
,deep_dense_1_biasadd_readvariableop_resource:	�K
<deep_batch_normalization_1_batchnorm_readvariableop_resource:	�O
@deep_batch_normalization_1_batchnorm_mul_readvariableop_resource:	�M
>deep_batch_normalization_1_batchnorm_readvariableop_1_resource:	�M
>deep_batch_normalization_1_batchnorm_readvariableop_2_resource:	�?
+deep_dense_2_matmul_readvariableop_resource:
��;
,deep_dense_2_biasadd_readvariableop_resource:	�K
<deep_batch_normalization_2_batchnorm_readvariableop_resource:	�O
@deep_batch_normalization_2_batchnorm_mul_readvariableop_resource:	�M
>deep_batch_normalization_2_batchnorm_readvariableop_1_resource:	�M
>deep_batch_normalization_2_batchnorm_readvariableop_2_resource:	�>
+deep_dense_3_matmul_readvariableop_resource:	�@:
,deep_dense_3_biasadd_readvariableop_resource:@=
+deep_dense_4_matmul_readvariableop_resource:@:
,deep_dense_4_biasadd_readvariableop_resource:
identity��1deep/batch_normalization/batchnorm/ReadVariableOp�3deep/batch_normalization/batchnorm/ReadVariableOp_1�3deep/batch_normalization/batchnorm/ReadVariableOp_2�5deep/batch_normalization/batchnorm/mul/ReadVariableOp�3deep/batch_normalization_1/batchnorm/ReadVariableOp�5deep/batch_normalization_1/batchnorm/ReadVariableOp_1�5deep/batch_normalization_1/batchnorm/ReadVariableOp_2�7deep/batch_normalization_1/batchnorm/mul/ReadVariableOp�3deep/batch_normalization_2/batchnorm/ReadVariableOp�5deep/batch_normalization_2/batchnorm/ReadVariableOp_1�5deep/batch_normalization_2/batchnorm/ReadVariableOp_2�7deep/batch_normalization_2/batchnorm/mul/ReadVariableOp� deep/dense/MatMul/ReadVariableOp�#deep/dense_1/BiasAdd/ReadVariableOp�"deep/dense_1/MatMul/ReadVariableOp�#deep/dense_2/BiasAdd/ReadVariableOp�"deep/dense_2/MatMul/ReadVariableOp�#deep/dense_3/BiasAdd/ReadVariableOp�"deep/dense_3/MatMul/ReadVariableOp�#deep/dense_4/BiasAdd/ReadVariableOp�"deep/dense_4/MatMul/ReadVariableOp�deep/embedding/embedding_lookup�!deep/embedding_1/embedding_lookup�<deep/integer_lookup/None_lookup_table_find/LookupTableFindV2�;deep/string_lookup/None_lookup_table_find/LookupTableFindV2�=deep/string_lookup_1/None_lookup_table_find/LookupTableFindV2�=deep/string_lookup_2/None_lookup_table_find/LookupTableFindV2�
deep/integer_lookup/CastCastitem_id*

DstT0	*

SrcT0*#
_output_shapes
:���������2
deep/integer_lookup/Cast�
<deep/integer_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Ideep_integer_lookup_none_lookup_table_find_lookuptablefindv2_table_handledeep/integer_lookup/Cast:y:0Jdeep_integer_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0	*

Tout0	*#
_output_shapes
:���������2>
<deep/integer_lookup/None_lookup_table_find/LookupTableFindV2�
deep/integer_lookup/IdentityIdentityEdeep/integer_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
deep/integer_lookup/Identity�
=deep/string_lookup_2/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Jdeep_string_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handlezip_codeKdeep_string_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:���������2?
=deep/string_lookup_2/None_lookup_table_find/LookupTableFindV2�
deep/string_lookup_2/IdentityIdentityFdeep/string_lookup_2/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
deep/string_lookup_2/Identity�
$deep/tf.expand_dims_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2&
$deep/tf.expand_dims_1/ExpandDims/dim�
 deep/tf.expand_dims_1/ExpandDims
ExpandDimsgender-deep/tf.expand_dims_1/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2"
 deep/tf.expand_dims_1/ExpandDims�
"deep/tf.expand_dims/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2$
"deep/tf.expand_dims/ExpandDims/dim�
deep/tf.expand_dims/ExpandDims
ExpandDims
occupation+deep/tf.expand_dims/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2 
deep/tf.expand_dims/ExpandDims�
;deep/string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Hdeep_string_lookup_none_lookup_table_find_lookuptablefindv2_table_handle'deep/tf.expand_dims/ExpandDims:output:0Ideep_string_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������2=
;deep/string_lookup/None_lookup_table_find/LookupTableFindV2�
!deep/string_lookup/bincount/ShapeShapeDdeep/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2#
!deep/string_lookup/bincount/Shape�
!deep/string_lookup/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2#
!deep/string_lookup/bincount/Const�
 deep/string_lookup/bincount/ProdProd*deep/string_lookup/bincount/Shape:output:0*deep/string_lookup/bincount/Const:output:0*
T0*
_output_shapes
: 2"
 deep/string_lookup/bincount/Prod�
%deep/string_lookup/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2'
%deep/string_lookup/bincount/Greater/y�
#deep/string_lookup/bincount/GreaterGreater)deep/string_lookup/bincount/Prod:output:0.deep/string_lookup/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2%
#deep/string_lookup/bincount/Greater�
 deep/string_lookup/bincount/CastCast'deep/string_lookup/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2"
 deep/string_lookup/bincount/Cast�
#deep/string_lookup/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2%
#deep/string_lookup/bincount/Const_1�
deep/string_lookup/bincount/MaxMaxDdeep/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0,deep/string_lookup/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2!
deep/string_lookup/bincount/Max�
!deep/string_lookup/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2#
!deep/string_lookup/bincount/add/y�
deep/string_lookup/bincount/addAddV2(deep/string_lookup/bincount/Max:output:0*deep/string_lookup/bincount/add/y:output:0*
T0	*
_output_shapes
: 2!
deep/string_lookup/bincount/add�
deep/string_lookup/bincount/mulMul$deep/string_lookup/bincount/Cast:y:0#deep/string_lookup/bincount/add:z:0*
T0	*
_output_shapes
: 2!
deep/string_lookup/bincount/mul�
%deep/string_lookup/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2'
%deep/string_lookup/bincount/minlength�
#deep/string_lookup/bincount/MaximumMaximum.deep/string_lookup/bincount/minlength:output:0#deep/string_lookup/bincount/mul:z:0*
T0	*
_output_shapes
: 2%
#deep/string_lookup/bincount/Maximum�
%deep/string_lookup/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2'
%deep/string_lookup/bincount/maxlength�
#deep/string_lookup/bincount/MinimumMinimum.deep/string_lookup/bincount/maxlength:output:0'deep/string_lookup/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2%
#deep/string_lookup/bincount/Minimum�
#deep/string_lookup/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2%
#deep/string_lookup/bincount/Const_2�
)deep/string_lookup/bincount/DenseBincountDenseBincountDdeep/string_lookup/None_lookup_table_find/LookupTableFindV2:values:0'deep/string_lookup/bincount/Minimum:z:0,deep/string_lookup/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2+
)deep/string_lookup/bincount/DenseBincount�
=deep/string_lookup_1/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Jdeep_string_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handle)deep/tf.expand_dims_1/ExpandDims:output:0Kdeep_string_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������2?
=deep/string_lookup_1/None_lookup_table_find/LookupTableFindV2�
#deep/string_lookup_1/bincount/ShapeShapeFdeep/string_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2%
#deep/string_lookup_1/bincount/Shape�
#deep/string_lookup_1/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2%
#deep/string_lookup_1/bincount/Const�
"deep/string_lookup_1/bincount/ProdProd,deep/string_lookup_1/bincount/Shape:output:0,deep/string_lookup_1/bincount/Const:output:0*
T0*
_output_shapes
: 2$
"deep/string_lookup_1/bincount/Prod�
'deep/string_lookup_1/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2)
'deep/string_lookup_1/bincount/Greater/y�
%deep/string_lookup_1/bincount/GreaterGreater+deep/string_lookup_1/bincount/Prod:output:00deep/string_lookup_1/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2'
%deep/string_lookup_1/bincount/Greater�
"deep/string_lookup_1/bincount/CastCast)deep/string_lookup_1/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2$
"deep/string_lookup_1/bincount/Cast�
%deep/string_lookup_1/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2'
%deep/string_lookup_1/bincount/Const_1�
!deep/string_lookup_1/bincount/MaxMaxFdeep/string_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0.deep/string_lookup_1/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2#
!deep/string_lookup_1/bincount/Max�
#deep/string_lookup_1/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2%
#deep/string_lookup_1/bincount/add/y�
!deep/string_lookup_1/bincount/addAddV2*deep/string_lookup_1/bincount/Max:output:0,deep/string_lookup_1/bincount/add/y:output:0*
T0	*
_output_shapes
: 2#
!deep/string_lookup_1/bincount/add�
!deep/string_lookup_1/bincount/mulMul&deep/string_lookup_1/bincount/Cast:y:0%deep/string_lookup_1/bincount/add:z:0*
T0	*
_output_shapes
: 2#
!deep/string_lookup_1/bincount/mul�
'deep/string_lookup_1/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2)
'deep/string_lookup_1/bincount/minlength�
%deep/string_lookup_1/bincount/MaximumMaximum0deep/string_lookup_1/bincount/minlength:output:0%deep/string_lookup_1/bincount/mul:z:0*
T0	*
_output_shapes
: 2'
%deep/string_lookup_1/bincount/Maximum�
'deep/string_lookup_1/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2)
'deep/string_lookup_1/bincount/maxlength�
%deep/string_lookup_1/bincount/MinimumMinimum0deep/string_lookup_1/bincount/maxlength:output:0)deep/string_lookup_1/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2'
%deep/string_lookup_1/bincount/Minimum�
%deep/string_lookup_1/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2'
%deep/string_lookup_1/bincount/Const_2�
+deep/string_lookup_1/bincount/DenseBincountDenseBincountFdeep/string_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0)deep/string_lookup_1/bincount/Minimum:z:0.deep/string_lookup_1/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2-
+deep/string_lookup_1/bincount/DenseBincount�
deep/embedding/embedding_lookupResourceGather%deep_embedding_embedding_lookup_50107&deep/string_lookup_2/Identity:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*8
_class.
,*loc:@deep/embedding/embedding_lookup/50107*'
_output_shapes
:���������*
dtype02!
deep/embedding/embedding_lookup�
(deep/embedding/embedding_lookup/IdentityIdentity(deep/embedding/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*8
_class.
,*loc:@deep/embedding/embedding_lookup/50107*'
_output_shapes
:���������2*
(deep/embedding/embedding_lookup/Identity�
*deep/embedding/embedding_lookup/Identity_1Identity1deep/embedding/embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������2,
*deep/embedding/embedding_lookup/Identity_1�
 deep/dense/MatMul/ReadVariableOpReadVariableOp)deep_dense_matmul_readvariableop_resource*
_output_shapes
:	�)*
dtype02"
 deep/dense/MatMul/ReadVariableOp�
deep/dense/MatMulMatMulhistory(deep/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������)2
deep/dense/MatMul�
$deep/tf.expand_dims_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2&
$deep/tf.expand_dims_2/ExpandDims/dim�
 deep/tf.expand_dims_2/ExpandDims
ExpandDimsage-deep/tf.expand_dims_2/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2"
 deep/tf.expand_dims_2/ExpandDims�
!deep/embedding_1/embedding_lookupResourceGather'deep_embedding_1_embedding_lookup_50117%deep/integer_lookup/Identity:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*:
_class0
.,loc:@deep/embedding_1/embedding_lookup/50117*'
_output_shapes
:���������)*
dtype02#
!deep/embedding_1/embedding_lookup�
*deep/embedding_1/embedding_lookup/IdentityIdentity*deep/embedding_1/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*:
_class0
.,loc:@deep/embedding_1/embedding_lookup/50117*'
_output_shapes
:���������)2,
*deep/embedding_1/embedding_lookup/Identity�
,deep/embedding_1/embedding_lookup/Identity_1Identity3deep/embedding_1/embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������)2.
,deep/embedding_1/embedding_lookup/Identity_1�
$deep/tf.expand_dims_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2&
$deep/tf.expand_dims_3/ExpandDims/dim�
 deep/tf.expand_dims_3/ExpandDims
ExpandDimsrelease_date-deep/tf.expand_dims_3/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2"
 deep/tf.expand_dims_3/ExpandDims~
deep/concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
deep/concatenate/concat/axis�
deep/concatenate/concatConcatV22deep/string_lookup/bincount/DenseBincount:output:0genre4deep/string_lookup_1/bincount/DenseBincount:output:03deep/embedding/embedding_lookup/Identity_1:output:0deep/dense/MatMul:product:0)deep/tf.expand_dims_2/ExpandDims:output:05deep/embedding_1/embedding_lookup/Identity_1:output:0)deep/tf.expand_dims_3/ExpandDims:output:0%deep/concatenate/concat/axis:output:0*
N*
T0*(
_output_shapes
:����������2
deep/concatenate/concat�
1deep/batch_normalization/batchnorm/ReadVariableOpReadVariableOp:deep_batch_normalization_batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype023
1deep/batch_normalization/batchnorm/ReadVariableOp�
(deep/batch_normalization/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2*
(deep/batch_normalization/batchnorm/add/y�
&deep/batch_normalization/batchnorm/addAddV29deep/batch_normalization/batchnorm/ReadVariableOp:value:01deep/batch_normalization/batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2(
&deep/batch_normalization/batchnorm/add�
(deep/batch_normalization/batchnorm/RsqrtRsqrt*deep/batch_normalization/batchnorm/add:z:0*
T0*
_output_shapes	
:�2*
(deep/batch_normalization/batchnorm/Rsqrt�
5deep/batch_normalization/batchnorm/mul/ReadVariableOpReadVariableOp>deep_batch_normalization_batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype027
5deep/batch_normalization/batchnorm/mul/ReadVariableOp�
&deep/batch_normalization/batchnorm/mulMul,deep/batch_normalization/batchnorm/Rsqrt:y:0=deep/batch_normalization/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2(
&deep/batch_normalization/batchnorm/mul�
(deep/batch_normalization/batchnorm/mul_1Mul deep/concatenate/concat:output:0*deep/batch_normalization/batchnorm/mul:z:0*
T0*(
_output_shapes
:����������2*
(deep/batch_normalization/batchnorm/mul_1�
3deep/batch_normalization/batchnorm/ReadVariableOp_1ReadVariableOp<deep_batch_normalization_batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype025
3deep/batch_normalization/batchnorm/ReadVariableOp_1�
(deep/batch_normalization/batchnorm/mul_2Mul;deep/batch_normalization/batchnorm/ReadVariableOp_1:value:0*deep/batch_normalization/batchnorm/mul:z:0*
T0*
_output_shapes	
:�2*
(deep/batch_normalization/batchnorm/mul_2�
3deep/batch_normalization/batchnorm/ReadVariableOp_2ReadVariableOp<deep_batch_normalization_batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype025
3deep/batch_normalization/batchnorm/ReadVariableOp_2�
&deep/batch_normalization/batchnorm/subSub;deep/batch_normalization/batchnorm/ReadVariableOp_2:value:0,deep/batch_normalization/batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2(
&deep/batch_normalization/batchnorm/sub�
(deep/batch_normalization/batchnorm/add_1AddV2,deep/batch_normalization/batchnorm/mul_1:z:0*deep/batch_normalization/batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2*
(deep/batch_normalization/batchnorm/add_1�
"deep/dense_1/MatMul/ReadVariableOpReadVariableOp+deep_dense_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02$
"deep/dense_1/MatMul/ReadVariableOp�
deep/dense_1/MatMulMatMul,deep/batch_normalization/batchnorm/add_1:z:0*deep/dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
deep/dense_1/MatMul�
#deep/dense_1/BiasAdd/ReadVariableOpReadVariableOp,deep_dense_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02%
#deep/dense_1/BiasAdd/ReadVariableOp�
deep/dense_1/BiasAddBiasAdddeep/dense_1/MatMul:product:0+deep/dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
deep/dense_1/BiasAdd�
deep/dense_1/ReluReludeep/dense_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
deep/dense_1/Relu�
deep/dropout/IdentityIdentitydeep/dense_1/Relu:activations:0*
T0*(
_output_shapes
:����������2
deep/dropout/Identity�
3deep/batch_normalization_1/batchnorm/ReadVariableOpReadVariableOp<deep_batch_normalization_1_batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype025
3deep/batch_normalization_1/batchnorm/ReadVariableOp�
*deep/batch_normalization_1/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2,
*deep/batch_normalization_1/batchnorm/add/y�
(deep/batch_normalization_1/batchnorm/addAddV2;deep/batch_normalization_1/batchnorm/ReadVariableOp:value:03deep/batch_normalization_1/batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2*
(deep/batch_normalization_1/batchnorm/add�
*deep/batch_normalization_1/batchnorm/RsqrtRsqrt,deep/batch_normalization_1/batchnorm/add:z:0*
T0*
_output_shapes	
:�2,
*deep/batch_normalization_1/batchnorm/Rsqrt�
7deep/batch_normalization_1/batchnorm/mul/ReadVariableOpReadVariableOp@deep_batch_normalization_1_batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype029
7deep/batch_normalization_1/batchnorm/mul/ReadVariableOp�
(deep/batch_normalization_1/batchnorm/mulMul.deep/batch_normalization_1/batchnorm/Rsqrt:y:0?deep/batch_normalization_1/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2*
(deep/batch_normalization_1/batchnorm/mul�
*deep/batch_normalization_1/batchnorm/mul_1Muldeep/dropout/Identity:output:0,deep/batch_normalization_1/batchnorm/mul:z:0*
T0*(
_output_shapes
:����������2,
*deep/batch_normalization_1/batchnorm/mul_1�
5deep/batch_normalization_1/batchnorm/ReadVariableOp_1ReadVariableOp>deep_batch_normalization_1_batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype027
5deep/batch_normalization_1/batchnorm/ReadVariableOp_1�
*deep/batch_normalization_1/batchnorm/mul_2Mul=deep/batch_normalization_1/batchnorm/ReadVariableOp_1:value:0,deep/batch_normalization_1/batchnorm/mul:z:0*
T0*
_output_shapes	
:�2,
*deep/batch_normalization_1/batchnorm/mul_2�
5deep/batch_normalization_1/batchnorm/ReadVariableOp_2ReadVariableOp>deep_batch_normalization_1_batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype027
5deep/batch_normalization_1/batchnorm/ReadVariableOp_2�
(deep/batch_normalization_1/batchnorm/subSub=deep/batch_normalization_1/batchnorm/ReadVariableOp_2:value:0.deep/batch_normalization_1/batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2*
(deep/batch_normalization_1/batchnorm/sub�
*deep/batch_normalization_1/batchnorm/add_1AddV2.deep/batch_normalization_1/batchnorm/mul_1:z:0,deep/batch_normalization_1/batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2,
*deep/batch_normalization_1/batchnorm/add_1�
"deep/dense_2/MatMul/ReadVariableOpReadVariableOp+deep_dense_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02$
"deep/dense_2/MatMul/ReadVariableOp�
deep/dense_2/MatMulMatMul.deep/batch_normalization_1/batchnorm/add_1:z:0*deep/dense_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
deep/dense_2/MatMul�
#deep/dense_2/BiasAdd/ReadVariableOpReadVariableOp,deep_dense_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02%
#deep/dense_2/BiasAdd/ReadVariableOp�
deep/dense_2/BiasAddBiasAdddeep/dense_2/MatMul:product:0+deep/dense_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
deep/dense_2/BiasAdd�
deep/dense_2/ReluReludeep/dense_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
deep/dense_2/Relu�
deep/dropout_1/IdentityIdentitydeep/dense_2/Relu:activations:0*
T0*(
_output_shapes
:����������2
deep/dropout_1/Identity�
3deep/batch_normalization_2/batchnorm/ReadVariableOpReadVariableOp<deep_batch_normalization_2_batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype025
3deep/batch_normalization_2/batchnorm/ReadVariableOp�
*deep/batch_normalization_2/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2,
*deep/batch_normalization_2/batchnorm/add/y�
(deep/batch_normalization_2/batchnorm/addAddV2;deep/batch_normalization_2/batchnorm/ReadVariableOp:value:03deep/batch_normalization_2/batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2*
(deep/batch_normalization_2/batchnorm/add�
*deep/batch_normalization_2/batchnorm/RsqrtRsqrt,deep/batch_normalization_2/batchnorm/add:z:0*
T0*
_output_shapes	
:�2,
*deep/batch_normalization_2/batchnorm/Rsqrt�
7deep/batch_normalization_2/batchnorm/mul/ReadVariableOpReadVariableOp@deep_batch_normalization_2_batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype029
7deep/batch_normalization_2/batchnorm/mul/ReadVariableOp�
(deep/batch_normalization_2/batchnorm/mulMul.deep/batch_normalization_2/batchnorm/Rsqrt:y:0?deep/batch_normalization_2/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2*
(deep/batch_normalization_2/batchnorm/mul�
*deep/batch_normalization_2/batchnorm/mul_1Mul deep/dropout_1/Identity:output:0,deep/batch_normalization_2/batchnorm/mul:z:0*
T0*(
_output_shapes
:����������2,
*deep/batch_normalization_2/batchnorm/mul_1�
5deep/batch_normalization_2/batchnorm/ReadVariableOp_1ReadVariableOp>deep_batch_normalization_2_batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype027
5deep/batch_normalization_2/batchnorm/ReadVariableOp_1�
*deep/batch_normalization_2/batchnorm/mul_2Mul=deep/batch_normalization_2/batchnorm/ReadVariableOp_1:value:0,deep/batch_normalization_2/batchnorm/mul:z:0*
T0*
_output_shapes	
:�2,
*deep/batch_normalization_2/batchnorm/mul_2�
5deep/batch_normalization_2/batchnorm/ReadVariableOp_2ReadVariableOp>deep_batch_normalization_2_batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype027
5deep/batch_normalization_2/batchnorm/ReadVariableOp_2�
(deep/batch_normalization_2/batchnorm/subSub=deep/batch_normalization_2/batchnorm/ReadVariableOp_2:value:0.deep/batch_normalization_2/batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2*
(deep/batch_normalization_2/batchnorm/sub�
*deep/batch_normalization_2/batchnorm/add_1AddV2.deep/batch_normalization_2/batchnorm/mul_1:z:0,deep/batch_normalization_2/batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2,
*deep/batch_normalization_2/batchnorm/add_1�
"deep/dense_3/MatMul/ReadVariableOpReadVariableOp+deep_dense_3_matmul_readvariableop_resource*
_output_shapes
:	�@*
dtype02$
"deep/dense_3/MatMul/ReadVariableOp�
deep/dense_3/MatMulMatMul.deep/batch_normalization_2/batchnorm/add_1:z:0*deep/dense_3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
deep/dense_3/MatMul�
#deep/dense_3/BiasAdd/ReadVariableOpReadVariableOp,deep_dense_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02%
#deep/dense_3/BiasAdd/ReadVariableOp�
deep/dense_3/BiasAddBiasAdddeep/dense_3/MatMul:product:0+deep/dense_3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
deep/dense_3/BiasAdd
deep/dense_3/ReluReludeep/dense_3/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
deep/dense_3/Relu�
deep/dropout_2/IdentityIdentitydeep/dense_3/Relu:activations:0*
T0*'
_output_shapes
:���������@2
deep/dropout_2/Identity�
"deep/dense_4/MatMul/ReadVariableOpReadVariableOp+deep_dense_4_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02$
"deep/dense_4/MatMul/ReadVariableOp�
deep/dense_4/MatMulMatMul deep/dropout_2/Identity:output:0*deep/dense_4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
deep/dense_4/MatMul�
#deep/dense_4/BiasAdd/ReadVariableOpReadVariableOp,deep_dense_4_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02%
#deep/dense_4/BiasAdd/ReadVariableOp�
deep/dense_4/BiasAddBiasAdddeep/dense_4/MatMul:product:0+deep/dense_4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
deep/dense_4/BiasAdd�
deep/dense_4/SigmoidSigmoiddeep/dense_4/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
deep/dense_4/Sigmoids
IdentityIdentitydeep/dense_4/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�

NoOpNoOp2^deep/batch_normalization/batchnorm/ReadVariableOp4^deep/batch_normalization/batchnorm/ReadVariableOp_14^deep/batch_normalization/batchnorm/ReadVariableOp_26^deep/batch_normalization/batchnorm/mul/ReadVariableOp4^deep/batch_normalization_1/batchnorm/ReadVariableOp6^deep/batch_normalization_1/batchnorm/ReadVariableOp_16^deep/batch_normalization_1/batchnorm/ReadVariableOp_28^deep/batch_normalization_1/batchnorm/mul/ReadVariableOp4^deep/batch_normalization_2/batchnorm/ReadVariableOp6^deep/batch_normalization_2/batchnorm/ReadVariableOp_16^deep/batch_normalization_2/batchnorm/ReadVariableOp_28^deep/batch_normalization_2/batchnorm/mul/ReadVariableOp!^deep/dense/MatMul/ReadVariableOp$^deep/dense_1/BiasAdd/ReadVariableOp#^deep/dense_1/MatMul/ReadVariableOp$^deep/dense_2/BiasAdd/ReadVariableOp#^deep/dense_2/MatMul/ReadVariableOp$^deep/dense_3/BiasAdd/ReadVariableOp#^deep/dense_3/MatMul/ReadVariableOp$^deep/dense_4/BiasAdd/ReadVariableOp#^deep/dense_4/MatMul/ReadVariableOp ^deep/embedding/embedding_lookup"^deep/embedding_1/embedding_lookup=^deep/integer_lookup/None_lookup_table_find/LookupTableFindV2<^deep/string_lookup/None_lookup_table_find/LookupTableFindV2>^deep/string_lookup_1/None_lookup_table_find/LookupTableFindV2>^deep/string_lookup_2/None_lookup_table_find/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2f
1deep/batch_normalization/batchnorm/ReadVariableOp1deep/batch_normalization/batchnorm/ReadVariableOp2j
3deep/batch_normalization/batchnorm/ReadVariableOp_13deep/batch_normalization/batchnorm/ReadVariableOp_12j
3deep/batch_normalization/batchnorm/ReadVariableOp_23deep/batch_normalization/batchnorm/ReadVariableOp_22n
5deep/batch_normalization/batchnorm/mul/ReadVariableOp5deep/batch_normalization/batchnorm/mul/ReadVariableOp2j
3deep/batch_normalization_1/batchnorm/ReadVariableOp3deep/batch_normalization_1/batchnorm/ReadVariableOp2n
5deep/batch_normalization_1/batchnorm/ReadVariableOp_15deep/batch_normalization_1/batchnorm/ReadVariableOp_12n
5deep/batch_normalization_1/batchnorm/ReadVariableOp_25deep/batch_normalization_1/batchnorm/ReadVariableOp_22r
7deep/batch_normalization_1/batchnorm/mul/ReadVariableOp7deep/batch_normalization_1/batchnorm/mul/ReadVariableOp2j
3deep/batch_normalization_2/batchnorm/ReadVariableOp3deep/batch_normalization_2/batchnorm/ReadVariableOp2n
5deep/batch_normalization_2/batchnorm/ReadVariableOp_15deep/batch_normalization_2/batchnorm/ReadVariableOp_12n
5deep/batch_normalization_2/batchnorm/ReadVariableOp_25deep/batch_normalization_2/batchnorm/ReadVariableOp_22r
7deep/batch_normalization_2/batchnorm/mul/ReadVariableOp7deep/batch_normalization_2/batchnorm/mul/ReadVariableOp2D
 deep/dense/MatMul/ReadVariableOp deep/dense/MatMul/ReadVariableOp2J
#deep/dense_1/BiasAdd/ReadVariableOp#deep/dense_1/BiasAdd/ReadVariableOp2H
"deep/dense_1/MatMul/ReadVariableOp"deep/dense_1/MatMul/ReadVariableOp2J
#deep/dense_2/BiasAdd/ReadVariableOp#deep/dense_2/BiasAdd/ReadVariableOp2H
"deep/dense_2/MatMul/ReadVariableOp"deep/dense_2/MatMul/ReadVariableOp2J
#deep/dense_3/BiasAdd/ReadVariableOp#deep/dense_3/BiasAdd/ReadVariableOp2H
"deep/dense_3/MatMul/ReadVariableOp"deep/dense_3/MatMul/ReadVariableOp2J
#deep/dense_4/BiasAdd/ReadVariableOp#deep/dense_4/BiasAdd/ReadVariableOp2H
"deep/dense_4/MatMul/ReadVariableOp"deep/dense_4/MatMul/ReadVariableOp2B
deep/embedding/embedding_lookupdeep/embedding/embedding_lookup2F
!deep/embedding_1/embedding_lookup!deep/embedding_1/embedding_lookup2|
<deep/integer_lookup/None_lookup_table_find/LookupTableFindV2<deep/integer_lookup/None_lookup_table_find/LookupTableFindV22z
;deep/string_lookup/None_lookup_table_find/LookupTableFindV2;deep/string_lookup/None_lookup_table_find/LookupTableFindV22~
=deep/string_lookup_1/None_lookup_table_find/LookupTableFindV2=deep/string_lookup_1/None_lookup_table_find/LookupTableFindV22~
=deep/string_lookup_2/None_lookup_table_find/LookupTableFindV2=deep/string_lookup_2/None_lookup_table_find/LookupTableFindV2:H D
#
_output_shapes
:���������

_user_specified_nameage:KG
#
_output_shapes
:���������
 
_user_specified_namegender:NJ
'
_output_shapes
:���������

_user_specified_namegenre:QM
(
_output_shapes
:����������
!
_user_specified_name	history:LH
#
_output_shapes
:���������
!
_user_specified_name	item_id:OK
#
_output_shapes
:���������
$
_user_specified_name
occupation:QM
#
_output_shapes
:���������
&
_user_specified_namerelease_date:MI
#
_output_shapes
:���������
"
_user_specified_name
zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_dense_3_layer_call_and_return_conditional_losses_50908

inputs1
matmul_readvariableop_resource:	�@-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������@2
Relum
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������@2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�*
�
N__inference_batch_normalization_layer_call_and_return_conditional_losses_52498

inputs6
'assignmovingavg_readvariableop_resource:	�8
)assignmovingavg_1_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�0
!batchnorm_readvariableop_resource:	�
identity��AssignMovingAvg�AssignMovingAvg/ReadVariableOp�AssignMovingAvg_1� AssignMovingAvg_1/ReadVariableOp�batchnorm/ReadVariableOp�batchnorm/mul/ReadVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/mean}
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes
:	�2
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*(
_output_shapes
:����������2
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze_1s
AssignMovingAvg/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOp'assignmovingavg_readvariableop_resource*
_output_shapes	
:�*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/mul�
AssignMovingAvgAssignSubVariableOp'assignmovingavg_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvgw
AssignMovingAvg_1/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp)assignmovingavg_1_readvariableop_resource*
_output_shapes	
:�*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/mul�
AssignMovingAvg_1AssignSubVariableOp)assignmovingavg_1_readvariableop_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvg_1g
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1|
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^AssignMovingAvg^AssignMovingAvg/ReadVariableOp^AssignMovingAvg_1!^AssignMovingAvg_1/ReadVariableOp^batchnorm/ReadVariableOp^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 2"
AssignMovingAvgAssignMovingAvg2@
AssignMovingAvg/ReadVariableOpAssignMovingAvg/ReadVariableOp2&
AssignMovingAvg_1AssignMovingAvg_12D
 AssignMovingAvg_1/ReadVariableOp AssignMovingAvg_1/ReadVariableOp24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp2<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
'__inference_dense_1_layer_call_fn_52544

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_508422
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
`
B__inference_dropout_layer_call_and_return_conditional_losses_50853

inputs

identity_1[
IdentityIdentityinputs*
T0*(
_output_shapes
:����������2

Identityj

Identity_1IdentityIdentity:output:0*
T0*(
_output_shapes
:����������2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
b
)__inference_dropout_2_layer_call_fn_52825

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_2_layer_call_and_return_conditional_losses_510342
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������@2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������@22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�$
__inference__traced_save_53301
file_prefixU
Qsavev2_string_lookup_2_index_table_lookup_table_export_values_lookuptableexportv2W
Ssavev2_string_lookup_2_index_table_lookup_table_export_values_lookuptableexportv2_1	T
Psavev2_integer_lookup_index_table_lookup_table_export_values_lookuptableexportv2	V
Rsavev2_integer_lookup_index_table_lookup_table_export_values_lookuptableexportv2_1	S
Osavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2U
Qsavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2_1	U
Qsavev2_string_lookup_1_index_table_lookup_table_export_values_lookuptableexportv2W
Ssavev2_string_lookup_1_index_table_lookup_table_export_values_lookuptableexportv2_1	3
/savev2_embedding_embeddings_read_readvariableop+
'savev2_dense_kernel_read_readvariableop5
1savev2_embedding_1_embeddings_read_readvariableop8
4savev2_batch_normalization_gamma_read_readvariableop7
3savev2_batch_normalization_beta_read_readvariableop>
:savev2_batch_normalization_moving_mean_read_readvariableopB
>savev2_batch_normalization_moving_variance_read_readvariableop-
)savev2_dense_1_kernel_read_readvariableop+
'savev2_dense_1_bias_read_readvariableop:
6savev2_batch_normalization_1_gamma_read_readvariableop9
5savev2_batch_normalization_1_beta_read_readvariableop@
<savev2_batch_normalization_1_moving_mean_read_readvariableopD
@savev2_batch_normalization_1_moving_variance_read_readvariableop-
)savev2_dense_2_kernel_read_readvariableop+
'savev2_dense_2_bias_read_readvariableop:
6savev2_batch_normalization_2_gamma_read_readvariableop9
5savev2_batch_normalization_2_beta_read_readvariableop@
<savev2_batch_normalization_2_moving_mean_read_readvariableopD
@savev2_batch_normalization_2_moving_variance_read_readvariableop-
)savev2_dense_3_kernel_read_readvariableop+
'savev2_dense_3_bias_read_readvariableop-
)savev2_dense_4_kernel_read_readvariableop+
'savev2_dense_4_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop-
)savev2_true_positives_read_readvariableop-
)savev2_true_negatives_read_readvariableop.
*savev2_false_positives_read_readvariableop.
*savev2_false_negatives_read_readvariableop:
6savev2_adam_embedding_embeddings_m_read_readvariableop2
.savev2_adam_dense_kernel_m_read_readvariableop<
8savev2_adam_embedding_1_embeddings_m_read_readvariableop?
;savev2_adam_batch_normalization_gamma_m_read_readvariableop>
:savev2_adam_batch_normalization_beta_m_read_readvariableop4
0savev2_adam_dense_1_kernel_m_read_readvariableop2
.savev2_adam_dense_1_bias_m_read_readvariableopA
=savev2_adam_batch_normalization_1_gamma_m_read_readvariableop@
<savev2_adam_batch_normalization_1_beta_m_read_readvariableop4
0savev2_adam_dense_2_kernel_m_read_readvariableop2
.savev2_adam_dense_2_bias_m_read_readvariableopA
=savev2_adam_batch_normalization_2_gamma_m_read_readvariableop@
<savev2_adam_batch_normalization_2_beta_m_read_readvariableop4
0savev2_adam_dense_3_kernel_m_read_readvariableop2
.savev2_adam_dense_3_bias_m_read_readvariableop4
0savev2_adam_dense_4_kernel_m_read_readvariableop2
.savev2_adam_dense_4_bias_m_read_readvariableop:
6savev2_adam_embedding_embeddings_v_read_readvariableop2
.savev2_adam_dense_kernel_v_read_readvariableop<
8savev2_adam_embedding_1_embeddings_v_read_readvariableop?
;savev2_adam_batch_normalization_gamma_v_read_readvariableop>
:savev2_adam_batch_normalization_beta_v_read_readvariableop4
0savev2_adam_dense_1_kernel_v_read_readvariableop2
.savev2_adam_dense_1_bias_v_read_readvariableopA
=savev2_adam_batch_normalization_1_gamma_v_read_readvariableop@
<savev2_adam_batch_normalization_1_beta_v_read_readvariableop4
0savev2_adam_dense_2_kernel_v_read_readvariableop2
.savev2_adam_dense_2_bias_v_read_readvariableopA
=savev2_adam_batch_normalization_2_gamma_v_read_readvariableop@
<savev2_adam_batch_normalization_2_beta_v_read_readvariableop4
0savev2_adam_dense_3_kernel_v_read_readvariableop2
.savev2_adam_dense_3_bias_v_read_readvariableop4
0savev2_adam_dense_4_kernel_v_read_readvariableop2
.savev2_adam_dense_4_bias_v_read_readvariableop
savev2_const_4

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�*
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:O*
dtype0*�)
value�)B�)OB2layer_with_weights-0/_table/.ATTRIBUTES/table-keysB4layer_with_weights-0/_table/.ATTRIBUTES/table-valuesB2layer_with_weights-1/_table/.ATTRIBUTES/table-keysB4layer_with_weights-1/_table/.ATTRIBUTES/table-valuesB2layer_with_weights-2/_table/.ATTRIBUTES/table-keysB4layer_with_weights-2/_table/.ATTRIBUTES/table-valuesB2layer_with_weights-3/_table/.ATTRIBUTES/table-keysB4layer_with_weights-3/_table/.ATTRIBUTES/table-valuesB:layer_with_weights-4/embeddings/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB:layer_with_weights-6/embeddings/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-7/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-7/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-7/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-9/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-9/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-9/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-11/gamma/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/beta/.ATTRIBUTES/VARIABLE_VALUEB<layer_with_weights-11/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-11/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-4/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-6/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-9/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-11/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-13/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-13/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-4/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-6/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-9/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-11/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-13/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-13/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:O*
dtype0*�
value�B�OB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�#
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0Qsavev2_string_lookup_2_index_table_lookup_table_export_values_lookuptableexportv2Ssavev2_string_lookup_2_index_table_lookup_table_export_values_lookuptableexportv2_1Psavev2_integer_lookup_index_table_lookup_table_export_values_lookuptableexportv2Rsavev2_integer_lookup_index_table_lookup_table_export_values_lookuptableexportv2_1Osavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2Qsavev2_string_lookup_index_table_lookup_table_export_values_lookuptableexportv2_1Qsavev2_string_lookup_1_index_table_lookup_table_export_values_lookuptableexportv2Ssavev2_string_lookup_1_index_table_lookup_table_export_values_lookuptableexportv2_1/savev2_embedding_embeddings_read_readvariableop'savev2_dense_kernel_read_readvariableop1savev2_embedding_1_embeddings_read_readvariableop4savev2_batch_normalization_gamma_read_readvariableop3savev2_batch_normalization_beta_read_readvariableop:savev2_batch_normalization_moving_mean_read_readvariableop>savev2_batch_normalization_moving_variance_read_readvariableop)savev2_dense_1_kernel_read_readvariableop'savev2_dense_1_bias_read_readvariableop6savev2_batch_normalization_1_gamma_read_readvariableop5savev2_batch_normalization_1_beta_read_readvariableop<savev2_batch_normalization_1_moving_mean_read_readvariableop@savev2_batch_normalization_1_moving_variance_read_readvariableop)savev2_dense_2_kernel_read_readvariableop'savev2_dense_2_bias_read_readvariableop6savev2_batch_normalization_2_gamma_read_readvariableop5savev2_batch_normalization_2_beta_read_readvariableop<savev2_batch_normalization_2_moving_mean_read_readvariableop@savev2_batch_normalization_2_moving_variance_read_readvariableop)savev2_dense_3_kernel_read_readvariableop'savev2_dense_3_bias_read_readvariableop)savev2_dense_4_kernel_read_readvariableop'savev2_dense_4_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop)savev2_true_positives_read_readvariableop)savev2_true_negatives_read_readvariableop*savev2_false_positives_read_readvariableop*savev2_false_negatives_read_readvariableop6savev2_adam_embedding_embeddings_m_read_readvariableop.savev2_adam_dense_kernel_m_read_readvariableop8savev2_adam_embedding_1_embeddings_m_read_readvariableop;savev2_adam_batch_normalization_gamma_m_read_readvariableop:savev2_adam_batch_normalization_beta_m_read_readvariableop0savev2_adam_dense_1_kernel_m_read_readvariableop.savev2_adam_dense_1_bias_m_read_readvariableop=savev2_adam_batch_normalization_1_gamma_m_read_readvariableop<savev2_adam_batch_normalization_1_beta_m_read_readvariableop0savev2_adam_dense_2_kernel_m_read_readvariableop.savev2_adam_dense_2_bias_m_read_readvariableop=savev2_adam_batch_normalization_2_gamma_m_read_readvariableop<savev2_adam_batch_normalization_2_beta_m_read_readvariableop0savev2_adam_dense_3_kernel_m_read_readvariableop.savev2_adam_dense_3_bias_m_read_readvariableop0savev2_adam_dense_4_kernel_m_read_readvariableop.savev2_adam_dense_4_bias_m_read_readvariableop6savev2_adam_embedding_embeddings_v_read_readvariableop.savev2_adam_dense_kernel_v_read_readvariableop8savev2_adam_embedding_1_embeddings_v_read_readvariableop;savev2_adam_batch_normalization_gamma_v_read_readvariableop:savev2_adam_batch_normalization_beta_v_read_readvariableop0savev2_adam_dense_1_kernel_v_read_readvariableop.savev2_adam_dense_1_bias_v_read_readvariableop=savev2_adam_batch_normalization_1_gamma_v_read_readvariableop<savev2_adam_batch_normalization_1_beta_v_read_readvariableop0savev2_adam_dense_2_kernel_v_read_readvariableop.savev2_adam_dense_2_bias_v_read_readvariableop=savev2_adam_batch_normalization_2_gamma_v_read_readvariableop<savev2_adam_batch_normalization_2_beta_v_read_readvariableop0savev2_adam_dense_3_kernel_v_read_readvariableop.savev2_adam_dense_3_bias_v_read_readvariableop0savev2_adam_dense_4_kernel_v_read_readvariableop.savev2_adam_dense_4_bias_v_read_readvariableopsavev2_const_4"/device:CPU:0*
_output_shapes
 *]
dtypesS
Q2O						2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :::::::::	�:	�):	�):�:�:�:�:
��:�:�:�:�:�:
��:�:�:�:�:�:	�@:@:@:: : : : : : : : : :�:�:�:�:	�:	�):	�):�:�:
��:�:�:�:
��:�:�:�:	�@:@:@::	�:	�):	�):�:�:
��:�:�:�:
��:�:�:�:	�@:@:@:: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:

_output_shapes
::

_output_shapes
::

_output_shapes
::

_output_shapes
::

_output_shapes
::

_output_shapes
::

_output_shapes
::

_output_shapes
::%	!

_output_shapes
:	�:%
!

_output_shapes
:	�):%!

_output_shapes
:	�):!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:&"
 
_output_shapes
:
��:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:!

_output_shapes	
:�:%!

_output_shapes
:	�@: 

_output_shapes
:@:$ 

_output_shapes

:@: 

_output_shapes
:: 

_output_shapes
: :!

_output_shapes
: :"

_output_shapes
: :#

_output_shapes
: :$

_output_shapes
: :%

_output_shapes
: :&

_output_shapes
: :'

_output_shapes
: :(

_output_shapes
: :!)

_output_shapes	
:�:!*

_output_shapes	
:�:!+

_output_shapes	
:�:!,

_output_shapes	
:�:%-!

_output_shapes
:	�:%.!

_output_shapes
:	�):%/!

_output_shapes
:	�):!0

_output_shapes	
:�:!1

_output_shapes	
:�:&2"
 
_output_shapes
:
��:!3

_output_shapes	
:�:!4

_output_shapes	
:�:!5

_output_shapes	
:�:&6"
 
_output_shapes
:
��:!7

_output_shapes	
:�:!8

_output_shapes	
:�:!9

_output_shapes	
:�:%:!

_output_shapes
:	�@: ;

_output_shapes
:@:$< 

_output_shapes

:@: =

_output_shapes
::%>!

_output_shapes
:	�:%?!

_output_shapes
:	�):%@!

_output_shapes
:	�):!A

_output_shapes	
:�:!B

_output_shapes	
:�:&C"
 
_output_shapes
:
��:!D

_output_shapes	
:�:!E

_output_shapes	
:�:!F

_output_shapes	
:�:&G"
 
_output_shapes
:
��:!H

_output_shapes	
:�:!I

_output_shapes	
:�:!J

_output_shapes	
:�:%K!

_output_shapes
:	�@: L

_output_shapes
:@:$M 

_output_shapes

:@: N

_output_shapes
::O

_output_shapes
: 
�
,
__inference__destroyer_52875
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
,
__inference__destroyer_52905
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
�
__inference_save_fn_52951
checkpoint_keyZ
Vinteger_lookup_index_table_lookup_table_export_values_lookuptableexportv2_table_handle
identity

identity_1

identity_2	

identity_3

identity_4

identity_5	��Iinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2�
Iinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2Vinteger_lookup_index_table_lookup_table_export_values_lookuptableexportv2_table_handle",/job:localhost/replica:0/task:0/device:CPU:0*
Tkeys0	*
Tvalues0	*
_output_shapes

::2K
Iinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2T
add/yConst*
_output_shapes
: *
dtype0*
valueB B-keys2
add/yR
addAddcheckpoint_keyadd/y:output:0*
T0*
_output_shapes
: 2
addZ
add_1/yConst*
_output_shapes
: *
dtype0*
valueB B-values2	
add_1/yX
add_1Addcheckpoint_keyadd_1/y:output:0*
T0*
_output_shapes
: 2
add_1Q
IdentityIdentityadd:z:0^NoOp*
T0*
_output_shapes
: 2

IdentityO
ConstConst*
_output_shapes
: *
dtype0*
valueB B 2
Const\

Identity_1IdentityConst:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1�

Identity_2IdentityPinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2:keys:0^NoOp*
T0	*
_output_shapes
:2

Identity_2W

Identity_3Identity	add_1:z:0^NoOp*
T0*
_output_shapes
: 2

Identity_3S
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 2	
Const_1^

Identity_4IdentityConst_1:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_4�

Identity_5IdentityRinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2:values:0^NoOp*
T0	*
_output_shapes
:2

Identity_5�
NoOpNoOpJ^integer_lookup_index_table_lookup_table_export_values/LookupTableExportV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : 2�
Iinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2Iinteger_lookup_index_table_lookup_table_export_values/LookupTableExportV2:F B

_output_shapes
: 
(
_user_specified_namecheckpoint_key
�

�
__inference_restore_fn_52932
restored_tensors_0
restored_tensors_1	N
Jstring_lookup_2_index_table_table_restore_lookuptableimportv2_table_handle
identity��=string_lookup_2_index_table_table_restore/LookupTableImportV2�
=string_lookup_2_index_table_table_restore/LookupTableImportV2LookupTableImportV2Jstring_lookup_2_index_table_table_restore_lookuptableimportv2_table_handlerestored_tensors_0restored_tensors_1",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
 2?
=string_lookup_2_index_table_table_restore/LookupTableImportV2P
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstX
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: 2

Identity�
NoOpNoOp>^string_lookup_2_index_table_table_restore/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes

::: 2~
=string_lookup_2_index_table_table_restore/LookupTableImportV2=string_lookup_2_index_table_table_restore/LookupTableImportV2:L H

_output_shapes
:
,
_user_specified_namerestored_tensors_0:LH

_output_shapes
:
,
_user_specified_namerestored_tensors_1
�
c
D__inference_dropout_1_layer_call_and_return_conditional_losses_52688

inputs
identity�c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *���?2
dropout/Constt
dropout/MulMulinputsdropout/Const:output:0*
T0*(
_output_shapes
:����������2
dropout/MulT
dropout/ShapeShapeinputs*
T0*
_output_shapes
:2
dropout/Shape�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L=2
dropout/GreaterEqual/y�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������2
dropout/GreaterEqual�
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������2
dropout/Cast{
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*(
_output_shapes
:����������2
dropout/Mul_1f
IdentityIdentitydropout/Mul_1:z:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
*
__inference_<lambda>_53033
identityS
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
c
D__inference_dropout_2_layer_call_and_return_conditional_losses_51034

inputs
identity�c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *���?2
dropout/Consts
dropout/MulMulinputsdropout/Const:output:0*
T0*'
_output_shapes
:���������@2
dropout/MulT
dropout/ShapeShapeinputs*
T0*
_output_shapes
:2
dropout/Shape�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*'
_output_shapes
:���������@*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L=2
dropout/GreaterEqual/y�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:���������@2
dropout/GreaterEqual
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:���������@2
dropout/Castz
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*'
_output_shapes
:���������@2
dropout/Mul_1e
IdentityIdentitydropout/Mul_1:z:0*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������@:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
F__inference_concatenate_layer_call_and_return_conditional_losses_52432
inputs_0
inputs_1
inputs_2
inputs_3
inputs_4
inputs_5
inputs_6
inputs_7
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputs_0inputs_1inputs_2inputs_3inputs_4inputs_5inputs_6inputs_7concat/axis:output:0*
N*
T0*(
_output_shapes
:����������2
concatd
IdentityIdentityconcat:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:���������:���������):���������:���������):���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3:QM
'
_output_shapes
:���������)
"
_user_specified_name
inputs/4:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/5:QM
'
_output_shapes
:���������)
"
_user_specified_name
inputs/6:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/7
�
�
'__inference_dense_2_layer_call_fn_52671

inputs
unknown:
��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_2_layer_call_and_return_conditional_losses_508752
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
c
D__inference_dropout_2_layer_call_and_return_conditional_losses_52815

inputs
identity�c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *���?2
dropout/Consts
dropout/MulMulinputsdropout/Const:output:0*
T0*'
_output_shapes
:���������@2
dropout/MulT
dropout/ShapeShapeinputs*
T0*
_output_shapes
:2
dropout/Shape�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*'
_output_shapes
:���������@*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L=2
dropout/GreaterEqual/y�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:���������@2
dropout/GreaterEqual
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:���������@2
dropout/Castz
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*'
_output_shapes
:���������@2
dropout/Mul_1e
IdentityIdentitydropout/Mul_1:z:0*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������@:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
`
B__inference_dropout_layer_call_and_return_conditional_losses_52549

inputs

identity_1[
IdentityIdentityinputs*
T0*(
_output_shapes
:����������2

Identityj

Identity_1IdentityIdentity:output:0*
T0*(
_output_shapes
:����������2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
#__inference_signature_wrapper_51838
age

gender	
genre
history
item_id

occupation
release_date
zip_code
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3
	unknown_4	
	unknown_5
	unknown_6	
	unknown_7:	�
	unknown_8:	�)
	unknown_9:	�)

unknown_10:	�

unknown_11:	�

unknown_12:	�

unknown_13:	�

unknown_14:
��

unknown_15:	�

unknown_16:	�

unknown_17:	�

unknown_18:	�

unknown_19:	�

unknown_20:
��

unknown_21:	�

unknown_22:	�

unknown_23:	�

unknown_24:	�

unknown_25:	�

unknown_26:	�@

unknown_27:@

unknown_28:@

unknown_29:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallagegendergenrehistoryitem_id
occupationrelease_datezip_codeunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29*2
Tin+
)2'				*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*9
_read_only_resource_inputs
 !"#$%&*0
config_proto 

CPU

GPU2*0J 8� *)
f$R"
 __inference__wrapped_model_502062
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:H D
#
_output_shapes
:���������

_user_specified_nameage:KG
#
_output_shapes
:���������
 
_user_specified_namegender:NJ
'
_output_shapes
:���������

_user_specified_namegenre:QM
(
_output_shapes
:����������
!
_user_specified_name	history:LH
#
_output_shapes
:���������
!
_user_specified_name	item_id:OK
#
_output_shapes
:���������
$
_user_specified_name
occupation:QM
#
_output_shapes
:���������
&
_user_specified_namerelease_date:MI
#
_output_shapes
:���������
"
_user_specified_name
zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
+__inference_embedding_1_layer_call_fn_52419

inputs	
unknown:	�)
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_embedding_1_layer_call_and_return_conditional_losses_508012
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������)2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*$
_input_shapes
:���������: 22
StatefulPartitionedCallStatefulPartitionedCall:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
B__inference_dense_1_layer_call_and_return_conditional_losses_50842

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
@__inference_dense_layer_call_and_return_conditional_losses_50786

inputs1
matmul_readvariableop_resource:	�)
identity��MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�)*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������)2
MatMulk
IdentityIdentityMatMul:product:0^NoOp*
T0*'
_output_shapes
:���������)2

Identityf
NoOpNoOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:����������: 2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_50392

inputs0
!batchnorm_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�2
#batchnorm_readvariableop_1_resource:	�2
#batchnorm_readvariableop_2_resource:	�
identity��batchnorm/ReadVariableOp�batchnorm/ReadVariableOp_1�batchnorm/ReadVariableOp_2�batchnorm/mul/ReadVariableOp�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^batchnorm/ReadVariableOp^batchnorm/ReadVariableOp_1^batchnorm/ReadVariableOp_2^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp28
batchnorm/ReadVariableOp_1batchnorm/ReadVariableOp_128
batchnorm/ReadVariableOp_2batchnorm/ReadVariableOp_22<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�	
�
D__inference_embedding_layer_call_and_return_conditional_losses_52382

inputs	)
embedding_lookup_52376:	�
identity��embedding_lookup�
embedding_lookupResourceGatherembedding_lookup_52376inputs",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*)
_class
loc:@embedding_lookup/52376*'
_output_shapes
:���������*
dtype02
embedding_lookup�
embedding_lookup/IdentityIdentityembedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*)
_class
loc:@embedding_lookup/52376*'
_output_shapes
:���������2
embedding_lookup/Identity�
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������2
embedding_lookup/Identity_1
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identitya
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*$
_input_shapes
:���������: 2$
embedding_lookupembedding_lookup:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs
�*
�
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_50614

inputs6
'assignmovingavg_readvariableop_resource:	�8
)assignmovingavg_1_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�0
!batchnorm_readvariableop_resource:	�
identity��AssignMovingAvg�AssignMovingAvg/ReadVariableOp�AssignMovingAvg_1� AssignMovingAvg_1/ReadVariableOp�batchnorm/ReadVariableOp�batchnorm/mul/ReadVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/mean}
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes
:	�2
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*(
_output_shapes
:����������2
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze_1s
AssignMovingAvg/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOp'assignmovingavg_readvariableop_resource*
_output_shapes	
:�*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/mul�
AssignMovingAvgAssignSubVariableOp'assignmovingavg_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvgw
AssignMovingAvg_1/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp)assignmovingavg_1_readvariableop_resource*
_output_shapes	
:�*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/mul�
AssignMovingAvg_1AssignSubVariableOp)assignmovingavg_1_readvariableop_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvg_1g
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1|
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^AssignMovingAvg^AssignMovingAvg/ReadVariableOp^AssignMovingAvg_1!^AssignMovingAvg_1/ReadVariableOp^batchnorm/ReadVariableOp^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 2"
AssignMovingAvgAssignMovingAvg2@
AssignMovingAvg/ReadVariableOpAssignMovingAvg/ReadVariableOp2&
AssignMovingAvg_1AssignMovingAvg_12D
 AssignMovingAvg_1/ReadVariableOp AssignMovingAvg_1/ReadVariableOp24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp2<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
B__inference_dense_2_layer_call_and_return_conditional_losses_52662

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
@__inference_dense_layer_call_and_return_conditional_losses_52396

inputs1
matmul_readvariableop_resource:	�)
identity��MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�)*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������)2
MatMulk
IdentityIdentityMatMul:product:0^NoOp*
T0*'
_output_shapes
:���������)2

Identityf
NoOpNoOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:����������: 2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_52591

inputs0
!batchnorm_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�2
#batchnorm_readvariableop_1_resource:	�2
#batchnorm_readvariableop_2_resource:	�
identity��batchnorm/ReadVariableOp�batchnorm/ReadVariableOp_1�batchnorm/ReadVariableOp_2�batchnorm/mul/ReadVariableOp�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^batchnorm/ReadVariableOp^batchnorm/ReadVariableOp_1^batchnorm/ReadVariableOp_2^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp28
batchnorm/ReadVariableOp_1batchnorm/ReadVariableOp_128
batchnorm/ReadVariableOp_2batchnorm/ReadVariableOp_22<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
N__inference_batch_normalization_layer_call_and_return_conditional_losses_50230

inputs0
!batchnorm_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�2
#batchnorm_readvariableop_1_resource:	�2
#batchnorm_readvariableop_2_resource:	�
identity��batchnorm/ReadVariableOp�batchnorm/ReadVariableOp_1�batchnorm/ReadVariableOp_2�batchnorm/mul/ReadVariableOp�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^batchnorm/ReadVariableOp^batchnorm/ReadVariableOp_1^batchnorm/ReadVariableOp_2^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp28
batchnorm/ReadVariableOp_1batchnorm/ReadVariableOp_128
batchnorm/ReadVariableOp_2batchnorm/ReadVariableOp_22<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
a
B__inference_dropout_layer_call_and_return_conditional_losses_52561

inputs
identity�c
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *���?2
dropout/Constt
dropout/MulMulinputsdropout/Const:output:0*
T0*(
_output_shapes
:����������2
dropout/MulT
dropout/ShapeShapeinputs*
T0*
_output_shapes
:2
dropout/Shape�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype02&
$dropout/random_uniform/RandomUniformu
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L=2
dropout/GreaterEqual/y�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������2
dropout/GreaterEqual�
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������2
dropout/Cast{
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*(
_output_shapes
:����������2
dropout/Mul_1f
IdentityIdentitydropout/Mul_1:z:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�1
!__inference__traced_restore_53533
file_prefixc
Ystring_lookup_2_index_table_table_restore_lookuptableimportv2_string_lookup_2_index_table: a
Winteger_lookup_index_table_table_restore_lookuptableimportv2_integer_lookup_index_table:	 _
Ustring_lookup_index_table_table_restore_lookuptableimportv2_string_lookup_index_table: c
Ystring_lookup_1_index_table_table_restore_lookuptableimportv2_string_lookup_1_index_table: 8
%assignvariableop_embedding_embeddings:	�2
assignvariableop_1_dense_kernel:	�)<
)assignvariableop_2_embedding_1_embeddings:	�);
,assignvariableop_3_batch_normalization_gamma:	�:
+assignvariableop_4_batch_normalization_beta:	�A
2assignvariableop_5_batch_normalization_moving_mean:	�E
6assignvariableop_6_batch_normalization_moving_variance:	�5
!assignvariableop_7_dense_1_kernel:
��.
assignvariableop_8_dense_1_bias:	�=
.assignvariableop_9_batch_normalization_1_gamma:	�=
.assignvariableop_10_batch_normalization_1_beta:	�D
5assignvariableop_11_batch_normalization_1_moving_mean:	�H
9assignvariableop_12_batch_normalization_1_moving_variance:	�6
"assignvariableop_13_dense_2_kernel:
��/
 assignvariableop_14_dense_2_bias:	�>
/assignvariableop_15_batch_normalization_2_gamma:	�=
.assignvariableop_16_batch_normalization_2_beta:	�D
5assignvariableop_17_batch_normalization_2_moving_mean:	�H
9assignvariableop_18_batch_normalization_2_moving_variance:	�5
"assignvariableop_19_dense_3_kernel:	�@.
 assignvariableop_20_dense_3_bias:@4
"assignvariableop_21_dense_4_kernel:@.
 assignvariableop_22_dense_4_bias:'
assignvariableop_23_adam_iter:	 )
assignvariableop_24_adam_beta_1: )
assignvariableop_25_adam_beta_2: (
assignvariableop_26_adam_decay: 0
&assignvariableop_27_adam_learning_rate: #
assignvariableop_28_total: #
assignvariableop_29_count: %
assignvariableop_30_total_1: %
assignvariableop_31_count_1: 1
"assignvariableop_32_true_positives:	�1
"assignvariableop_33_true_negatives:	�2
#assignvariableop_34_false_positives:	�2
#assignvariableop_35_false_negatives:	�B
/assignvariableop_36_adam_embedding_embeddings_m:	�:
'assignvariableop_37_adam_dense_kernel_m:	�)D
1assignvariableop_38_adam_embedding_1_embeddings_m:	�)C
4assignvariableop_39_adam_batch_normalization_gamma_m:	�B
3assignvariableop_40_adam_batch_normalization_beta_m:	�=
)assignvariableop_41_adam_dense_1_kernel_m:
��6
'assignvariableop_42_adam_dense_1_bias_m:	�E
6assignvariableop_43_adam_batch_normalization_1_gamma_m:	�D
5assignvariableop_44_adam_batch_normalization_1_beta_m:	�=
)assignvariableop_45_adam_dense_2_kernel_m:
��6
'assignvariableop_46_adam_dense_2_bias_m:	�E
6assignvariableop_47_adam_batch_normalization_2_gamma_m:	�D
5assignvariableop_48_adam_batch_normalization_2_beta_m:	�<
)assignvariableop_49_adam_dense_3_kernel_m:	�@5
'assignvariableop_50_adam_dense_3_bias_m:@;
)assignvariableop_51_adam_dense_4_kernel_m:@5
'assignvariableop_52_adam_dense_4_bias_m:B
/assignvariableop_53_adam_embedding_embeddings_v:	�:
'assignvariableop_54_adam_dense_kernel_v:	�)D
1assignvariableop_55_adam_embedding_1_embeddings_v:	�)C
4assignvariableop_56_adam_batch_normalization_gamma_v:	�B
3assignvariableop_57_adam_batch_normalization_beta_v:	�=
)assignvariableop_58_adam_dense_1_kernel_v:
��6
'assignvariableop_59_adam_dense_1_bias_v:	�E
6assignvariableop_60_adam_batch_normalization_1_gamma_v:	�D
5assignvariableop_61_adam_batch_normalization_1_beta_v:	�=
)assignvariableop_62_adam_dense_2_kernel_v:
��6
'assignvariableop_63_adam_dense_2_bias_v:	�E
6assignvariableop_64_adam_batch_normalization_2_gamma_v:	�D
5assignvariableop_65_adam_batch_normalization_2_beta_v:	�<
)assignvariableop_66_adam_dense_3_kernel_v:	�@5
'assignvariableop_67_adam_dense_3_bias_v:@;
)assignvariableop_68_adam_dense_4_kernel_v:@5
'assignvariableop_69_adam_dense_4_bias_v:
identity_71��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_41�AssignVariableOp_42�AssignVariableOp_43�AssignVariableOp_44�AssignVariableOp_45�AssignVariableOp_46�AssignVariableOp_47�AssignVariableOp_48�AssignVariableOp_49�AssignVariableOp_5�AssignVariableOp_50�AssignVariableOp_51�AssignVariableOp_52�AssignVariableOp_53�AssignVariableOp_54�AssignVariableOp_55�AssignVariableOp_56�AssignVariableOp_57�AssignVariableOp_58�AssignVariableOp_59�AssignVariableOp_6�AssignVariableOp_60�AssignVariableOp_61�AssignVariableOp_62�AssignVariableOp_63�AssignVariableOp_64�AssignVariableOp_65�AssignVariableOp_66�AssignVariableOp_67�AssignVariableOp_68�AssignVariableOp_69�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�<integer_lookup_index_table_table_restore/LookupTableImportV2�=string_lookup_1_index_table_table_restore/LookupTableImportV2�=string_lookup_2_index_table_table_restore/LookupTableImportV2�;string_lookup_index_table_table_restore/LookupTableImportV2�*
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:O*
dtype0*�)
value�)B�)OB2layer_with_weights-0/_table/.ATTRIBUTES/table-keysB4layer_with_weights-0/_table/.ATTRIBUTES/table-valuesB2layer_with_weights-1/_table/.ATTRIBUTES/table-keysB4layer_with_weights-1/_table/.ATTRIBUTES/table-valuesB2layer_with_weights-2/_table/.ATTRIBUTES/table-keysB4layer_with_weights-2/_table/.ATTRIBUTES/table-valuesB2layer_with_weights-3/_table/.ATTRIBUTES/table-keysB4layer_with_weights-3/_table/.ATTRIBUTES/table-valuesB:layer_with_weights-4/embeddings/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB:layer_with_weights-6/embeddings/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-7/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-7/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-7/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-8/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-8/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-9/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-9/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-9/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-9/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-10/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-10/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-11/gamma/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-11/beta/.ATTRIBUTES/VARIABLE_VALUEB<layer_with_weights-11/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-11/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-12/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-12/bias/.ATTRIBUTES/VARIABLE_VALUEB7layer_with_weights-13/kernel/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-13/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-4/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-6/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-9/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-11/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-13/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-13/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-4/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-6/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-7/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-8/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-8/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-9/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-9/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-10/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-10/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-11/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-11/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-12/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-12/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBSlayer_with_weights-13/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-13/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:O*
dtype0*�
value�B�OB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*]
dtypesS
Q2O						2
	RestoreV2�
=string_lookup_2_index_table_table_restore/LookupTableImportV2LookupTableImportV2Ystring_lookup_2_index_table_table_restore_lookuptableimportv2_string_lookup_2_index_tableRestoreV2:tensors:0RestoreV2:tensors:1*	
Tin0*

Tout0	*.
_class$
" loc:@string_lookup_2_index_table*
_output_shapes
 2?
=string_lookup_2_index_table_table_restore/LookupTableImportV2�
<integer_lookup_index_table_table_restore/LookupTableImportV2LookupTableImportV2Winteger_lookup_index_table_table_restore_lookuptableimportv2_integer_lookup_index_tableRestoreV2:tensors:2RestoreV2:tensors:3*	
Tin0	*

Tout0	*-
_class#
!loc:@integer_lookup_index_table*
_output_shapes
 2>
<integer_lookup_index_table_table_restore/LookupTableImportV2�
;string_lookup_index_table_table_restore/LookupTableImportV2LookupTableImportV2Ustring_lookup_index_table_table_restore_lookuptableimportv2_string_lookup_index_tableRestoreV2:tensors:4RestoreV2:tensors:5*	
Tin0*

Tout0	*,
_class"
 loc:@string_lookup_index_table*
_output_shapes
 2=
;string_lookup_index_table_table_restore/LookupTableImportV2�
=string_lookup_1_index_table_table_restore/LookupTableImportV2LookupTableImportV2Ystring_lookup_1_index_table_table_restore_lookuptableimportv2_string_lookup_1_index_tableRestoreV2:tensors:6RestoreV2:tensors:7*	
Tin0*

Tout0	*.
_class$
" loc:@string_lookup_1_index_table*
_output_shapes
 2?
=string_lookup_1_index_table_table_restore/LookupTableImportV2g
IdentityIdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp%assignvariableop_embedding_embeddingsIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOpassignvariableop_1_dense_kernelIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1l

Identity_2IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp)assignvariableop_2_embedding_1_embeddingsIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2l

Identity_3IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp,assignvariableop_3_batch_normalization_gammaIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3l

Identity_4IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp+assignvariableop_4_batch_normalization_betaIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4l

Identity_5IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp2assignvariableop_5_batch_normalization_moving_meanIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5l

Identity_6IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp6assignvariableop_6_batch_normalization_moving_varianceIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6l

Identity_7IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp!assignvariableop_7_dense_1_kernelIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7l

Identity_8IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOpassignvariableop_8_dense_1_biasIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8l

Identity_9IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp.assignvariableop_9_batch_normalization_1_gammaIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp.assignvariableop_10_batch_normalization_1_betaIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOp5assignvariableop_11_batch_normalization_1_moving_meanIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp9assignvariableop_12_batch_normalization_1_moving_varianceIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp"assignvariableop_13_dense_2_kernelIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp assignvariableop_14_dense_2_biasIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp/assignvariableop_15_batch_normalization_2_gammaIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp.assignvariableop_16_batch_normalization_2_betaIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp5assignvariableop_17_batch_normalization_2_moving_meanIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp9assignvariableop_18_batch_normalization_2_moving_varianceIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp"assignvariableop_19_dense_3_kernelIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp assignvariableop_20_dense_3_biasIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp"assignvariableop_21_dense_4_kernelIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp assignvariableop_22_dense_4_biasIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:31"/device:CPU:0*
T0	*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOpassignvariableop_23_adam_iterIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOpassignvariableop_24_adam_beta_1Identity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOpassignvariableop_25_adam_beta_2Identity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOpassignvariableop_26_adam_decayIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp&assignvariableop_27_adam_learning_rateIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOpassignvariableop_28_totalIdentity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOpassignvariableop_29_countIdentity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOpassignvariableop_30_total_1Identity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOpassignvariableop_31_count_1Identity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp"assignvariableop_32_true_positivesIdentity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOp"assignvariableop_33_true_negativesIdentity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOp#assignvariableop_34_false_positivesIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOp#assignvariableop_35_false_negativesIdentity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOp/assignvariableop_36_adam_embedding_embeddings_mIdentity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37�
AssignVariableOp_37AssignVariableOp'assignvariableop_37_adam_dense_kernel_mIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38�
AssignVariableOp_38AssignVariableOp1assignvariableop_38_adam_embedding_1_embeddings_mIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:47"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39�
AssignVariableOp_39AssignVariableOp4assignvariableop_39_adam_batch_normalization_gamma_mIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:48"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40�
AssignVariableOp_40AssignVariableOp3assignvariableop_40_adam_batch_normalization_beta_mIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_40n
Identity_41IdentityRestoreV2:tensors:49"/device:CPU:0*
T0*
_output_shapes
:2
Identity_41�
AssignVariableOp_41AssignVariableOp)assignvariableop_41_adam_dense_1_kernel_mIdentity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_41n
Identity_42IdentityRestoreV2:tensors:50"/device:CPU:0*
T0*
_output_shapes
:2
Identity_42�
AssignVariableOp_42AssignVariableOp'assignvariableop_42_adam_dense_1_bias_mIdentity_42:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_42n
Identity_43IdentityRestoreV2:tensors:51"/device:CPU:0*
T0*
_output_shapes
:2
Identity_43�
AssignVariableOp_43AssignVariableOp6assignvariableop_43_adam_batch_normalization_1_gamma_mIdentity_43:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_43n
Identity_44IdentityRestoreV2:tensors:52"/device:CPU:0*
T0*
_output_shapes
:2
Identity_44�
AssignVariableOp_44AssignVariableOp5assignvariableop_44_adam_batch_normalization_1_beta_mIdentity_44:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_44n
Identity_45IdentityRestoreV2:tensors:53"/device:CPU:0*
T0*
_output_shapes
:2
Identity_45�
AssignVariableOp_45AssignVariableOp)assignvariableop_45_adam_dense_2_kernel_mIdentity_45:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_45n
Identity_46IdentityRestoreV2:tensors:54"/device:CPU:0*
T0*
_output_shapes
:2
Identity_46�
AssignVariableOp_46AssignVariableOp'assignvariableop_46_adam_dense_2_bias_mIdentity_46:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_46n
Identity_47IdentityRestoreV2:tensors:55"/device:CPU:0*
T0*
_output_shapes
:2
Identity_47�
AssignVariableOp_47AssignVariableOp6assignvariableop_47_adam_batch_normalization_2_gamma_mIdentity_47:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_47n
Identity_48IdentityRestoreV2:tensors:56"/device:CPU:0*
T0*
_output_shapes
:2
Identity_48�
AssignVariableOp_48AssignVariableOp5assignvariableop_48_adam_batch_normalization_2_beta_mIdentity_48:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_48n
Identity_49IdentityRestoreV2:tensors:57"/device:CPU:0*
T0*
_output_shapes
:2
Identity_49�
AssignVariableOp_49AssignVariableOp)assignvariableop_49_adam_dense_3_kernel_mIdentity_49:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_49n
Identity_50IdentityRestoreV2:tensors:58"/device:CPU:0*
T0*
_output_shapes
:2
Identity_50�
AssignVariableOp_50AssignVariableOp'assignvariableop_50_adam_dense_3_bias_mIdentity_50:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_50n
Identity_51IdentityRestoreV2:tensors:59"/device:CPU:0*
T0*
_output_shapes
:2
Identity_51�
AssignVariableOp_51AssignVariableOp)assignvariableop_51_adam_dense_4_kernel_mIdentity_51:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_51n
Identity_52IdentityRestoreV2:tensors:60"/device:CPU:0*
T0*
_output_shapes
:2
Identity_52�
AssignVariableOp_52AssignVariableOp'assignvariableop_52_adam_dense_4_bias_mIdentity_52:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_52n
Identity_53IdentityRestoreV2:tensors:61"/device:CPU:0*
T0*
_output_shapes
:2
Identity_53�
AssignVariableOp_53AssignVariableOp/assignvariableop_53_adam_embedding_embeddings_vIdentity_53:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_53n
Identity_54IdentityRestoreV2:tensors:62"/device:CPU:0*
T0*
_output_shapes
:2
Identity_54�
AssignVariableOp_54AssignVariableOp'assignvariableop_54_adam_dense_kernel_vIdentity_54:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_54n
Identity_55IdentityRestoreV2:tensors:63"/device:CPU:0*
T0*
_output_shapes
:2
Identity_55�
AssignVariableOp_55AssignVariableOp1assignvariableop_55_adam_embedding_1_embeddings_vIdentity_55:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_55n
Identity_56IdentityRestoreV2:tensors:64"/device:CPU:0*
T0*
_output_shapes
:2
Identity_56�
AssignVariableOp_56AssignVariableOp4assignvariableop_56_adam_batch_normalization_gamma_vIdentity_56:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_56n
Identity_57IdentityRestoreV2:tensors:65"/device:CPU:0*
T0*
_output_shapes
:2
Identity_57�
AssignVariableOp_57AssignVariableOp3assignvariableop_57_adam_batch_normalization_beta_vIdentity_57:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_57n
Identity_58IdentityRestoreV2:tensors:66"/device:CPU:0*
T0*
_output_shapes
:2
Identity_58�
AssignVariableOp_58AssignVariableOp)assignvariableop_58_adam_dense_1_kernel_vIdentity_58:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_58n
Identity_59IdentityRestoreV2:tensors:67"/device:CPU:0*
T0*
_output_shapes
:2
Identity_59�
AssignVariableOp_59AssignVariableOp'assignvariableop_59_adam_dense_1_bias_vIdentity_59:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_59n
Identity_60IdentityRestoreV2:tensors:68"/device:CPU:0*
T0*
_output_shapes
:2
Identity_60�
AssignVariableOp_60AssignVariableOp6assignvariableop_60_adam_batch_normalization_1_gamma_vIdentity_60:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_60n
Identity_61IdentityRestoreV2:tensors:69"/device:CPU:0*
T0*
_output_shapes
:2
Identity_61�
AssignVariableOp_61AssignVariableOp5assignvariableop_61_adam_batch_normalization_1_beta_vIdentity_61:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_61n
Identity_62IdentityRestoreV2:tensors:70"/device:CPU:0*
T0*
_output_shapes
:2
Identity_62�
AssignVariableOp_62AssignVariableOp)assignvariableop_62_adam_dense_2_kernel_vIdentity_62:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_62n
Identity_63IdentityRestoreV2:tensors:71"/device:CPU:0*
T0*
_output_shapes
:2
Identity_63�
AssignVariableOp_63AssignVariableOp'assignvariableop_63_adam_dense_2_bias_vIdentity_63:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_63n
Identity_64IdentityRestoreV2:tensors:72"/device:CPU:0*
T0*
_output_shapes
:2
Identity_64�
AssignVariableOp_64AssignVariableOp6assignvariableop_64_adam_batch_normalization_2_gamma_vIdentity_64:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_64n
Identity_65IdentityRestoreV2:tensors:73"/device:CPU:0*
T0*
_output_shapes
:2
Identity_65�
AssignVariableOp_65AssignVariableOp5assignvariableop_65_adam_batch_normalization_2_beta_vIdentity_65:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_65n
Identity_66IdentityRestoreV2:tensors:74"/device:CPU:0*
T0*
_output_shapes
:2
Identity_66�
AssignVariableOp_66AssignVariableOp)assignvariableop_66_adam_dense_3_kernel_vIdentity_66:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_66n
Identity_67IdentityRestoreV2:tensors:75"/device:CPU:0*
T0*
_output_shapes
:2
Identity_67�
AssignVariableOp_67AssignVariableOp'assignvariableop_67_adam_dense_3_bias_vIdentity_67:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_67n
Identity_68IdentityRestoreV2:tensors:76"/device:CPU:0*
T0*
_output_shapes
:2
Identity_68�
AssignVariableOp_68AssignVariableOp)assignvariableop_68_adam_dense_4_kernel_vIdentity_68:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_68n
Identity_69IdentityRestoreV2:tensors:77"/device:CPU:0*
T0*
_output_shapes
:2
Identity_69�
AssignVariableOp_69AssignVariableOp'assignvariableop_69_adam_dense_4_bias_vIdentity_69:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_699
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_70Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp=^integer_lookup_index_table_table_restore/LookupTableImportV2>^string_lookup_1_index_table_table_restore/LookupTableImportV2>^string_lookup_2_index_table_table_restore/LookupTableImportV2<^string_lookup_index_table_table_restore/LookupTableImportV2"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_70f
Identity_71IdentityIdentity_70:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_71�
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9=^integer_lookup_index_table_table_restore/LookupTableImportV2>^string_lookup_1_index_table_table_restore/LookupTableImportV2>^string_lookup_2_index_table_table_restore/LookupTableImportV2<^string_lookup_index_table_table_restore/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_71Identity_71:output:0*�
_input_shapes�
�: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462*
AssignVariableOp_47AssignVariableOp_472*
AssignVariableOp_48AssignVariableOp_482*
AssignVariableOp_49AssignVariableOp_492(
AssignVariableOp_5AssignVariableOp_52*
AssignVariableOp_50AssignVariableOp_502*
AssignVariableOp_51AssignVariableOp_512*
AssignVariableOp_52AssignVariableOp_522*
AssignVariableOp_53AssignVariableOp_532*
AssignVariableOp_54AssignVariableOp_542*
AssignVariableOp_55AssignVariableOp_552*
AssignVariableOp_56AssignVariableOp_562*
AssignVariableOp_57AssignVariableOp_572*
AssignVariableOp_58AssignVariableOp_582*
AssignVariableOp_59AssignVariableOp_592(
AssignVariableOp_6AssignVariableOp_62*
AssignVariableOp_60AssignVariableOp_602*
AssignVariableOp_61AssignVariableOp_612*
AssignVariableOp_62AssignVariableOp_622*
AssignVariableOp_63AssignVariableOp_632*
AssignVariableOp_64AssignVariableOp_642*
AssignVariableOp_65AssignVariableOp_652*
AssignVariableOp_66AssignVariableOp_662*
AssignVariableOp_67AssignVariableOp_672*
AssignVariableOp_68AssignVariableOp_682*
AssignVariableOp_69AssignVariableOp_692(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92|
<integer_lookup_index_table_table_restore/LookupTableImportV2<integer_lookup_index_table_table_restore/LookupTableImportV22~
=string_lookup_1_index_table_table_restore/LookupTableImportV2=string_lookup_1_index_table_table_restore/LookupTableImportV22~
=string_lookup_2_index_table_table_restore/LookupTableImportV2=string_lookup_2_index_table_table_restore/LookupTableImportV22z
;string_lookup_index_table_table_restore/LookupTableImportV2;string_lookup_index_table_table_restore/LookupTableImportV2:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:40
.
_class$
" loc:@string_lookup_2_index_table:3/
-
_class#
!loc:@integer_lookup_index_table:2.
,
_class"
 loc:@string_lookup_index_table:40
.
_class$
" loc:@string_lookup_1_index_table
�
�
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_50554

inputs0
!batchnorm_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�2
#batchnorm_readvariableop_1_resource:	�2
#batchnorm_readvariableop_2_resource:	�
identity��batchnorm/ReadVariableOp�batchnorm/ReadVariableOp_1�batchnorm/ReadVariableOp_2�batchnorm/mul/ReadVariableOp�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^batchnorm/ReadVariableOp^batchnorm/ReadVariableOp_1^batchnorm/ReadVariableOp_2^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp28
batchnorm/ReadVariableOp_1batchnorm/ReadVariableOp_128
batchnorm/ReadVariableOp_2batchnorm/ReadVariableOp_22<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
��
�
?__inference_deep_layer_call_and_return_conditional_losses_52225

inputs_age
inputs_gender
inputs_genre
inputs_history
inputs_item_id
inputs_occupation
inputs_release_date
inputs_zip_codeH
Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleI
Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value	G
Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handleH
Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value	3
 embedding_embedding_lookup_52063:	�7
$dense_matmul_readvariableop_resource:	�)5
"embedding_1_embedding_lookup_52073:	�)J
;batch_normalization_assignmovingavg_readvariableop_resource:	�L
=batch_normalization_assignmovingavg_1_readvariableop_resource:	�H
9batch_normalization_batchnorm_mul_readvariableop_resource:	�D
5batch_normalization_batchnorm_readvariableop_resource:	�:
&dense_1_matmul_readvariableop_resource:
��6
'dense_1_biasadd_readvariableop_resource:	�L
=batch_normalization_1_assignmovingavg_readvariableop_resource:	�N
?batch_normalization_1_assignmovingavg_1_readvariableop_resource:	�J
;batch_normalization_1_batchnorm_mul_readvariableop_resource:	�F
7batch_normalization_1_batchnorm_readvariableop_resource:	�:
&dense_2_matmul_readvariableop_resource:
��6
'dense_2_biasadd_readvariableop_resource:	�L
=batch_normalization_2_assignmovingavg_readvariableop_resource:	�N
?batch_normalization_2_assignmovingavg_1_readvariableop_resource:	�J
;batch_normalization_2_batchnorm_mul_readvariableop_resource:	�F
7batch_normalization_2_batchnorm_readvariableop_resource:	�9
&dense_3_matmul_readvariableop_resource:	�@5
'dense_3_biasadd_readvariableop_resource:@8
&dense_4_matmul_readvariableop_resource:@5
'dense_4_biasadd_readvariableop_resource:
identity��#batch_normalization/AssignMovingAvg�2batch_normalization/AssignMovingAvg/ReadVariableOp�%batch_normalization/AssignMovingAvg_1�4batch_normalization/AssignMovingAvg_1/ReadVariableOp�,batch_normalization/batchnorm/ReadVariableOp�0batch_normalization/batchnorm/mul/ReadVariableOp�%batch_normalization_1/AssignMovingAvg�4batch_normalization_1/AssignMovingAvg/ReadVariableOp�'batch_normalization_1/AssignMovingAvg_1�6batch_normalization_1/AssignMovingAvg_1/ReadVariableOp�.batch_normalization_1/batchnorm/ReadVariableOp�2batch_normalization_1/batchnorm/mul/ReadVariableOp�%batch_normalization_2/AssignMovingAvg�4batch_normalization_2/AssignMovingAvg/ReadVariableOp�'batch_normalization_2/AssignMovingAvg_1�6batch_normalization_2/AssignMovingAvg_1/ReadVariableOp�.batch_normalization_2/batchnorm/ReadVariableOp�2batch_normalization_2/batchnorm/mul/ReadVariableOp�dense/MatMul/ReadVariableOp�dense_1/BiasAdd/ReadVariableOp�dense_1/MatMul/ReadVariableOp�dense_2/BiasAdd/ReadVariableOp�dense_2/MatMul/ReadVariableOp�dense_3/BiasAdd/ReadVariableOp�dense_3/MatMul/ReadVariableOp�dense_4/BiasAdd/ReadVariableOp�dense_4/MatMul/ReadVariableOp�embedding/embedding_lookup�embedding_1/embedding_lookup�7integer_lookup/None_lookup_table_find/LookupTableFindV2�6string_lookup/None_lookup_table_find/LookupTableFindV2�8string_lookup_1/None_lookup_table_find/LookupTableFindV2�8string_lookup_2/None_lookup_table_find/LookupTableFindV2
integer_lookup/CastCastinputs_item_id*

DstT0	*

SrcT0*#
_output_shapes
:���������2
integer_lookup/Cast�
7integer_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleinteger_lookup/Cast:y:0Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0	*

Tout0	*#
_output_shapes
:���������29
7integer_lookup/None_lookup_table_find/LookupTableFindV2�
integer_lookup/IdentityIdentity@integer_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
integer_lookup/Identity�
8string_lookup_2/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleinputs_zip_codeFstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:���������2:
8string_lookup_2/None_lookup_table_find/LookupTableFindV2�
string_lookup_2/IdentityIdentityAstring_lookup_2/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
string_lookup_2/Identity�
tf.expand_dims_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_1/ExpandDims/dim�
tf.expand_dims_1/ExpandDims
ExpandDimsinputs_gender(tf.expand_dims_1/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_1/ExpandDims�
tf.expand_dims/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2
tf.expand_dims/ExpandDims/dim�
tf.expand_dims/ExpandDims
ExpandDimsinputs_occupation&tf.expand_dims/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims/ExpandDims�
6string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handle"tf.expand_dims/ExpandDims:output:0Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������28
6string_lookup/None_lookup_table_find/LookupTableFindV2�
string_lookup/bincount/ShapeShape?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2
string_lookup/bincount/Shape�
string_lookup/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
string_lookup/bincount/Const�
string_lookup/bincount/ProdProd%string_lookup/bincount/Shape:output:0%string_lookup/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup/bincount/Prod�
 string_lookup/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2"
 string_lookup/bincount/Greater/y�
string_lookup/bincount/GreaterGreater$string_lookup/bincount/Prod:output:0)string_lookup/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2 
string_lookup/bincount/Greater�
string_lookup/bincount/CastCast"string_lookup/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup/bincount/Cast�
string_lookup/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2 
string_lookup/bincount/Const_1�
string_lookup/bincount/MaxMax?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0'string_lookup/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/Max~
string_lookup/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2
string_lookup/bincount/add/y�
string_lookup/bincount/addAddV2#string_lookup/bincount/Max:output:0%string_lookup/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/add�
string_lookup/bincount/mulMulstring_lookup/bincount/Cast:y:0string_lookup/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/mul�
 string_lookup/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/minlength�
string_lookup/bincount/MaximumMaximum)string_lookup/bincount/minlength:output:0string_lookup/bincount/mul:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Maximum�
 string_lookup/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/maxlength�
string_lookup/bincount/MinimumMinimum)string_lookup/bincount/maxlength:output:0"string_lookup/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Minimum�
string_lookup/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2 
string_lookup/bincount/Const_2�
$string_lookup/bincount/DenseBincountDenseBincount?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0"string_lookup/bincount/Minimum:z:0'string_lookup/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2&
$string_lookup/bincount/DenseBincount�
8string_lookup_1/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handle$tf.expand_dims_1/ExpandDims:output:0Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������2:
8string_lookup_1/None_lookup_table_find/LookupTableFindV2�
string_lookup_1/bincount/ShapeShapeAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2 
string_lookup_1/bincount/Shape�
string_lookup_1/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2 
string_lookup_1/bincount/Const�
string_lookup_1/bincount/ProdProd'string_lookup_1/bincount/Shape:output:0'string_lookup_1/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup_1/bincount/Prod�
"string_lookup_1/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2$
"string_lookup_1/bincount/Greater/y�
 string_lookup_1/bincount/GreaterGreater&string_lookup_1/bincount/Prod:output:0+string_lookup_1/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2"
 string_lookup_1/bincount/Greater�
string_lookup_1/bincount/CastCast$string_lookup_1/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup_1/bincount/Cast�
 string_lookup_1/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2"
 string_lookup_1/bincount/Const_1�
string_lookup_1/bincount/MaxMaxAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0)string_lookup_1/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/Max�
string_lookup_1/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2 
string_lookup_1/bincount/add/y�
string_lookup_1/bincount/addAddV2%string_lookup_1/bincount/Max:output:0'string_lookup_1/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/add�
string_lookup_1/bincount/mulMul!string_lookup_1/bincount/Cast:y:0 string_lookup_1/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/mul�
"string_lookup_1/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/minlength�
 string_lookup_1/bincount/MaximumMaximum+string_lookup_1/bincount/minlength:output:0 string_lookup_1/bincount/mul:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Maximum�
"string_lookup_1/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/maxlength�
 string_lookup_1/bincount/MinimumMinimum+string_lookup_1/bincount/maxlength:output:0$string_lookup_1/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Minimum�
 string_lookup_1/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2"
 string_lookup_1/bincount/Const_2�
&string_lookup_1/bincount/DenseBincountDenseBincountAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0$string_lookup_1/bincount/Minimum:z:0)string_lookup_1/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2(
&string_lookup_1/bincount/DenseBincount�
embedding/embedding_lookupResourceGather embedding_embedding_lookup_52063!string_lookup_2/Identity:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*3
_class)
'%loc:@embedding/embedding_lookup/52063*'
_output_shapes
:���������*
dtype02
embedding/embedding_lookup�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*3
_class)
'%loc:@embedding/embedding_lookup/52063*'
_output_shapes
:���������2%
#embedding/embedding_lookup/Identity�
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������2'
%embedding/embedding_lookup/Identity_1�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes
:	�)*
dtype02
dense/MatMul/ReadVariableOp�
dense/MatMulMatMulinputs_history#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������)2
dense/MatMul�
tf.expand_dims_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_2/ExpandDims/dim�
tf.expand_dims_2/ExpandDims
ExpandDims
inputs_age(tf.expand_dims_2/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_2/ExpandDims�
embedding_1/embedding_lookupResourceGather"embedding_1_embedding_lookup_52073 integer_lookup/Identity:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*5
_class+
)'loc:@embedding_1/embedding_lookup/52073*'
_output_shapes
:���������)*
dtype02
embedding_1/embedding_lookup�
%embedding_1/embedding_lookup/IdentityIdentity%embedding_1/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*5
_class+
)'loc:@embedding_1/embedding_lookup/52073*'
_output_shapes
:���������)2'
%embedding_1/embedding_lookup/Identity�
'embedding_1/embedding_lookup/Identity_1Identity.embedding_1/embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������)2)
'embedding_1/embedding_lookup/Identity_1�
tf.expand_dims_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_3/ExpandDims/dim�
tf.expand_dims_3/ExpandDims
ExpandDimsinputs_release_date(tf.expand_dims_3/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_3/ExpandDimst
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis�
concatenate/concatConcatV2-string_lookup/bincount/DenseBincount:output:0inputs_genre/string_lookup_1/bincount/DenseBincount:output:0.embedding/embedding_lookup/Identity_1:output:0dense/MatMul:product:0$tf.expand_dims_2/ExpandDims:output:00embedding_1/embedding_lookup/Identity_1:output:0$tf.expand_dims_3/ExpandDims:output:0 concatenate/concat/axis:output:0*
N*
T0*(
_output_shapes
:����������2
concatenate/concat�
2batch_normalization/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 24
2batch_normalization/moments/mean/reduction_indices�
 batch_normalization/moments/meanMeanconcatenate/concat:output:0;batch_normalization/moments/mean/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2"
 batch_normalization/moments/mean�
(batch_normalization/moments/StopGradientStopGradient)batch_normalization/moments/mean:output:0*
T0*
_output_shapes
:	�2*
(batch_normalization/moments/StopGradient�
-batch_normalization/moments/SquaredDifferenceSquaredDifferenceconcatenate/concat:output:01batch_normalization/moments/StopGradient:output:0*
T0*(
_output_shapes
:����������2/
-batch_normalization/moments/SquaredDifference�
6batch_normalization/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 28
6batch_normalization/moments/variance/reduction_indices�
$batch_normalization/moments/varianceMean1batch_normalization/moments/SquaredDifference:z:0?batch_normalization/moments/variance/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2&
$batch_normalization/moments/variance�
#batch_normalization/moments/SqueezeSqueeze)batch_normalization/moments/mean:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2%
#batch_normalization/moments/Squeeze�
%batch_normalization/moments/Squeeze_1Squeeze-batch_normalization/moments/variance:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2'
%batch_normalization/moments/Squeeze_1�
)batch_normalization/AssignMovingAvg/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2+
)batch_normalization/AssignMovingAvg/decay�
2batch_normalization/AssignMovingAvg/ReadVariableOpReadVariableOp;batch_normalization_assignmovingavg_readvariableop_resource*
_output_shapes	
:�*
dtype024
2batch_normalization/AssignMovingAvg/ReadVariableOp�
'batch_normalization/AssignMovingAvg/subSub:batch_normalization/AssignMovingAvg/ReadVariableOp:value:0,batch_normalization/moments/Squeeze:output:0*
T0*
_output_shapes	
:�2)
'batch_normalization/AssignMovingAvg/sub�
'batch_normalization/AssignMovingAvg/mulMul+batch_normalization/AssignMovingAvg/sub:z:02batch_normalization/AssignMovingAvg/decay:output:0*
T0*
_output_shapes	
:�2)
'batch_normalization/AssignMovingAvg/mul�
#batch_normalization/AssignMovingAvgAssignSubVariableOp;batch_normalization_assignmovingavg_readvariableop_resource+batch_normalization/AssignMovingAvg/mul:z:03^batch_normalization/AssignMovingAvg/ReadVariableOp*
_output_shapes
 *
dtype02%
#batch_normalization/AssignMovingAvg�
+batch_normalization/AssignMovingAvg_1/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2-
+batch_normalization/AssignMovingAvg_1/decay�
4batch_normalization/AssignMovingAvg_1/ReadVariableOpReadVariableOp=batch_normalization_assignmovingavg_1_readvariableop_resource*
_output_shapes	
:�*
dtype026
4batch_normalization/AssignMovingAvg_1/ReadVariableOp�
)batch_normalization/AssignMovingAvg_1/subSub<batch_normalization/AssignMovingAvg_1/ReadVariableOp:value:0.batch_normalization/moments/Squeeze_1:output:0*
T0*
_output_shapes	
:�2+
)batch_normalization/AssignMovingAvg_1/sub�
)batch_normalization/AssignMovingAvg_1/mulMul-batch_normalization/AssignMovingAvg_1/sub:z:04batch_normalization/AssignMovingAvg_1/decay:output:0*
T0*
_output_shapes	
:�2+
)batch_normalization/AssignMovingAvg_1/mul�
%batch_normalization/AssignMovingAvg_1AssignSubVariableOp=batch_normalization_assignmovingavg_1_readvariableop_resource-batch_normalization/AssignMovingAvg_1/mul:z:05^batch_normalization/AssignMovingAvg_1/ReadVariableOp*
_output_shapes
 *
dtype02'
%batch_normalization/AssignMovingAvg_1�
#batch_normalization/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2%
#batch_normalization/batchnorm/add/y�
!batch_normalization/batchnorm/addAddV2.batch_normalization/moments/Squeeze_1:output:0,batch_normalization/batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2#
!batch_normalization/batchnorm/add�
#batch_normalization/batchnorm/RsqrtRsqrt%batch_normalization/batchnorm/add:z:0*
T0*
_output_shapes	
:�2%
#batch_normalization/batchnorm/Rsqrt�
0batch_normalization/batchnorm/mul/ReadVariableOpReadVariableOp9batch_normalization_batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype022
0batch_normalization/batchnorm/mul/ReadVariableOp�
!batch_normalization/batchnorm/mulMul'batch_normalization/batchnorm/Rsqrt:y:08batch_normalization/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2#
!batch_normalization/batchnorm/mul�
#batch_normalization/batchnorm/mul_1Mulconcatenate/concat:output:0%batch_normalization/batchnorm/mul:z:0*
T0*(
_output_shapes
:����������2%
#batch_normalization/batchnorm/mul_1�
#batch_normalization/batchnorm/mul_2Mul,batch_normalization/moments/Squeeze:output:0%batch_normalization/batchnorm/mul:z:0*
T0*
_output_shapes	
:�2%
#batch_normalization/batchnorm/mul_2�
,batch_normalization/batchnorm/ReadVariableOpReadVariableOp5batch_normalization_batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,batch_normalization/batchnorm/ReadVariableOp�
!batch_normalization/batchnorm/subSub4batch_normalization/batchnorm/ReadVariableOp:value:0'batch_normalization/batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2#
!batch_normalization/batchnorm/sub�
#batch_normalization/batchnorm/add_1AddV2'batch_normalization/batchnorm/mul_1:z:0%batch_normalization/batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2%
#batch_normalization/batchnorm/add_1�
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
dense_1/MatMul/ReadVariableOp�
dense_1/MatMulMatMul'batch_normalization/batchnorm/add_1:z:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_1/MatMul�
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02 
dense_1/BiasAdd/ReadVariableOp�
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_1/BiasAddq
dense_1/ReluReludense_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
dense_1/Relus
dropout/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *���?2
dropout/dropout/Const�
dropout/dropout/MulMuldense_1/Relu:activations:0dropout/dropout/Const:output:0*
T0*(
_output_shapes
:����������2
dropout/dropout/Mulx
dropout/dropout/ShapeShapedense_1/Relu:activations:0*
T0*
_output_shapes
:2
dropout/dropout/Shape�
,dropout/dropout/random_uniform/RandomUniformRandomUniformdropout/dropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype02.
,dropout/dropout/random_uniform/RandomUniform�
dropout/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L=2 
dropout/dropout/GreaterEqual/y�
dropout/dropout/GreaterEqualGreaterEqual5dropout/dropout/random_uniform/RandomUniform:output:0'dropout/dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������2
dropout/dropout/GreaterEqual�
dropout/dropout/CastCast dropout/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������2
dropout/dropout/Cast�
dropout/dropout/Mul_1Muldropout/dropout/Mul:z:0dropout/dropout/Cast:y:0*
T0*(
_output_shapes
:����������2
dropout/dropout/Mul_1�
4batch_normalization_1/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 26
4batch_normalization_1/moments/mean/reduction_indices�
"batch_normalization_1/moments/meanMeandropout/dropout/Mul_1:z:0=batch_normalization_1/moments/mean/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2$
"batch_normalization_1/moments/mean�
*batch_normalization_1/moments/StopGradientStopGradient+batch_normalization_1/moments/mean:output:0*
T0*
_output_shapes
:	�2,
*batch_normalization_1/moments/StopGradient�
/batch_normalization_1/moments/SquaredDifferenceSquaredDifferencedropout/dropout/Mul_1:z:03batch_normalization_1/moments/StopGradient:output:0*
T0*(
_output_shapes
:����������21
/batch_normalization_1/moments/SquaredDifference�
8batch_normalization_1/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2:
8batch_normalization_1/moments/variance/reduction_indices�
&batch_normalization_1/moments/varianceMean3batch_normalization_1/moments/SquaredDifference:z:0Abatch_normalization_1/moments/variance/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2(
&batch_normalization_1/moments/variance�
%batch_normalization_1/moments/SqueezeSqueeze+batch_normalization_1/moments/mean:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2'
%batch_normalization_1/moments/Squeeze�
'batch_normalization_1/moments/Squeeze_1Squeeze/batch_normalization_1/moments/variance:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2)
'batch_normalization_1/moments/Squeeze_1�
+batch_normalization_1/AssignMovingAvg/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2-
+batch_normalization_1/AssignMovingAvg/decay�
4batch_normalization_1/AssignMovingAvg/ReadVariableOpReadVariableOp=batch_normalization_1_assignmovingavg_readvariableop_resource*
_output_shapes	
:�*
dtype026
4batch_normalization_1/AssignMovingAvg/ReadVariableOp�
)batch_normalization_1/AssignMovingAvg/subSub<batch_normalization_1/AssignMovingAvg/ReadVariableOp:value:0.batch_normalization_1/moments/Squeeze:output:0*
T0*
_output_shapes	
:�2+
)batch_normalization_1/AssignMovingAvg/sub�
)batch_normalization_1/AssignMovingAvg/mulMul-batch_normalization_1/AssignMovingAvg/sub:z:04batch_normalization_1/AssignMovingAvg/decay:output:0*
T0*
_output_shapes	
:�2+
)batch_normalization_1/AssignMovingAvg/mul�
%batch_normalization_1/AssignMovingAvgAssignSubVariableOp=batch_normalization_1_assignmovingavg_readvariableop_resource-batch_normalization_1/AssignMovingAvg/mul:z:05^batch_normalization_1/AssignMovingAvg/ReadVariableOp*
_output_shapes
 *
dtype02'
%batch_normalization_1/AssignMovingAvg�
-batch_normalization_1/AssignMovingAvg_1/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2/
-batch_normalization_1/AssignMovingAvg_1/decay�
6batch_normalization_1/AssignMovingAvg_1/ReadVariableOpReadVariableOp?batch_normalization_1_assignmovingavg_1_readvariableop_resource*
_output_shapes	
:�*
dtype028
6batch_normalization_1/AssignMovingAvg_1/ReadVariableOp�
+batch_normalization_1/AssignMovingAvg_1/subSub>batch_normalization_1/AssignMovingAvg_1/ReadVariableOp:value:00batch_normalization_1/moments/Squeeze_1:output:0*
T0*
_output_shapes	
:�2-
+batch_normalization_1/AssignMovingAvg_1/sub�
+batch_normalization_1/AssignMovingAvg_1/mulMul/batch_normalization_1/AssignMovingAvg_1/sub:z:06batch_normalization_1/AssignMovingAvg_1/decay:output:0*
T0*
_output_shapes	
:�2-
+batch_normalization_1/AssignMovingAvg_1/mul�
'batch_normalization_1/AssignMovingAvg_1AssignSubVariableOp?batch_normalization_1_assignmovingavg_1_readvariableop_resource/batch_normalization_1/AssignMovingAvg_1/mul:z:07^batch_normalization_1/AssignMovingAvg_1/ReadVariableOp*
_output_shapes
 *
dtype02)
'batch_normalization_1/AssignMovingAvg_1�
%batch_normalization_1/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2'
%batch_normalization_1/batchnorm/add/y�
#batch_normalization_1/batchnorm/addAddV20batch_normalization_1/moments/Squeeze_1:output:0.batch_normalization_1/batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2%
#batch_normalization_1/batchnorm/add�
%batch_normalization_1/batchnorm/RsqrtRsqrt'batch_normalization_1/batchnorm/add:z:0*
T0*
_output_shapes	
:�2'
%batch_normalization_1/batchnorm/Rsqrt�
2batch_normalization_1/batchnorm/mul/ReadVariableOpReadVariableOp;batch_normalization_1_batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype024
2batch_normalization_1/batchnorm/mul/ReadVariableOp�
#batch_normalization_1/batchnorm/mulMul)batch_normalization_1/batchnorm/Rsqrt:y:0:batch_normalization_1/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2%
#batch_normalization_1/batchnorm/mul�
%batch_normalization_1/batchnorm/mul_1Muldropout/dropout/Mul_1:z:0'batch_normalization_1/batchnorm/mul:z:0*
T0*(
_output_shapes
:����������2'
%batch_normalization_1/batchnorm/mul_1�
%batch_normalization_1/batchnorm/mul_2Mul.batch_normalization_1/moments/Squeeze:output:0'batch_normalization_1/batchnorm/mul:z:0*
T0*
_output_shapes	
:�2'
%batch_normalization_1/batchnorm/mul_2�
.batch_normalization_1/batchnorm/ReadVariableOpReadVariableOp7batch_normalization_1_batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype020
.batch_normalization_1/batchnorm/ReadVariableOp�
#batch_normalization_1/batchnorm/subSub6batch_normalization_1/batchnorm/ReadVariableOp:value:0)batch_normalization_1/batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2%
#batch_normalization_1/batchnorm/sub�
%batch_normalization_1/batchnorm/add_1AddV2)batch_normalization_1/batchnorm/mul_1:z:0'batch_normalization_1/batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2'
%batch_normalization_1/batchnorm/add_1�
dense_2/MatMul/ReadVariableOpReadVariableOp&dense_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
dense_2/MatMul/ReadVariableOp�
dense_2/MatMulMatMul)batch_normalization_1/batchnorm/add_1:z:0%dense_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_2/MatMul�
dense_2/BiasAdd/ReadVariableOpReadVariableOp'dense_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02 
dense_2/BiasAdd/ReadVariableOp�
dense_2/BiasAddBiasAdddense_2/MatMul:product:0&dense_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_2/BiasAddq
dense_2/ReluReludense_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
dense_2/Reluw
dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *���?2
dropout_1/dropout/Const�
dropout_1/dropout/MulMuldense_2/Relu:activations:0 dropout_1/dropout/Const:output:0*
T0*(
_output_shapes
:����������2
dropout_1/dropout/Mul|
dropout_1/dropout/ShapeShapedense_2/Relu:activations:0*
T0*
_output_shapes
:2
dropout_1/dropout/Shape�
.dropout_1/dropout/random_uniform/RandomUniformRandomUniform dropout_1/dropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype020
.dropout_1/dropout/random_uniform/RandomUniform�
 dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L=2"
 dropout_1/dropout/GreaterEqual/y�
dropout_1/dropout/GreaterEqualGreaterEqual7dropout_1/dropout/random_uniform/RandomUniform:output:0)dropout_1/dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������2 
dropout_1/dropout/GreaterEqual�
dropout_1/dropout/CastCast"dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������2
dropout_1/dropout/Cast�
dropout_1/dropout/Mul_1Muldropout_1/dropout/Mul:z:0dropout_1/dropout/Cast:y:0*
T0*(
_output_shapes
:����������2
dropout_1/dropout/Mul_1�
4batch_normalization_2/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 26
4batch_normalization_2/moments/mean/reduction_indices�
"batch_normalization_2/moments/meanMeandropout_1/dropout/Mul_1:z:0=batch_normalization_2/moments/mean/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2$
"batch_normalization_2/moments/mean�
*batch_normalization_2/moments/StopGradientStopGradient+batch_normalization_2/moments/mean:output:0*
T0*
_output_shapes
:	�2,
*batch_normalization_2/moments/StopGradient�
/batch_normalization_2/moments/SquaredDifferenceSquaredDifferencedropout_1/dropout/Mul_1:z:03batch_normalization_2/moments/StopGradient:output:0*
T0*(
_output_shapes
:����������21
/batch_normalization_2/moments/SquaredDifference�
8batch_normalization_2/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2:
8batch_normalization_2/moments/variance/reduction_indices�
&batch_normalization_2/moments/varianceMean3batch_normalization_2/moments/SquaredDifference:z:0Abatch_normalization_2/moments/variance/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2(
&batch_normalization_2/moments/variance�
%batch_normalization_2/moments/SqueezeSqueeze+batch_normalization_2/moments/mean:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2'
%batch_normalization_2/moments/Squeeze�
'batch_normalization_2/moments/Squeeze_1Squeeze/batch_normalization_2/moments/variance:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2)
'batch_normalization_2/moments/Squeeze_1�
+batch_normalization_2/AssignMovingAvg/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2-
+batch_normalization_2/AssignMovingAvg/decay�
4batch_normalization_2/AssignMovingAvg/ReadVariableOpReadVariableOp=batch_normalization_2_assignmovingavg_readvariableop_resource*
_output_shapes	
:�*
dtype026
4batch_normalization_2/AssignMovingAvg/ReadVariableOp�
)batch_normalization_2/AssignMovingAvg/subSub<batch_normalization_2/AssignMovingAvg/ReadVariableOp:value:0.batch_normalization_2/moments/Squeeze:output:0*
T0*
_output_shapes	
:�2+
)batch_normalization_2/AssignMovingAvg/sub�
)batch_normalization_2/AssignMovingAvg/mulMul-batch_normalization_2/AssignMovingAvg/sub:z:04batch_normalization_2/AssignMovingAvg/decay:output:0*
T0*
_output_shapes	
:�2+
)batch_normalization_2/AssignMovingAvg/mul�
%batch_normalization_2/AssignMovingAvgAssignSubVariableOp=batch_normalization_2_assignmovingavg_readvariableop_resource-batch_normalization_2/AssignMovingAvg/mul:z:05^batch_normalization_2/AssignMovingAvg/ReadVariableOp*
_output_shapes
 *
dtype02'
%batch_normalization_2/AssignMovingAvg�
-batch_normalization_2/AssignMovingAvg_1/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2/
-batch_normalization_2/AssignMovingAvg_1/decay�
6batch_normalization_2/AssignMovingAvg_1/ReadVariableOpReadVariableOp?batch_normalization_2_assignmovingavg_1_readvariableop_resource*
_output_shapes	
:�*
dtype028
6batch_normalization_2/AssignMovingAvg_1/ReadVariableOp�
+batch_normalization_2/AssignMovingAvg_1/subSub>batch_normalization_2/AssignMovingAvg_1/ReadVariableOp:value:00batch_normalization_2/moments/Squeeze_1:output:0*
T0*
_output_shapes	
:�2-
+batch_normalization_2/AssignMovingAvg_1/sub�
+batch_normalization_2/AssignMovingAvg_1/mulMul/batch_normalization_2/AssignMovingAvg_1/sub:z:06batch_normalization_2/AssignMovingAvg_1/decay:output:0*
T0*
_output_shapes	
:�2-
+batch_normalization_2/AssignMovingAvg_1/mul�
'batch_normalization_2/AssignMovingAvg_1AssignSubVariableOp?batch_normalization_2_assignmovingavg_1_readvariableop_resource/batch_normalization_2/AssignMovingAvg_1/mul:z:07^batch_normalization_2/AssignMovingAvg_1/ReadVariableOp*
_output_shapes
 *
dtype02)
'batch_normalization_2/AssignMovingAvg_1�
%batch_normalization_2/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2'
%batch_normalization_2/batchnorm/add/y�
#batch_normalization_2/batchnorm/addAddV20batch_normalization_2/moments/Squeeze_1:output:0.batch_normalization_2/batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2%
#batch_normalization_2/batchnorm/add�
%batch_normalization_2/batchnorm/RsqrtRsqrt'batch_normalization_2/batchnorm/add:z:0*
T0*
_output_shapes	
:�2'
%batch_normalization_2/batchnorm/Rsqrt�
2batch_normalization_2/batchnorm/mul/ReadVariableOpReadVariableOp;batch_normalization_2_batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype024
2batch_normalization_2/batchnorm/mul/ReadVariableOp�
#batch_normalization_2/batchnorm/mulMul)batch_normalization_2/batchnorm/Rsqrt:y:0:batch_normalization_2/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2%
#batch_normalization_2/batchnorm/mul�
%batch_normalization_2/batchnorm/mul_1Muldropout_1/dropout/Mul_1:z:0'batch_normalization_2/batchnorm/mul:z:0*
T0*(
_output_shapes
:����������2'
%batch_normalization_2/batchnorm/mul_1�
%batch_normalization_2/batchnorm/mul_2Mul.batch_normalization_2/moments/Squeeze:output:0'batch_normalization_2/batchnorm/mul:z:0*
T0*
_output_shapes	
:�2'
%batch_normalization_2/batchnorm/mul_2�
.batch_normalization_2/batchnorm/ReadVariableOpReadVariableOp7batch_normalization_2_batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype020
.batch_normalization_2/batchnorm/ReadVariableOp�
#batch_normalization_2/batchnorm/subSub6batch_normalization_2/batchnorm/ReadVariableOp:value:0)batch_normalization_2/batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2%
#batch_normalization_2/batchnorm/sub�
%batch_normalization_2/batchnorm/add_1AddV2)batch_normalization_2/batchnorm/mul_1:z:0'batch_normalization_2/batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2'
%batch_normalization_2/batchnorm/add_1�
dense_3/MatMul/ReadVariableOpReadVariableOp&dense_3_matmul_readvariableop_resource*
_output_shapes
:	�@*
dtype02
dense_3/MatMul/ReadVariableOp�
dense_3/MatMulMatMul)batch_normalization_2/batchnorm/add_1:z:0%dense_3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_3/MatMul�
dense_3/BiasAdd/ReadVariableOpReadVariableOp'dense_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_3/BiasAdd/ReadVariableOp�
dense_3/BiasAddBiasAdddense_3/MatMul:product:0&dense_3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_3/BiasAddp
dense_3/ReluReludense_3/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
dense_3/Reluw
dropout_2/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *���?2
dropout_2/dropout/Const�
dropout_2/dropout/MulMuldense_3/Relu:activations:0 dropout_2/dropout/Const:output:0*
T0*'
_output_shapes
:���������@2
dropout_2/dropout/Mul|
dropout_2/dropout/ShapeShapedense_3/Relu:activations:0*
T0*
_output_shapes
:2
dropout_2/dropout/Shape�
.dropout_2/dropout/random_uniform/RandomUniformRandomUniform dropout_2/dropout/Shape:output:0*
T0*'
_output_shapes
:���������@*
dtype020
.dropout_2/dropout/random_uniform/RandomUniform�
 dropout_2/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L=2"
 dropout_2/dropout/GreaterEqual/y�
dropout_2/dropout/GreaterEqualGreaterEqual7dropout_2/dropout/random_uniform/RandomUniform:output:0)dropout_2/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:���������@2 
dropout_2/dropout/GreaterEqual�
dropout_2/dropout/CastCast"dropout_2/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:���������@2
dropout_2/dropout/Cast�
dropout_2/dropout/Mul_1Muldropout_2/dropout/Mul:z:0dropout_2/dropout/Cast:y:0*
T0*'
_output_shapes
:���������@2
dropout_2/dropout/Mul_1�
dense_4/MatMul/ReadVariableOpReadVariableOp&dense_4_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02
dense_4/MatMul/ReadVariableOp�
dense_4/MatMulMatMuldropout_2/dropout/Mul_1:z:0%dense_4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dense_4/MatMul�
dense_4/BiasAdd/ReadVariableOpReadVariableOp'dense_4_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02 
dense_4/BiasAdd/ReadVariableOp�
dense_4/BiasAddBiasAdddense_4/MatMul:product:0&dense_4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dense_4/BiasAddy
dense_4/SigmoidSigmoiddense_4/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
dense_4/Sigmoidn
IdentityIdentitydense_4/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp$^batch_normalization/AssignMovingAvg3^batch_normalization/AssignMovingAvg/ReadVariableOp&^batch_normalization/AssignMovingAvg_15^batch_normalization/AssignMovingAvg_1/ReadVariableOp-^batch_normalization/batchnorm/ReadVariableOp1^batch_normalization/batchnorm/mul/ReadVariableOp&^batch_normalization_1/AssignMovingAvg5^batch_normalization_1/AssignMovingAvg/ReadVariableOp(^batch_normalization_1/AssignMovingAvg_17^batch_normalization_1/AssignMovingAvg_1/ReadVariableOp/^batch_normalization_1/batchnorm/ReadVariableOp3^batch_normalization_1/batchnorm/mul/ReadVariableOp&^batch_normalization_2/AssignMovingAvg5^batch_normalization_2/AssignMovingAvg/ReadVariableOp(^batch_normalization_2/AssignMovingAvg_17^batch_normalization_2/AssignMovingAvg_1/ReadVariableOp/^batch_normalization_2/batchnorm/ReadVariableOp3^batch_normalization_2/batchnorm/mul/ReadVariableOp^dense/MatMul/ReadVariableOp^dense_1/BiasAdd/ReadVariableOp^dense_1/MatMul/ReadVariableOp^dense_2/BiasAdd/ReadVariableOp^dense_2/MatMul/ReadVariableOp^dense_3/BiasAdd/ReadVariableOp^dense_3/MatMul/ReadVariableOp^dense_4/BiasAdd/ReadVariableOp^dense_4/MatMul/ReadVariableOp^embedding/embedding_lookup^embedding_1/embedding_lookup8^integer_lookup/None_lookup_table_find/LookupTableFindV27^string_lookup/None_lookup_table_find/LookupTableFindV29^string_lookup_1/None_lookup_table_find/LookupTableFindV29^string_lookup_2/None_lookup_table_find/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2J
#batch_normalization/AssignMovingAvg#batch_normalization/AssignMovingAvg2h
2batch_normalization/AssignMovingAvg/ReadVariableOp2batch_normalization/AssignMovingAvg/ReadVariableOp2N
%batch_normalization/AssignMovingAvg_1%batch_normalization/AssignMovingAvg_12l
4batch_normalization/AssignMovingAvg_1/ReadVariableOp4batch_normalization/AssignMovingAvg_1/ReadVariableOp2\
,batch_normalization/batchnorm/ReadVariableOp,batch_normalization/batchnorm/ReadVariableOp2d
0batch_normalization/batchnorm/mul/ReadVariableOp0batch_normalization/batchnorm/mul/ReadVariableOp2N
%batch_normalization_1/AssignMovingAvg%batch_normalization_1/AssignMovingAvg2l
4batch_normalization_1/AssignMovingAvg/ReadVariableOp4batch_normalization_1/AssignMovingAvg/ReadVariableOp2R
'batch_normalization_1/AssignMovingAvg_1'batch_normalization_1/AssignMovingAvg_12p
6batch_normalization_1/AssignMovingAvg_1/ReadVariableOp6batch_normalization_1/AssignMovingAvg_1/ReadVariableOp2`
.batch_normalization_1/batchnorm/ReadVariableOp.batch_normalization_1/batchnorm/ReadVariableOp2h
2batch_normalization_1/batchnorm/mul/ReadVariableOp2batch_normalization_1/batchnorm/mul/ReadVariableOp2N
%batch_normalization_2/AssignMovingAvg%batch_normalization_2/AssignMovingAvg2l
4batch_normalization_2/AssignMovingAvg/ReadVariableOp4batch_normalization_2/AssignMovingAvg/ReadVariableOp2R
'batch_normalization_2/AssignMovingAvg_1'batch_normalization_2/AssignMovingAvg_12p
6batch_normalization_2/AssignMovingAvg_1/ReadVariableOp6batch_normalization_2/AssignMovingAvg_1/ReadVariableOp2`
.batch_normalization_2/batchnorm/ReadVariableOp.batch_normalization_2/batchnorm/ReadVariableOp2h
2batch_normalization_2/batchnorm/mul/ReadVariableOp2batch_normalization_2/batchnorm/mul/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp2@
dense_1/BiasAdd/ReadVariableOpdense_1/BiasAdd/ReadVariableOp2>
dense_1/MatMul/ReadVariableOpdense_1/MatMul/ReadVariableOp2@
dense_2/BiasAdd/ReadVariableOpdense_2/BiasAdd/ReadVariableOp2>
dense_2/MatMul/ReadVariableOpdense_2/MatMul/ReadVariableOp2@
dense_3/BiasAdd/ReadVariableOpdense_3/BiasAdd/ReadVariableOp2>
dense_3/MatMul/ReadVariableOpdense_3/MatMul/ReadVariableOp2@
dense_4/BiasAdd/ReadVariableOpdense_4/BiasAdd/ReadVariableOp2>
dense_4/MatMul/ReadVariableOpdense_4/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup2<
embedding_1/embedding_lookupembedding_1/embedding_lookup2r
7integer_lookup/None_lookup_table_find/LookupTableFindV27integer_lookup/None_lookup_table_find/LookupTableFindV22p
6string_lookup/None_lookup_table_find/LookupTableFindV26string_lookup/None_lookup_table_find/LookupTableFindV22t
8string_lookup_1/None_lookup_table_find/LookupTableFindV28string_lookup_1/None_lookup_table_find/LookupTableFindV22t
8string_lookup_2/None_lookup_table_find/LookupTableFindV28string_lookup_2/None_lookup_table_find/LookupTableFindV2:O K
#
_output_shapes
:���������
$
_user_specified_name
inputs/age:RN
#
_output_shapes
:���������
'
_user_specified_nameinputs/gender:UQ
'
_output_shapes
:���������
&
_user_specified_nameinputs/genre:XT
(
_output_shapes
:����������
(
_user_specified_nameinputs/history:SO
#
_output_shapes
:���������
(
_user_specified_nameinputs/item_id:VR
#
_output_shapes
:���������
+
_user_specified_nameinputs/occupation:XT
#
_output_shapes
:���������
-
_user_specified_nameinputs/release_date:TP
#
_output_shapes
:���������
)
_user_specified_nameinputs/zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
z
%__inference_dense_layer_call_fn_52403

inputs
unknown:	�)
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_507862
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������)2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:����������: 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
5__inference_batch_normalization_2_layer_call_fn_52778

inputs
unknown:	�
	unknown_0:	�
	unknown_1:	�
	unknown_2:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_506142
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
$__inference_deep_layer_call_fn_52373

inputs_age
inputs_gender
inputs_genre
inputs_history
inputs_item_id
inputs_occupation
inputs_release_date
inputs_zip_code
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3
	unknown_4	
	unknown_5
	unknown_6	
	unknown_7:	�
	unknown_8:	�)
	unknown_9:	�)

unknown_10:	�

unknown_11:	�

unknown_12:	�

unknown_13:	�

unknown_14:
��

unknown_15:	�

unknown_16:	�

unknown_17:	�

unknown_18:	�

unknown_19:	�

unknown_20:
��

unknown_21:	�

unknown_22:	�

unknown_23:	�

unknown_24:	�

unknown_25:	�

unknown_26:	�@

unknown_27:@

unknown_28:@

unknown_29:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCall
inputs_ageinputs_genderinputs_genreinputs_historyinputs_item_idinputs_occupationinputs_release_dateinputs_zip_codeunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29*2
Tin+
)2'				*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*3
_read_only_resource_inputs
!"#$%&*0
config_proto 

CPU

GPU2*0J 8� *H
fCRA
?__inference_deep_layer_call_and_return_conditional_losses_513612
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
#
_output_shapes
:���������
$
_user_specified_name
inputs/age:RN
#
_output_shapes
:���������
'
_user_specified_nameinputs/gender:UQ
'
_output_shapes
:���������
&
_user_specified_nameinputs/genre:XT
(
_output_shapes
:����������
(
_user_specified_nameinputs/history:SO
#
_output_shapes
:���������
(
_user_specified_nameinputs/item_id:VR
#
_output_shapes
:���������
+
_user_specified_nameinputs/occupation:XT
#
_output_shapes
:���������
-
_user_specified_nameinputs/release_date:TP
#
_output_shapes
:���������
)
_user_specified_nameinputs/zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
5__inference_batch_normalization_1_layer_call_fn_52638

inputs
unknown:	�
	unknown_0:	�
	unknown_1:	�
	unknown_2:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_503922
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
__inference_save_fn_52924
checkpoint_key[
Wstring_lookup_2_index_table_lookup_table_export_values_lookuptableexportv2_table_handle
identity

identity_1

identity_2

identity_3

identity_4

identity_5	��Jstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2�
Jstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2Wstring_lookup_2_index_table_lookup_table_export_values_lookuptableexportv2_table_handle",/job:localhost/replica:0/task:0/device:CPU:0*
Tkeys0*
Tvalues0	*
_output_shapes

::2L
Jstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2T
add/yConst*
_output_shapes
: *
dtype0*
valueB B-keys2
add/yR
addAddcheckpoint_keyadd/y:output:0*
T0*
_output_shapes
: 2
addZ
add_1/yConst*
_output_shapes
: *
dtype0*
valueB B-values2	
add_1/yX
add_1Addcheckpoint_keyadd_1/y:output:0*
T0*
_output_shapes
: 2
add_1Q
IdentityIdentityadd:z:0^NoOp*
T0*
_output_shapes
: 2

IdentityO
ConstConst*
_output_shapes
: *
dtype0*
valueB B 2
Const\

Identity_1IdentityConst:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1�

Identity_2IdentityQstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2:keys:0^NoOp*
T0*
_output_shapes
:2

Identity_2W

Identity_3Identity	add_1:z:0^NoOp*
T0*
_output_shapes
: 2

Identity_3S
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 2	
Const_1^

Identity_4IdentityConst_1:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_4�

Identity_5IdentitySstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2:values:0^NoOp*
T0	*
_output_shapes
:2

Identity_5�
NoOpNoOpK^string_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : 2�
Jstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2Jstring_lookup_2_index_table_lookup_table_export_values/LookupTableExportV2:F B

_output_shapes
: 
(
_user_specified_namecheckpoint_key
�
�
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_52718

inputs0
!batchnorm_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�2
#batchnorm_readvariableop_1_resource:	�2
#batchnorm_readvariableop_2_resource:	�
identity��batchnorm/ReadVariableOp�batchnorm/ReadVariableOp_1�batchnorm/ReadVariableOp_2�batchnorm/mul/ReadVariableOp�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^batchnorm/ReadVariableOp^batchnorm/ReadVariableOp_1^batchnorm/ReadVariableOp_2^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp28
batchnorm/ReadVariableOp_1batchnorm/ReadVariableOp_128
batchnorm/ReadVariableOp_2batchnorm/ReadVariableOp_22<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
B__inference_dense_3_layer_call_and_return_conditional_losses_52789

inputs1
matmul_readvariableop_resource:	�@-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2	
BiasAddX
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������@2
Relum
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������@2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
*
__inference_<lambda>_53023
identityS
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
�
B__inference_dense_2_layer_call_and_return_conditional_losses_50875

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
5__inference_batch_normalization_2_layer_call_fn_52765

inputs
unknown:	�
	unknown_0:	�
	unknown_1:	�
	unknown_2:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_505542
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
'__inference_dense_4_layer_call_fn_52845

inputs
unknown:@
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_4_layer_call_and_return_conditional_losses_509322
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������@: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
+__inference_concatenate_layer_call_fn_52444
inputs_0
inputs_1
inputs_2
inputs_3
inputs_4
inputs_5
inputs_6
inputs_7
identity�
PartitionedCallPartitionedCallinputs_0inputs_1inputs_2inputs_3inputs_4inputs_5inputs_6inputs_7*
Tin

2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_concatenate_layer_call_and_return_conditional_losses_508202
PartitionedCallm
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:���������:���������):���������:���������):���������:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3:QM
'
_output_shapes
:���������)
"
_user_specified_name
inputs/4:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/5:QM
'
_output_shapes
:���������)
"
_user_specified_name
inputs/6:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/7
�
E
)__inference_dropout_2_layer_call_fn_52820

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_2_layer_call_and_return_conditional_losses_509192
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������@2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������@:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�

�
__inference_restore_fn_53013
restored_tensors_0
restored_tensors_1	N
Jstring_lookup_1_index_table_table_restore_lookuptableimportv2_table_handle
identity��=string_lookup_1_index_table_table_restore/LookupTableImportV2�
=string_lookup_1_index_table_table_restore/LookupTableImportV2LookupTableImportV2Jstring_lookup_1_index_table_table_restore_lookuptableimportv2_table_handlerestored_tensors_0restored_tensors_1",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
 2?
=string_lookup_1_index_table_table_restore/LookupTableImportV2P
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstX
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: 2

Identity�
NoOpNoOp>^string_lookup_1_index_table_table_restore/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes

::: 2~
=string_lookup_1_index_table_table_restore/LookupTableImportV2=string_lookup_1_index_table_table_restore/LookupTableImportV2:L H

_output_shapes
:
,
_user_specified_namerestored_tensors_0:LH

_output_shapes
:
,
_user_specified_namerestored_tensors_1
�
b
D__inference_dropout_1_layer_call_and_return_conditional_losses_52676

inputs

identity_1[
IdentityIdentityinputs*
T0*(
_output_shapes
:����������2

Identityj

Identity_1IdentityIdentity:output:0*
T0*(
_output_shapes
:����������2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�*
�
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_50452

inputs6
'assignmovingavg_readvariableop_resource:	�8
)assignmovingavg_1_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�0
!batchnorm_readvariableop_resource:	�
identity��AssignMovingAvg�AssignMovingAvg/ReadVariableOp�AssignMovingAvg_1� AssignMovingAvg_1/ReadVariableOp�batchnorm/ReadVariableOp�batchnorm/mul/ReadVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/mean}
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes
:	�2
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*(
_output_shapes
:����������2
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze_1s
AssignMovingAvg/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOp'assignmovingavg_readvariableop_resource*
_output_shapes	
:�*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/mul�
AssignMovingAvgAssignSubVariableOp'assignmovingavg_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvgw
AssignMovingAvg_1/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp)assignmovingavg_1_readvariableop_resource*
_output_shapes	
:�*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/mul�
AssignMovingAvg_1AssignSubVariableOp)assignmovingavg_1_readvariableop_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvg_1g
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1|
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^AssignMovingAvg^AssignMovingAvg/ReadVariableOp^AssignMovingAvg_1!^AssignMovingAvg_1/ReadVariableOp^batchnorm/ReadVariableOp^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 2"
AssignMovingAvgAssignMovingAvg2@
AssignMovingAvg/ReadVariableOpAssignMovingAvg/ReadVariableOp2&
AssignMovingAvg_1AssignMovingAvg_12D
 AssignMovingAvg_1/ReadVariableOp AssignMovingAvg_1/ReadVariableOp24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp2<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
3__inference_batch_normalization_layer_call_fn_52511

inputs
unknown:	�
	unknown_0:	�
	unknown_1:	�
	unknown_2:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_batch_normalization_layer_call_and_return_conditional_losses_502302
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�*
�
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_52625

inputs6
'assignmovingavg_readvariableop_resource:	�8
)assignmovingavg_1_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�0
!batchnorm_readvariableop_resource:	�
identity��AssignMovingAvg�AssignMovingAvg/ReadVariableOp�AssignMovingAvg_1� AssignMovingAvg_1/ReadVariableOp�batchnorm/ReadVariableOp�batchnorm/mul/ReadVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/mean}
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes
:	�2
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*(
_output_shapes
:����������2
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze_1s
AssignMovingAvg/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOp'assignmovingavg_readvariableop_resource*
_output_shapes	
:�*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/mul�
AssignMovingAvgAssignSubVariableOp'assignmovingavg_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvgw
AssignMovingAvg_1/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp)assignmovingavg_1_readvariableop_resource*
_output_shapes	
:�*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/mul�
AssignMovingAvg_1AssignSubVariableOp)assignmovingavg_1_readvariableop_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvg_1g
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1|
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^AssignMovingAvg^AssignMovingAvg/ReadVariableOp^AssignMovingAvg_1!^AssignMovingAvg_1/ReadVariableOp^batchnorm/ReadVariableOp^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 2"
AssignMovingAvgAssignMovingAvg2@
AssignMovingAvg/ReadVariableOpAssignMovingAvg/ReadVariableOp2&
AssignMovingAvg_1AssignMovingAvg_12D
 AssignMovingAvg_1/ReadVariableOp AssignMovingAvg_1/ReadVariableOp24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp2<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
P
__inference__creator_52865
identity:	 ��integer_lookup_index_table�
integer_lookup_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0	*
shared_nametable_25928*
value_dtype0	2
integer_lookup_index_tables
IdentityIdentity)integer_lookup_index_table:table_handle:0^NoOp*
T0*
_output_shapes
: 2

Identityk
NoOpNoOp^integer_lookup_index_table*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 28
integer_lookup_index_tableinteger_lookup_index_table
�
b
D__inference_dropout_2_layer_call_and_return_conditional_losses_52803

inputs

identity_1Z
IdentityIdentityinputs*
T0*'
_output_shapes
:���������@2

Identityi

Identity_1IdentityIdentity:output:0*
T0*'
_output_shapes
:���������@2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������@:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
.
__inference__initializer_52855
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
�
N__inference_batch_normalization_layer_call_and_return_conditional_losses_52464

inputs0
!batchnorm_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�2
#batchnorm_readvariableop_1_resource:	�2
#batchnorm_readvariableop_2_resource:	�
identity��batchnorm/ReadVariableOp�batchnorm/ReadVariableOp_1�batchnorm/ReadVariableOp_2�batchnorm/mul/ReadVariableOp�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^batchnorm/ReadVariableOp^batchnorm/ReadVariableOp_1^batchnorm/ReadVariableOp_2^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp28
batchnorm/ReadVariableOp_1batchnorm/ReadVariableOp_128
batchnorm/ReadVariableOp_2batchnorm/ReadVariableOp_22<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�	
�
F__inference_embedding_1_layer_call_and_return_conditional_losses_50801

inputs	)
embedding_lookup_50795:	�)
identity��embedding_lookup�
embedding_lookupResourceGatherembedding_lookup_50795inputs",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*)
_class
loc:@embedding_lookup/50795*'
_output_shapes
:���������)*
dtype02
embedding_lookup�
embedding_lookup/IdentityIdentityembedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*)
_class
loc:@embedding_lookup/50795*'
_output_shapes
:���������)2
embedding_lookup/Identity�
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������)2
embedding_lookup/Identity_1
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*'
_output_shapes
:���������)2

Identitya
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*$
_input_shapes
:���������: 2$
embedding_lookupembedding_lookup:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs
�
Q
__inference__creator_52850
identity: ��string_lookup_2_index_table�
string_lookup_2_index_tableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_nametable_25875*
value_dtype0	2
string_lookup_2_index_tablet
IdentityIdentity*string_lookup_2_index_table:table_handle:0^NoOp*
T0*
_output_shapes
: 2

Identityl
NoOpNoOp^string_lookup_2_index_table*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 2:
string_lookup_2_index_tablestring_lookup_2_index_table
�
.
__inference__initializer_52900
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
b
)__inference_dropout_1_layer_call_fn_52698

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_510672
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�

�
__inference_restore_fn_52986
restored_tensors_0
restored_tensors_1	L
Hstring_lookup_index_table_table_restore_lookuptableimportv2_table_handle
identity��;string_lookup_index_table_table_restore/LookupTableImportV2�
;string_lookup_index_table_table_restore/LookupTableImportV2LookupTableImportV2Hstring_lookup_index_table_table_restore_lookuptableimportv2_table_handlerestored_tensors_0restored_tensors_1",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
 2=
;string_lookup_index_table_table_restore/LookupTableImportV2P
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstX
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: 2

Identity�
NoOpNoOp<^string_lookup_index_table_table_restore/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes

::: 2z
;string_lookup_index_table_table_restore/LookupTableImportV2;string_lookup_index_table_table_restore/LookupTableImportV2:L H

_output_shapes
:
,
_user_specified_namerestored_tensors_0:LH

_output_shapes
:
,
_user_specified_namerestored_tensors_1
�
�
B__inference_dense_1_layer_call_and_return_conditional_losses_52535

inputs2
matmul_readvariableop_resource:
��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
MatMul/ReadVariableOpt
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2	
BiasAddY
ReluReluBiasAdd:output:0*
T0*(
_output_shapes
:����������2
Relun
IdentityIdentityRelu:activations:0^NoOp*
T0*(
_output_shapes
:����������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
5__inference_batch_normalization_1_layer_call_fn_52651

inputs
unknown:	�
	unknown_0:	�
	unknown_1:	�
	unknown_2:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_504522
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
$__inference_deep_layer_call_fn_51500
age

gender	
genre
history
item_id

occupation
release_date
zip_code
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3
	unknown_4	
	unknown_5
	unknown_6	
	unknown_7:	�
	unknown_8:	�)
	unknown_9:	�)

unknown_10:	�

unknown_11:	�

unknown_12:	�

unknown_13:	�

unknown_14:
��

unknown_15:	�

unknown_16:	�

unknown_17:	�

unknown_18:	�

unknown_19:	�

unknown_20:
��

unknown_21:	�

unknown_22:	�

unknown_23:	�

unknown_24:	�

unknown_25:	�

unknown_26:	�@

unknown_27:@

unknown_28:@

unknown_29:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallagegendergenrehistoryitem_id
occupationrelease_datezip_codeunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29*2
Tin+
)2'				*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*3
_read_only_resource_inputs
!"#$%&*0
config_proto 

CPU

GPU2*0J 8� *H
fCRA
?__inference_deep_layer_call_and_return_conditional_losses_513612
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:H D
#
_output_shapes
:���������

_user_specified_nameage:KG
#
_output_shapes
:���������
 
_user_specified_namegender:NJ
'
_output_shapes
:���������

_user_specified_namegenre:QM
(
_output_shapes
:����������
!
_user_specified_name	history:LH
#
_output_shapes
:���������
!
_user_specified_name	item_id:OK
#
_output_shapes
:���������
$
_user_specified_name
occupation:QM
#
_output_shapes
:���������
&
_user_specified_namerelease_date:MI
#
_output_shapes
:���������
"
_user_specified_name
zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�*
�
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_52752

inputs6
'assignmovingavg_readvariableop_resource:	�8
)assignmovingavg_1_readvariableop_resource:	�4
%batchnorm_mul_readvariableop_resource:	�0
!batchnorm_readvariableop_resource:	�
identity��AssignMovingAvg�AssignMovingAvg/ReadVariableOp�AssignMovingAvg_1� AssignMovingAvg_1/ReadVariableOp�batchnorm/ReadVariableOp�batchnorm/mul/ReadVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/mean}
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes
:	�2
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*(
_output_shapes
:����������2
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes
:	�*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes	
:�*
squeeze_dims
 2
moments/Squeeze_1s
AssignMovingAvg/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOp'assignmovingavg_readvariableop_resource*
_output_shapes	
:�*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg/mul�
AssignMovingAvgAssignSubVariableOp'assignmovingavg_readvariableop_resourceAssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvgw
AssignMovingAvg_1/decayConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOp)assignmovingavg_1_readvariableop_resource*
_output_shapes	
:�*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*
_output_shapes	
:�2
AssignMovingAvg_1/mul�
AssignMovingAvg_1AssignSubVariableOp)assignmovingavg_1_readvariableop_resourceAssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*
_output_shapes
 *
dtype02
AssignMovingAvg_1g
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2
batchnorm/addd
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes	
:�2
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2
batchnorm/mulw
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*(
_output_shapes
:����������2
batchnorm/mul_1|
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes	
:�2
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2
batchnorm/add_1o
IdentityIdentitybatchnorm/add_1:z:0^NoOp*
T0*(
_output_shapes
:����������2

Identity�
NoOpNoOp^AssignMovingAvg^AssignMovingAvg/ReadVariableOp^AssignMovingAvg_1!^AssignMovingAvg_1/ReadVariableOp^batchnorm/ReadVariableOp^batchnorm/mul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 2"
AssignMovingAvgAssignMovingAvg2@
AssignMovingAvg/ReadVariableOpAssignMovingAvg/ReadVariableOp2&
AssignMovingAvg_1AssignMovingAvg_12D
 AssignMovingAvg_1/ReadVariableOp AssignMovingAvg_1/ReadVariableOp24
batchnorm/ReadVariableOpbatchnorm/ReadVariableOp2<
batchnorm/mul/ReadVariableOpbatchnorm/mul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�

�
__inference_restore_fn_52959
restored_tensors_0	
restored_tensors_1	M
Iinteger_lookup_index_table_table_restore_lookuptableimportv2_table_handle
identity��<integer_lookup_index_table_table_restore/LookupTableImportV2�
<integer_lookup_index_table_table_restore/LookupTableImportV2LookupTableImportV2Iinteger_lookup_index_table_table_restore_lookuptableimportv2_table_handlerestored_tensors_0restored_tensors_1",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0	*

Tout0	*
_output_shapes
 2>
<integer_lookup_index_table_table_restore/LookupTableImportV2P
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstX
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: 2

Identity�
NoOpNoOp=^integer_lookup_index_table_table_restore/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes

::: 2|
<integer_lookup_index_table_table_restore/LookupTableImportV2<integer_lookup_index_table_table_restore/LookupTableImportV2:L H

_output_shapes
:
,
_user_specified_namerestored_tensors_0:LH

_output_shapes
:
,
_user_specified_namerestored_tensors_1
�
b
D__inference_dropout_1_layer_call_and_return_conditional_losses_50886

inputs

identity_1[
IdentityIdentityinputs*
T0*(
_output_shapes
:����������2

Identityj

Identity_1IdentityIdentity:output:0*
T0*(
_output_shapes
:����������2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
C
'__inference_dropout_layer_call_fn_52566

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_508532
PartitionedCallm
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
b
D__inference_dropout_2_layer_call_and_return_conditional_losses_50919

inputs

identity_1Z
IdentityIdentityinputs*
T0*'
_output_shapes
:���������@2

Identityi

Identity_1IdentityIdentity:output:0*
T0*'
_output_shapes
:���������@2

Identity_1"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������@:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
__inference_save_fn_52978
checkpoint_keyY
Ustring_lookup_index_table_lookup_table_export_values_lookuptableexportv2_table_handle
identity

identity_1

identity_2

identity_3

identity_4

identity_5	��Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2�
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2Ustring_lookup_index_table_lookup_table_export_values_lookuptableexportv2_table_handle",/job:localhost/replica:0/task:0/device:CPU:0*
Tkeys0*
Tvalues0	*
_output_shapes

::2J
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2T
add/yConst*
_output_shapes
: *
dtype0*
valueB B-keys2
add/yR
addAddcheckpoint_keyadd/y:output:0*
T0*
_output_shapes
: 2
addZ
add_1/yConst*
_output_shapes
: *
dtype0*
valueB B-values2	
add_1/yX
add_1Addcheckpoint_keyadd_1/y:output:0*
T0*
_output_shapes
: 2
add_1Q
IdentityIdentityadd:z:0^NoOp*
T0*
_output_shapes
: 2

IdentityO
ConstConst*
_output_shapes
: *
dtype0*
valueB B 2
Const\

Identity_1IdentityConst:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1�

Identity_2IdentityOstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:keys:0^NoOp*
T0*
_output_shapes
:2

Identity_2W

Identity_3Identity	add_1:z:0^NoOp*
T0*
_output_shapes
: 2

Identity_3S
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 2	
Const_1^

Identity_4IdentityConst_1:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_4�

Identity_5IdentityQstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:values:0^NoOp*
T0	*
_output_shapes
:2

Identity_5�
NoOpNoOpI^string_lookup_index_table_lookup_table_export_values/LookupTableExportV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : 2�
Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2Hstring_lookup_index_table_lookup_table_export_values/LookupTableExportV2:F B

_output_shapes
: 
(
_user_specified_namecheckpoint_key
�
�
'__inference_dense_3_layer_call_fn_52798

inputs
unknown:	�@
	unknown_0:@
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_3_layer_call_and_return_conditional_losses_509082
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������@2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
.
__inference__initializer_52885
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
�
__inference_save_fn_53005
checkpoint_key[
Wstring_lookup_1_index_table_lookup_table_export_values_lookuptableexportv2_table_handle
identity

identity_1

identity_2

identity_3

identity_4

identity_5	��Jstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2�
Jstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2LookupTableExportV2Wstring_lookup_1_index_table_lookup_table_export_values_lookuptableexportv2_table_handle",/job:localhost/replica:0/task:0/device:CPU:0*
Tkeys0*
Tvalues0	*
_output_shapes

::2L
Jstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2T
add/yConst*
_output_shapes
: *
dtype0*
valueB B-keys2
add/yR
addAddcheckpoint_keyadd/y:output:0*
T0*
_output_shapes
: 2
addZ
add_1/yConst*
_output_shapes
: *
dtype0*
valueB B-values2	
add_1/yX
add_1Addcheckpoint_keyadd_1/y:output:0*
T0*
_output_shapes
: 2
add_1Q
IdentityIdentityadd:z:0^NoOp*
T0*
_output_shapes
: 2

IdentityO
ConstConst*
_output_shapes
: *
dtype0*
valueB B 2
Const\

Identity_1IdentityConst:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1�

Identity_2IdentityQstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2:keys:0^NoOp*
T0*
_output_shapes
:2

Identity_2W

Identity_3Identity	add_1:z:0^NoOp*
T0*
_output_shapes
: 2

Identity_3S
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 2	
Const_1^

Identity_4IdentityConst_1:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_4�

Identity_5IdentitySstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2:values:0^NoOp*
T0	*
_output_shapes
:2

Identity_5�
NoOpNoOpK^string_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : 2�
Jstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2Jstring_lookup_1_index_table_lookup_table_export_values/LookupTableExportV2:F B

_output_shapes
: 
(
_user_specified_namecheckpoint_key
�
�
B__inference_dense_4_layer_call_and_return_conditional_losses_52836

inputs0
matmul_readvariableop_resource:@-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2	
BiasAdda
SigmoidSigmoidBiasAdd:output:0*
T0*'
_output_shapes
:���������2	
Sigmoidf
IdentityIdentitySigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
3__inference_batch_normalization_layer_call_fn_52524

inputs
unknown:	�
	unknown_0:	�
	unknown_1:	�
	unknown_2:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_batch_normalization_layer_call_and_return_conditional_losses_502902
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������: : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
,
__inference__destroyer_52890
identityP
ConstConst*
_output_shapes
: *
dtype0*
value	B :2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
`
'__inference_dropout_layer_call_fn_52571

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_511002
StatefulPartitionedCall|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
E
)__inference_dropout_1_layer_call_fn_52693

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_508862
PartitionedCallm
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:����������2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
*
__inference_<lambda>_53028
identityS
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ConstQ
IdentityIdentityConst:output:0*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
ϥ
�
?__inference_deep_layer_call_and_return_conditional_losses_50939

inputs
inputs_1
inputs_2
inputs_3
inputs_4
inputs_5
inputs_6
inputs_7H
Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleI
Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value	G
Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handleH
Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value	"
embedding_50776:	�
dense_50787:	�)$
embedding_1_50802:	�)(
batch_normalization_50822:	�(
batch_normalization_50824:	�(
batch_normalization_50826:	�(
batch_normalization_50828:	�!
dense_1_50843:
��
dense_1_50845:	�*
batch_normalization_1_50855:	�*
batch_normalization_1_50857:	�*
batch_normalization_1_50859:	�*
batch_normalization_1_50861:	�!
dense_2_50876:
��
dense_2_50878:	�*
batch_normalization_2_50888:	�*
batch_normalization_2_50890:	�*
batch_normalization_2_50892:	�*
batch_normalization_2_50894:	� 
dense_3_50909:	�@
dense_3_50911:@
dense_4_50933:@
dense_4_50935:
identity��+batch_normalization/StatefulPartitionedCall�-batch_normalization_1/StatefulPartitionedCall�-batch_normalization_2/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�dense_2/StatefulPartitionedCall�dense_3/StatefulPartitionedCall�dense_4/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�#embedding_1/StatefulPartitionedCall�7integer_lookup/None_lookup_table_find/LookupTableFindV2�6string_lookup/None_lookup_table_find/LookupTableFindV2�8string_lookup_1/None_lookup_table_find/LookupTableFindV2�8string_lookup_2/None_lookup_table_find/LookupTableFindV2y
integer_lookup/CastCastinputs_4*

DstT0	*

SrcT0*#
_output_shapes
:���������2
integer_lookup/Cast�
7integer_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleinteger_lookup/Cast:y:0Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0	*

Tout0	*#
_output_shapes
:���������29
7integer_lookup/None_lookup_table_find/LookupTableFindV2�
integer_lookup/IdentityIdentity@integer_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
integer_lookup/Identity�
8string_lookup_2/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleinputs_7Fstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:���������2:
8string_lookup_2/None_lookup_table_find/LookupTableFindV2�
string_lookup_2/IdentityIdentityAstring_lookup_2/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
string_lookup_2/Identity�
tf.expand_dims_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_1/ExpandDims/dim�
tf.expand_dims_1/ExpandDims
ExpandDimsinputs_1(tf.expand_dims_1/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_1/ExpandDims�
tf.expand_dims/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2
tf.expand_dims/ExpandDims/dim�
tf.expand_dims/ExpandDims
ExpandDimsinputs_5&tf.expand_dims/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims/ExpandDims�
6string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handle"tf.expand_dims/ExpandDims:output:0Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������28
6string_lookup/None_lookup_table_find/LookupTableFindV2�
string_lookup/bincount/ShapeShape?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2
string_lookup/bincount/Shape�
string_lookup/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
string_lookup/bincount/Const�
string_lookup/bincount/ProdProd%string_lookup/bincount/Shape:output:0%string_lookup/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup/bincount/Prod�
 string_lookup/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2"
 string_lookup/bincount/Greater/y�
string_lookup/bincount/GreaterGreater$string_lookup/bincount/Prod:output:0)string_lookup/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2 
string_lookup/bincount/Greater�
string_lookup/bincount/CastCast"string_lookup/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup/bincount/Cast�
string_lookup/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2 
string_lookup/bincount/Const_1�
string_lookup/bincount/MaxMax?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0'string_lookup/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/Max~
string_lookup/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2
string_lookup/bincount/add/y�
string_lookup/bincount/addAddV2#string_lookup/bincount/Max:output:0%string_lookup/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/add�
string_lookup/bincount/mulMulstring_lookup/bincount/Cast:y:0string_lookup/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/mul�
 string_lookup/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/minlength�
string_lookup/bincount/MaximumMaximum)string_lookup/bincount/minlength:output:0string_lookup/bincount/mul:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Maximum�
 string_lookup/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/maxlength�
string_lookup/bincount/MinimumMinimum)string_lookup/bincount/maxlength:output:0"string_lookup/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Minimum�
string_lookup/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2 
string_lookup/bincount/Const_2�
$string_lookup/bincount/DenseBincountDenseBincount?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0"string_lookup/bincount/Minimum:z:0'string_lookup/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2&
$string_lookup/bincount/DenseBincount�
8string_lookup_1/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handle$tf.expand_dims_1/ExpandDims:output:0Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������2:
8string_lookup_1/None_lookup_table_find/LookupTableFindV2�
string_lookup_1/bincount/ShapeShapeAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2 
string_lookup_1/bincount/Shape�
string_lookup_1/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2 
string_lookup_1/bincount/Const�
string_lookup_1/bincount/ProdProd'string_lookup_1/bincount/Shape:output:0'string_lookup_1/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup_1/bincount/Prod�
"string_lookup_1/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2$
"string_lookup_1/bincount/Greater/y�
 string_lookup_1/bincount/GreaterGreater&string_lookup_1/bincount/Prod:output:0+string_lookup_1/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2"
 string_lookup_1/bincount/Greater�
string_lookup_1/bincount/CastCast$string_lookup_1/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup_1/bincount/Cast�
 string_lookup_1/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2"
 string_lookup_1/bincount/Const_1�
string_lookup_1/bincount/MaxMaxAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0)string_lookup_1/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/Max�
string_lookup_1/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2 
string_lookup_1/bincount/add/y�
string_lookup_1/bincount/addAddV2%string_lookup_1/bincount/Max:output:0'string_lookup_1/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/add�
string_lookup_1/bincount/mulMul!string_lookup_1/bincount/Cast:y:0 string_lookup_1/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/mul�
"string_lookup_1/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/minlength�
 string_lookup_1/bincount/MaximumMaximum+string_lookup_1/bincount/minlength:output:0 string_lookup_1/bincount/mul:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Maximum�
"string_lookup_1/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/maxlength�
 string_lookup_1/bincount/MinimumMinimum+string_lookup_1/bincount/maxlength:output:0$string_lookup_1/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Minimum�
 string_lookup_1/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2"
 string_lookup_1/bincount/Const_2�
&string_lookup_1/bincount/DenseBincountDenseBincountAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0$string_lookup_1/bincount/Minimum:z:0)string_lookup_1/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2(
&string_lookup_1/bincount/DenseBincount�
!embedding/StatefulPartitionedCallStatefulPartitionedCall!string_lookup_2/Identity:output:0embedding_50776*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_507752#
!embedding/StatefulPartitionedCall�
dense/StatefulPartitionedCallStatefulPartitionedCallinputs_3dense_50787*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_507862
dense/StatefulPartitionedCall�
tf.expand_dims_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_2/ExpandDims/dim�
tf.expand_dims_2/ExpandDims
ExpandDimsinputs(tf.expand_dims_2/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_2/ExpandDims�
#embedding_1/StatefulPartitionedCallStatefulPartitionedCall integer_lookup/Identity:output:0embedding_1_50802*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������)*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_embedding_1_layer_call_and_return_conditional_losses_508012%
#embedding_1/StatefulPartitionedCall�
tf.expand_dims_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_3/ExpandDims/dim�
tf.expand_dims_3/ExpandDims
ExpandDimsinputs_6(tf.expand_dims_3/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_3/ExpandDims�
concatenate/PartitionedCallPartitionedCall-string_lookup/bincount/DenseBincount:output:0inputs_2/string_lookup_1/bincount/DenseBincount:output:0*embedding/StatefulPartitionedCall:output:0&dense/StatefulPartitionedCall:output:0$tf.expand_dims_2/ExpandDims:output:0,embedding_1/StatefulPartitionedCall:output:0$tf.expand_dims_3/ExpandDims:output:0*
Tin

2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_concatenate_layer_call_and_return_conditional_losses_508202
concatenate/PartitionedCall�
+batch_normalization/StatefulPartitionedCallStatefulPartitionedCall$concatenate/PartitionedCall:output:0batch_normalization_50822batch_normalization_50824batch_normalization_50826batch_normalization_50828*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *W
fRRP
N__inference_batch_normalization_layer_call_and_return_conditional_losses_502302-
+batch_normalization/StatefulPartitionedCall�
dense_1/StatefulPartitionedCallStatefulPartitionedCall4batch_normalization/StatefulPartitionedCall:output:0dense_1_50843dense_1_50845*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_1_layer_call_and_return_conditional_losses_508422!
dense_1/StatefulPartitionedCall�
dropout/PartitionedCallPartitionedCall(dense_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_508532
dropout/PartitionedCall�
-batch_normalization_1/StatefulPartitionedCallStatefulPartitionedCall dropout/PartitionedCall:output:0batch_normalization_1_50855batch_normalization_1_50857batch_normalization_1_50859batch_normalization_1_50861*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_503922/
-batch_normalization_1/StatefulPartitionedCall�
dense_2/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_1/StatefulPartitionedCall:output:0dense_2_50876dense_2_50878*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_2_layer_call_and_return_conditional_losses_508752!
dense_2/StatefulPartitionedCall�
dropout_1/PartitionedCallPartitionedCall(dense_2/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_508862
dropout_1/PartitionedCall�
-batch_normalization_2/StatefulPartitionedCallStatefulPartitionedCall"dropout_1/PartitionedCall:output:0batch_normalization_2_50888batch_normalization_2_50890batch_normalization_2_50892batch_normalization_2_50894*
Tin	
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������*&
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Y
fTRR
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_505542/
-batch_normalization_2/StatefulPartitionedCall�
dense_3/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_2/StatefulPartitionedCall:output:0dense_3_50909dense_3_50911*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_3_layer_call_and_return_conditional_losses_509082!
dense_3/StatefulPartitionedCall�
dropout_2/PartitionedCallPartitionedCall(dense_3/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dropout_2_layer_call_and_return_conditional_losses_509192
dropout_2/PartitionedCall�
dense_4/StatefulPartitionedCallStatefulPartitionedCall"dropout_2/PartitionedCall:output:0dense_4_50933dense_4_50935*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *K
fFRD
B__inference_dense_4_layer_call_and_return_conditional_losses_509322!
dense_4/StatefulPartitionedCall�
IdentityIdentity(dense_4/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp,^batch_normalization/StatefulPartitionedCall.^batch_normalization_1/StatefulPartitionedCall.^batch_normalization_2/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall ^dense_2/StatefulPartitionedCall ^dense_3/StatefulPartitionedCall ^dense_4/StatefulPartitionedCall"^embedding/StatefulPartitionedCall$^embedding_1/StatefulPartitionedCall8^integer_lookup/None_lookup_table_find/LookupTableFindV27^string_lookup/None_lookup_table_find/LookupTableFindV29^string_lookup_1/None_lookup_table_find/LookupTableFindV29^string_lookup_2/None_lookup_table_find/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2Z
+batch_normalization/StatefulPartitionedCall+batch_normalization/StatefulPartitionedCall2^
-batch_normalization_1/StatefulPartitionedCall-batch_normalization_1/StatefulPartitionedCall2^
-batch_normalization_2/StatefulPartitionedCall-batch_normalization_2/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall2B
dense_2/StatefulPartitionedCalldense_2/StatefulPartitionedCall2B
dense_3/StatefulPartitionedCalldense_3/StatefulPartitionedCall2B
dense_4/StatefulPartitionedCalldense_4/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall2J
#embedding_1/StatefulPartitionedCall#embedding_1/StatefulPartitionedCall2r
7integer_lookup/None_lookup_table_find/LookupTableFindV27integer_lookup/None_lookup_table_find/LookupTableFindV22p
6string_lookup/None_lookup_table_find/LookupTableFindV26string_lookup/None_lookup_table_find/LookupTableFindV22t
8string_lookup_1/None_lookup_table_find/LookupTableFindV28string_lookup_1/None_lookup_table_find/LookupTableFindV22t
8string_lookup_2/None_lookup_table_find/LookupTableFindV28string_lookup_2/None_lookup_table_find/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:PL
(
_output_shapes
:����������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:KG
#
_output_shapes
:���������
 
_user_specified_nameinputs:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
��
�
?__inference_deep_layer_call_and_return_conditional_losses_52000

inputs_age
inputs_gender
inputs_genre
inputs_history
inputs_item_id
inputs_occupation
inputs_release_date
inputs_zip_codeH
Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleI
Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value	G
Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handleH
Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value	I
Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handleJ
Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value	3
 embedding_embedding_lookup_51901:	�7
$dense_matmul_readvariableop_resource:	�)5
"embedding_1_embedding_lookup_51911:	�)D
5batch_normalization_batchnorm_readvariableop_resource:	�H
9batch_normalization_batchnorm_mul_readvariableop_resource:	�F
7batch_normalization_batchnorm_readvariableop_1_resource:	�F
7batch_normalization_batchnorm_readvariableop_2_resource:	�:
&dense_1_matmul_readvariableop_resource:
��6
'dense_1_biasadd_readvariableop_resource:	�F
7batch_normalization_1_batchnorm_readvariableop_resource:	�J
;batch_normalization_1_batchnorm_mul_readvariableop_resource:	�H
9batch_normalization_1_batchnorm_readvariableop_1_resource:	�H
9batch_normalization_1_batchnorm_readvariableop_2_resource:	�:
&dense_2_matmul_readvariableop_resource:
��6
'dense_2_biasadd_readvariableop_resource:	�F
7batch_normalization_2_batchnorm_readvariableop_resource:	�J
;batch_normalization_2_batchnorm_mul_readvariableop_resource:	�H
9batch_normalization_2_batchnorm_readvariableop_1_resource:	�H
9batch_normalization_2_batchnorm_readvariableop_2_resource:	�9
&dense_3_matmul_readvariableop_resource:	�@5
'dense_3_biasadd_readvariableop_resource:@8
&dense_4_matmul_readvariableop_resource:@5
'dense_4_biasadd_readvariableop_resource:
identity��,batch_normalization/batchnorm/ReadVariableOp�.batch_normalization/batchnorm/ReadVariableOp_1�.batch_normalization/batchnorm/ReadVariableOp_2�0batch_normalization/batchnorm/mul/ReadVariableOp�.batch_normalization_1/batchnorm/ReadVariableOp�0batch_normalization_1/batchnorm/ReadVariableOp_1�0batch_normalization_1/batchnorm/ReadVariableOp_2�2batch_normalization_1/batchnorm/mul/ReadVariableOp�.batch_normalization_2/batchnorm/ReadVariableOp�0batch_normalization_2/batchnorm/ReadVariableOp_1�0batch_normalization_2/batchnorm/ReadVariableOp_2�2batch_normalization_2/batchnorm/mul/ReadVariableOp�dense/MatMul/ReadVariableOp�dense_1/BiasAdd/ReadVariableOp�dense_1/MatMul/ReadVariableOp�dense_2/BiasAdd/ReadVariableOp�dense_2/MatMul/ReadVariableOp�dense_3/BiasAdd/ReadVariableOp�dense_3/MatMul/ReadVariableOp�dense_4/BiasAdd/ReadVariableOp�dense_4/MatMul/ReadVariableOp�embedding/embedding_lookup�embedding_1/embedding_lookup�7integer_lookup/None_lookup_table_find/LookupTableFindV2�6string_lookup/None_lookup_table_find/LookupTableFindV2�8string_lookup_1/None_lookup_table_find/LookupTableFindV2�8string_lookup_2/None_lookup_table_find/LookupTableFindV2
integer_lookup/CastCastinputs_item_id*

DstT0	*

SrcT0*#
_output_shapes
:���������2
integer_lookup/Cast�
7integer_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Dinteger_lookup_none_lookup_table_find_lookuptablefindv2_table_handleinteger_lookup/Cast:y:0Einteger_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0	*

Tout0	*#
_output_shapes
:���������29
7integer_lookup/None_lookup_table_find/LookupTableFindV2�
integer_lookup/IdentityIdentity@integer_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
integer_lookup/Identity�
8string_lookup_2/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_2_none_lookup_table_find_lookuptablefindv2_table_handleinputs_zip_codeFstring_lookup_2_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*#
_output_shapes
:���������2:
8string_lookup_2/None_lookup_table_find/LookupTableFindV2�
string_lookup_2/IdentityIdentityAstring_lookup_2/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:���������2
string_lookup_2/Identity�
tf.expand_dims_1/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_1/ExpandDims/dim�
tf.expand_dims_1/ExpandDims
ExpandDimsinputs_gender(tf.expand_dims_1/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_1/ExpandDims�
tf.expand_dims/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2
tf.expand_dims/ExpandDims/dim�
tf.expand_dims/ExpandDims
ExpandDimsinputs_occupation&tf.expand_dims/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims/ExpandDims�
6string_lookup/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Cstring_lookup_none_lookup_table_find_lookuptablefindv2_table_handle"tf.expand_dims/ExpandDims:output:0Dstring_lookup_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������28
6string_lookup/None_lookup_table_find/LookupTableFindV2�
string_lookup/bincount/ShapeShape?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2
string_lookup/bincount/Shape�
string_lookup/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2
string_lookup/bincount/Const�
string_lookup/bincount/ProdProd%string_lookup/bincount/Shape:output:0%string_lookup/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup/bincount/Prod�
 string_lookup/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2"
 string_lookup/bincount/Greater/y�
string_lookup/bincount/GreaterGreater$string_lookup/bincount/Prod:output:0)string_lookup/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2 
string_lookup/bincount/Greater�
string_lookup/bincount/CastCast"string_lookup/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup/bincount/Cast�
string_lookup/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2 
string_lookup/bincount/Const_1�
string_lookup/bincount/MaxMax?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0'string_lookup/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/Max~
string_lookup/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2
string_lookup/bincount/add/y�
string_lookup/bincount/addAddV2#string_lookup/bincount/Max:output:0%string_lookup/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/add�
string_lookup/bincount/mulMulstring_lookup/bincount/Cast:y:0string_lookup/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup/bincount/mul�
 string_lookup/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/minlength�
string_lookup/bincount/MaximumMaximum)string_lookup/bincount/minlength:output:0string_lookup/bincount/mul:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Maximum�
 string_lookup/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2"
 string_lookup/bincount/maxlength�
string_lookup/bincount/MinimumMinimum)string_lookup/bincount/maxlength:output:0"string_lookup/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2 
string_lookup/bincount/Minimum�
string_lookup/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2 
string_lookup/bincount/Const_2�
$string_lookup/bincount/DenseBincountDenseBincount?string_lookup/None_lookup_table_find/LookupTableFindV2:values:0"string_lookup/bincount/Minimum:z:0'string_lookup/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2&
$string_lookup/bincount/DenseBincount�
8string_lookup_1/None_lookup_table_find/LookupTableFindV2LookupTableFindV2Estring_lookup_1_none_lookup_table_find_lookuptablefindv2_table_handle$tf.expand_dims_1/ExpandDims:output:0Fstring_lookup_1_none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*'
_output_shapes
:���������2:
8string_lookup_1/None_lookup_table_find/LookupTableFindV2�
string_lookup_1/bincount/ShapeShapeAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:2 
string_lookup_1/bincount/Shape�
string_lookup_1/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: 2 
string_lookup_1/bincount/Const�
string_lookup_1/bincount/ProdProd'string_lookup_1/bincount/Shape:output:0'string_lookup_1/bincount/Const:output:0*
T0*
_output_shapes
: 2
string_lookup_1/bincount/Prod�
"string_lookup_1/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : 2$
"string_lookup_1/bincount/Greater/y�
 string_lookup_1/bincount/GreaterGreater&string_lookup_1/bincount/Prod:output:0+string_lookup_1/bincount/Greater/y:output:0*
T0*
_output_shapes
: 2"
 string_lookup_1/bincount/Greater�
string_lookup_1/bincount/CastCast$string_lookup_1/bincount/Greater:z:0*

DstT0	*

SrcT0
*
_output_shapes
: 2
string_lookup_1/bincount/Cast�
 string_lookup_1/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB"       2"
 string_lookup_1/bincount/Const_1�
string_lookup_1/bincount/MaxMaxAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0)string_lookup_1/bincount/Const_1:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/Max�
string_lookup_1/bincount/add/yConst*
_output_shapes
: *
dtype0	*
value	B	 R2 
string_lookup_1/bincount/add/y�
string_lookup_1/bincount/addAddV2%string_lookup_1/bincount/Max:output:0'string_lookup_1/bincount/add/y:output:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/add�
string_lookup_1/bincount/mulMul!string_lookup_1/bincount/Cast:y:0 string_lookup_1/bincount/add:z:0*
T0	*
_output_shapes
: 2
string_lookup_1/bincount/mul�
"string_lookup_1/bincount/minlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/minlength�
 string_lookup_1/bincount/MaximumMaximum+string_lookup_1/bincount/minlength:output:0 string_lookup_1/bincount/mul:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Maximum�
"string_lookup_1/bincount/maxlengthConst*
_output_shapes
: *
dtype0	*
value	B	 R2$
"string_lookup_1/bincount/maxlength�
 string_lookup_1/bincount/MinimumMinimum+string_lookup_1/bincount/maxlength:output:0$string_lookup_1/bincount/Maximum:z:0*
T0	*
_output_shapes
: 2"
 string_lookup_1/bincount/Minimum�
 string_lookup_1/bincount/Const_2Const*
_output_shapes
: *
dtype0*
valueB 2"
 string_lookup_1/bincount/Const_2�
&string_lookup_1/bincount/DenseBincountDenseBincountAstring_lookup_1/None_lookup_table_find/LookupTableFindV2:values:0$string_lookup_1/bincount/Minimum:z:0)string_lookup_1/bincount/Const_2:output:0*
T0*

Tidx0	*'
_output_shapes
:���������*
binary_output(2(
&string_lookup_1/bincount/DenseBincount�
embedding/embedding_lookupResourceGather embedding_embedding_lookup_51901!string_lookup_2/Identity:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*3
_class)
'%loc:@embedding/embedding_lookup/51901*'
_output_shapes
:���������*
dtype02
embedding/embedding_lookup�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*3
_class)
'%loc:@embedding/embedding_lookup/51901*'
_output_shapes
:���������2%
#embedding/embedding_lookup/Identity�
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������2'
%embedding/embedding_lookup/Identity_1�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes
:	�)*
dtype02
dense/MatMul/ReadVariableOp�
dense/MatMulMatMulinputs_history#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������)2
dense/MatMul�
tf.expand_dims_2/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_2/ExpandDims/dim�
tf.expand_dims_2/ExpandDims
ExpandDims
inputs_age(tf.expand_dims_2/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_2/ExpandDims�
embedding_1/embedding_lookupResourceGather"embedding_1_embedding_lookup_51911 integer_lookup/Identity:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
Tindices0	*5
_class+
)'loc:@embedding_1/embedding_lookup/51911*'
_output_shapes
:���������)*
dtype02
embedding_1/embedding_lookup�
%embedding_1/embedding_lookup/IdentityIdentity%embedding_1/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:GPU:0*
T0*5
_class+
)'loc:@embedding_1/embedding_lookup/51911*'
_output_shapes
:���������)2'
%embedding_1/embedding_lookup/Identity�
'embedding_1/embedding_lookup/Identity_1Identity.embedding_1/embedding_lookup/Identity:output:0*
T0*'
_output_shapes
:���������)2)
'embedding_1/embedding_lookup/Identity_1�
tf.expand_dims_3/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
���������2!
tf.expand_dims_3/ExpandDims/dim�
tf.expand_dims_3/ExpandDims
ExpandDimsinputs_release_date(tf.expand_dims_3/ExpandDims/dim:output:0*
T0*'
_output_shapes
:���������2
tf.expand_dims_3/ExpandDimst
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis�
concatenate/concatConcatV2-string_lookup/bincount/DenseBincount:output:0inputs_genre/string_lookup_1/bincount/DenseBincount:output:0.embedding/embedding_lookup/Identity_1:output:0dense/MatMul:product:0$tf.expand_dims_2/ExpandDims:output:00embedding_1/embedding_lookup/Identity_1:output:0$tf.expand_dims_3/ExpandDims:output:0 concatenate/concat/axis:output:0*
N*
T0*(
_output_shapes
:����������2
concatenate/concat�
,batch_normalization/batchnorm/ReadVariableOpReadVariableOp5batch_normalization_batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,batch_normalization/batchnorm/ReadVariableOp�
#batch_normalization/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2%
#batch_normalization/batchnorm/add/y�
!batch_normalization/batchnorm/addAddV24batch_normalization/batchnorm/ReadVariableOp:value:0,batch_normalization/batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2#
!batch_normalization/batchnorm/add�
#batch_normalization/batchnorm/RsqrtRsqrt%batch_normalization/batchnorm/add:z:0*
T0*
_output_shapes	
:�2%
#batch_normalization/batchnorm/Rsqrt�
0batch_normalization/batchnorm/mul/ReadVariableOpReadVariableOp9batch_normalization_batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype022
0batch_normalization/batchnorm/mul/ReadVariableOp�
!batch_normalization/batchnorm/mulMul'batch_normalization/batchnorm/Rsqrt:y:08batch_normalization/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2#
!batch_normalization/batchnorm/mul�
#batch_normalization/batchnorm/mul_1Mulconcatenate/concat:output:0%batch_normalization/batchnorm/mul:z:0*
T0*(
_output_shapes
:����������2%
#batch_normalization/batchnorm/mul_1�
.batch_normalization/batchnorm/ReadVariableOp_1ReadVariableOp7batch_normalization_batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype020
.batch_normalization/batchnorm/ReadVariableOp_1�
#batch_normalization/batchnorm/mul_2Mul6batch_normalization/batchnorm/ReadVariableOp_1:value:0%batch_normalization/batchnorm/mul:z:0*
T0*
_output_shapes	
:�2%
#batch_normalization/batchnorm/mul_2�
.batch_normalization/batchnorm/ReadVariableOp_2ReadVariableOp7batch_normalization_batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype020
.batch_normalization/batchnorm/ReadVariableOp_2�
!batch_normalization/batchnorm/subSub6batch_normalization/batchnorm/ReadVariableOp_2:value:0'batch_normalization/batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2#
!batch_normalization/batchnorm/sub�
#batch_normalization/batchnorm/add_1AddV2'batch_normalization/batchnorm/mul_1:z:0%batch_normalization/batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2%
#batch_normalization/batchnorm/add_1�
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
dense_1/MatMul/ReadVariableOp�
dense_1/MatMulMatMul'batch_normalization/batchnorm/add_1:z:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_1/MatMul�
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02 
dense_1/BiasAdd/ReadVariableOp�
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_1/BiasAddq
dense_1/ReluReludense_1/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
dense_1/Relu
dropout/IdentityIdentitydense_1/Relu:activations:0*
T0*(
_output_shapes
:����������2
dropout/Identity�
.batch_normalization_1/batchnorm/ReadVariableOpReadVariableOp7batch_normalization_1_batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype020
.batch_normalization_1/batchnorm/ReadVariableOp�
%batch_normalization_1/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2'
%batch_normalization_1/batchnorm/add/y�
#batch_normalization_1/batchnorm/addAddV26batch_normalization_1/batchnorm/ReadVariableOp:value:0.batch_normalization_1/batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2%
#batch_normalization_1/batchnorm/add�
%batch_normalization_1/batchnorm/RsqrtRsqrt'batch_normalization_1/batchnorm/add:z:0*
T0*
_output_shapes	
:�2'
%batch_normalization_1/batchnorm/Rsqrt�
2batch_normalization_1/batchnorm/mul/ReadVariableOpReadVariableOp;batch_normalization_1_batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype024
2batch_normalization_1/batchnorm/mul/ReadVariableOp�
#batch_normalization_1/batchnorm/mulMul)batch_normalization_1/batchnorm/Rsqrt:y:0:batch_normalization_1/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2%
#batch_normalization_1/batchnorm/mul�
%batch_normalization_1/batchnorm/mul_1Muldropout/Identity:output:0'batch_normalization_1/batchnorm/mul:z:0*
T0*(
_output_shapes
:����������2'
%batch_normalization_1/batchnorm/mul_1�
0batch_normalization_1/batchnorm/ReadVariableOp_1ReadVariableOp9batch_normalization_1_batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype022
0batch_normalization_1/batchnorm/ReadVariableOp_1�
%batch_normalization_1/batchnorm/mul_2Mul8batch_normalization_1/batchnorm/ReadVariableOp_1:value:0'batch_normalization_1/batchnorm/mul:z:0*
T0*
_output_shapes	
:�2'
%batch_normalization_1/batchnorm/mul_2�
0batch_normalization_1/batchnorm/ReadVariableOp_2ReadVariableOp9batch_normalization_1_batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype022
0batch_normalization_1/batchnorm/ReadVariableOp_2�
#batch_normalization_1/batchnorm/subSub8batch_normalization_1/batchnorm/ReadVariableOp_2:value:0)batch_normalization_1/batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2%
#batch_normalization_1/batchnorm/sub�
%batch_normalization_1/batchnorm/add_1AddV2)batch_normalization_1/batchnorm/mul_1:z:0'batch_normalization_1/batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2'
%batch_normalization_1/batchnorm/add_1�
dense_2/MatMul/ReadVariableOpReadVariableOp&dense_2_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype02
dense_2/MatMul/ReadVariableOp�
dense_2/MatMulMatMul)batch_normalization_1/batchnorm/add_1:z:0%dense_2/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_2/MatMul�
dense_2/BiasAdd/ReadVariableOpReadVariableOp'dense_2_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype02 
dense_2/BiasAdd/ReadVariableOp�
dense_2/BiasAddBiasAdddense_2/MatMul:product:0&dense_2/BiasAdd/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dense_2/BiasAddq
dense_2/ReluReludense_2/BiasAdd:output:0*
T0*(
_output_shapes
:����������2
dense_2/Relu�
dropout_1/IdentityIdentitydense_2/Relu:activations:0*
T0*(
_output_shapes
:����������2
dropout_1/Identity�
.batch_normalization_2/batchnorm/ReadVariableOpReadVariableOp7batch_normalization_2_batchnorm_readvariableop_resource*
_output_shapes	
:�*
dtype020
.batch_normalization_2/batchnorm/ReadVariableOp�
%batch_normalization_2/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2'
%batch_normalization_2/batchnorm/add/y�
#batch_normalization_2/batchnorm/addAddV26batch_normalization_2/batchnorm/ReadVariableOp:value:0.batch_normalization_2/batchnorm/add/y:output:0*
T0*
_output_shapes	
:�2%
#batch_normalization_2/batchnorm/add�
%batch_normalization_2/batchnorm/RsqrtRsqrt'batch_normalization_2/batchnorm/add:z:0*
T0*
_output_shapes	
:�2'
%batch_normalization_2/batchnorm/Rsqrt�
2batch_normalization_2/batchnorm/mul/ReadVariableOpReadVariableOp;batch_normalization_2_batchnorm_mul_readvariableop_resource*
_output_shapes	
:�*
dtype024
2batch_normalization_2/batchnorm/mul/ReadVariableOp�
#batch_normalization_2/batchnorm/mulMul)batch_normalization_2/batchnorm/Rsqrt:y:0:batch_normalization_2/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes	
:�2%
#batch_normalization_2/batchnorm/mul�
%batch_normalization_2/batchnorm/mul_1Muldropout_1/Identity:output:0'batch_normalization_2/batchnorm/mul:z:0*
T0*(
_output_shapes
:����������2'
%batch_normalization_2/batchnorm/mul_1�
0batch_normalization_2/batchnorm/ReadVariableOp_1ReadVariableOp9batch_normalization_2_batchnorm_readvariableop_1_resource*
_output_shapes	
:�*
dtype022
0batch_normalization_2/batchnorm/ReadVariableOp_1�
%batch_normalization_2/batchnorm/mul_2Mul8batch_normalization_2/batchnorm/ReadVariableOp_1:value:0'batch_normalization_2/batchnorm/mul:z:0*
T0*
_output_shapes	
:�2'
%batch_normalization_2/batchnorm/mul_2�
0batch_normalization_2/batchnorm/ReadVariableOp_2ReadVariableOp9batch_normalization_2_batchnorm_readvariableop_2_resource*
_output_shapes	
:�*
dtype022
0batch_normalization_2/batchnorm/ReadVariableOp_2�
#batch_normalization_2/batchnorm/subSub8batch_normalization_2/batchnorm/ReadVariableOp_2:value:0)batch_normalization_2/batchnorm/mul_2:z:0*
T0*
_output_shapes	
:�2%
#batch_normalization_2/batchnorm/sub�
%batch_normalization_2/batchnorm/add_1AddV2)batch_normalization_2/batchnorm/mul_1:z:0'batch_normalization_2/batchnorm/sub:z:0*
T0*(
_output_shapes
:����������2'
%batch_normalization_2/batchnorm/add_1�
dense_3/MatMul/ReadVariableOpReadVariableOp&dense_3_matmul_readvariableop_resource*
_output_shapes
:	�@*
dtype02
dense_3/MatMul/ReadVariableOp�
dense_3/MatMulMatMul)batch_normalization_2/batchnorm/add_1:z:0%dense_3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_3/MatMul�
dense_3/BiasAdd/ReadVariableOpReadVariableOp'dense_3_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype02 
dense_3/BiasAdd/ReadVariableOp�
dense_3/BiasAddBiasAdddense_3/MatMul:product:0&dense_3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������@2
dense_3/BiasAddp
dense_3/ReluReludense_3/BiasAdd:output:0*
T0*'
_output_shapes
:���������@2
dense_3/Relu�
dropout_2/IdentityIdentitydense_3/Relu:activations:0*
T0*'
_output_shapes
:���������@2
dropout_2/Identity�
dense_4/MatMul/ReadVariableOpReadVariableOp&dense_4_matmul_readvariableop_resource*
_output_shapes

:@*
dtype02
dense_4/MatMul/ReadVariableOp�
dense_4/MatMulMatMuldropout_2/Identity:output:0%dense_4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dense_4/MatMul�
dense_4/BiasAdd/ReadVariableOpReadVariableOp'dense_4_biasadd_readvariableop_resource*
_output_shapes
:*
dtype02 
dense_4/BiasAdd/ReadVariableOp�
dense_4/BiasAddBiasAdddense_4/MatMul:product:0&dense_4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dense_4/BiasAddy
dense_4/SigmoidSigmoiddense_4/BiasAdd:output:0*
T0*'
_output_shapes
:���������2
dense_4/Sigmoidn
IdentityIdentitydense_4/Sigmoid:y:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�	
NoOpNoOp-^batch_normalization/batchnorm/ReadVariableOp/^batch_normalization/batchnorm/ReadVariableOp_1/^batch_normalization/batchnorm/ReadVariableOp_21^batch_normalization/batchnorm/mul/ReadVariableOp/^batch_normalization_1/batchnorm/ReadVariableOp1^batch_normalization_1/batchnorm/ReadVariableOp_11^batch_normalization_1/batchnorm/ReadVariableOp_23^batch_normalization_1/batchnorm/mul/ReadVariableOp/^batch_normalization_2/batchnorm/ReadVariableOp1^batch_normalization_2/batchnorm/ReadVariableOp_11^batch_normalization_2/batchnorm/ReadVariableOp_23^batch_normalization_2/batchnorm/mul/ReadVariableOp^dense/MatMul/ReadVariableOp^dense_1/BiasAdd/ReadVariableOp^dense_1/MatMul/ReadVariableOp^dense_2/BiasAdd/ReadVariableOp^dense_2/MatMul/ReadVariableOp^dense_3/BiasAdd/ReadVariableOp^dense_3/MatMul/ReadVariableOp^dense_4/BiasAdd/ReadVariableOp^dense_4/MatMul/ReadVariableOp^embedding/embedding_lookup^embedding_1/embedding_lookup8^integer_lookup/None_lookup_table_find/LookupTableFindV27^string_lookup/None_lookup_table_find/LookupTableFindV29^string_lookup_1/None_lookup_table_find/LookupTableFindV29^string_lookup_2/None_lookup_table_find/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������:���������:���������:����������:���������:���������:���������:���������: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2\
,batch_normalization/batchnorm/ReadVariableOp,batch_normalization/batchnorm/ReadVariableOp2`
.batch_normalization/batchnorm/ReadVariableOp_1.batch_normalization/batchnorm/ReadVariableOp_12`
.batch_normalization/batchnorm/ReadVariableOp_2.batch_normalization/batchnorm/ReadVariableOp_22d
0batch_normalization/batchnorm/mul/ReadVariableOp0batch_normalization/batchnorm/mul/ReadVariableOp2`
.batch_normalization_1/batchnorm/ReadVariableOp.batch_normalization_1/batchnorm/ReadVariableOp2d
0batch_normalization_1/batchnorm/ReadVariableOp_10batch_normalization_1/batchnorm/ReadVariableOp_12d
0batch_normalization_1/batchnorm/ReadVariableOp_20batch_normalization_1/batchnorm/ReadVariableOp_22h
2batch_normalization_1/batchnorm/mul/ReadVariableOp2batch_normalization_1/batchnorm/mul/ReadVariableOp2`
.batch_normalization_2/batchnorm/ReadVariableOp.batch_normalization_2/batchnorm/ReadVariableOp2d
0batch_normalization_2/batchnorm/ReadVariableOp_10batch_normalization_2/batchnorm/ReadVariableOp_12d
0batch_normalization_2/batchnorm/ReadVariableOp_20batch_normalization_2/batchnorm/ReadVariableOp_22h
2batch_normalization_2/batchnorm/mul/ReadVariableOp2batch_normalization_2/batchnorm/mul/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp2@
dense_1/BiasAdd/ReadVariableOpdense_1/BiasAdd/ReadVariableOp2>
dense_1/MatMul/ReadVariableOpdense_1/MatMul/ReadVariableOp2@
dense_2/BiasAdd/ReadVariableOpdense_2/BiasAdd/ReadVariableOp2>
dense_2/MatMul/ReadVariableOpdense_2/MatMul/ReadVariableOp2@
dense_3/BiasAdd/ReadVariableOpdense_3/BiasAdd/ReadVariableOp2>
dense_3/MatMul/ReadVariableOpdense_3/MatMul/ReadVariableOp2@
dense_4/BiasAdd/ReadVariableOpdense_4/BiasAdd/ReadVariableOp2>
dense_4/MatMul/ReadVariableOpdense_4/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup2<
embedding_1/embedding_lookupembedding_1/embedding_lookup2r
7integer_lookup/None_lookup_table_find/LookupTableFindV27integer_lookup/None_lookup_table_find/LookupTableFindV22p
6string_lookup/None_lookup_table_find/LookupTableFindV26string_lookup/None_lookup_table_find/LookupTableFindV22t
8string_lookup_1/None_lookup_table_find/LookupTableFindV28string_lookup_1/None_lookup_table_find/LookupTableFindV22t
8string_lookup_2/None_lookup_table_find/LookupTableFindV28string_lookup_2/None_lookup_table_find/LookupTableFindV2:O K
#
_output_shapes
:���������
$
_user_specified_name
inputs/age:RN
#
_output_shapes
:���������
'
_user_specified_nameinputs/gender:UQ
'
_output_shapes
:���������
&
_user_specified_nameinputs/genre:XT
(
_output_shapes
:����������
(
_user_specified_nameinputs/history:SO
#
_output_shapes
:���������
(
_user_specified_nameinputs/item_id:VR
#
_output_shapes
:���������
+
_user_specified_nameinputs/occupation:XT
#
_output_shapes
:���������
-
_user_specified_nameinputs/release_date:TP
#
_output_shapes
:���������
)
_user_specified_nameinputs/zip_code:	

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: "�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
/
age(
serving_default_age:0���������
5
gender+
serving_default_gender:0���������
7
genre.
serving_default_genre:0���������
<
history1
serving_default_history:0����������
7
item_id,
serving_default_item_id:0���������
=

occupation/
serving_default_occupation:0���������
A
release_date1
serving_default_release_date:0���������
9
zip_code-
serving_default_zip_code:0���������;
dense_40
StatefulPartitionedCall:0���������tensorflow/serving/predict:��
�
layer-0
layer-1
layer-2
layer-3
layer-4
layer-5
layer_with_weights-0
layer-6
layer-7
	layer-8

layer_with_weights-1

layer-9
layer-10
layer_with_weights-2
layer-11
layer-12
layer_with_weights-3
layer-13
layer_with_weights-4
layer-14
layer_with_weights-5
layer-15
layer-16
layer_with_weights-6
layer-17
layer-18
layer-19
layer_with_weights-7
layer-20
layer_with_weights-8
layer-21
layer-22
layer_with_weights-9
layer-23
layer_with_weights-10
layer-24
layer-25
layer_with_weights-11
layer-26
layer_with_weights-12
layer-27
layer-28
layer_with_weights-13
layer-29
	optimizer
 	variables
!trainable_variables
"regularization_losses
#	keras_api
$
signatures
�_default_save_signature
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_network
"
_tf_keras_input_layer
"
_tf_keras_input_layer
"
_tf_keras_input_layer
"
_tf_keras_input_layer
(
%	keras_api"
_tf_keras_layer
(
&	keras_api"
_tf_keras_layer
I
'state_variables

(_table
)	keras_api"
_tf_keras_layer
"
_tf_keras_input_layer
"
_tf_keras_input_layer
I
*state_variables

+_table
,	keras_api"
_tf_keras_layer
"
_tf_keras_input_layer
I
-state_variables

._table
/	keras_api"
_tf_keras_layer
"
_tf_keras_input_layer
I
0state_variables

1_table
2	keras_api"
_tf_keras_layer
�
3
embeddings
4	variables
5regularization_losses
6trainable_variables
7	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

8kernel
9	variables
:regularization_losses
;trainable_variables
<	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
(
=	keras_api"
_tf_keras_layer
�
>
embeddings
?	variables
@regularization_losses
Atrainable_variables
B	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
(
C	keras_api"
_tf_keras_layer
�
D	variables
Eregularization_losses
Ftrainable_variables
G	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�
Haxis
	Igamma
Jbeta
Kmoving_mean
Lmoving_variance
M	variables
Nregularization_losses
Otrainable_variables
P	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

Qkernel
Rbias
S	variables
Tregularization_losses
Utrainable_variables
V	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�
W	variables
Xregularization_losses
Ytrainable_variables
Z	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�
[axis
	\gamma
]beta
^moving_mean
_moving_variance
`	variables
aregularization_losses
btrainable_variables
c	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

dkernel
ebias
f	variables
gregularization_losses
htrainable_variables
i	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�
j	variables
kregularization_losses
ltrainable_variables
m	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�
naxis
	ogamma
pbeta
qmoving_mean
rmoving_variance
s	variables
tregularization_losses
utrainable_variables
v	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�

wkernel
xbias
y	variables
zregularization_losses
{trainable_variables
|	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�
}	variables
~regularization_losses
trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�
�kernel
	�bias
�	variables
�regularization_losses
�trainable_variables
�	keras_api
+�&call_and_return_all_conditional_losses
�__call__"
_tf_keras_layer
�
	�iter
�beta_1
�beta_2

�decay
�learning_rate3m�8m�>m�Im�Jm�Qm�Rm�\m�]m�dm�em�om�pm�wm�xm�	�m�	�m�3v�8v�>v�Iv�Jv�Qv�Rv�\v�]v�dv�ev�ov�pv�wv�xv�	�v�	�v�"
tf_deprecated_optimizer
�
34
85
>6
I7
J8
K9
L10
Q11
R12
\13
]14
^15
_16
d17
e18
o19
p20
q21
r22
w23
x24
�25
�26"
trackable_list_wrapper
�
30
81
>2
I3
J4
Q5
R6
\7
]8
d9
e10
o11
p12
w13
x14
�15
�16"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
 	variables
�metrics
�layer_metrics
!trainable_variables
"regularization_losses
�layers
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
"
_generic_user_object
"
_generic_user_object
 "
trackable_dict_wrapper
T
�_create_resource
�_initialize
�_destroy_resourceR Z
table��
"
_generic_user_object
 "
trackable_dict_wrapper
T
�_create_resource
�_initialize
�_destroy_resourceR Z
table��
"
_generic_user_object
 "
trackable_dict_wrapper
T
�_create_resource
�_initialize
�_destroy_resourceR Z
table��
"
_generic_user_object
 "
trackable_dict_wrapper
T
�_create_resource
�_initialize
�_destroy_resourceR Z
table��
"
_generic_user_object
':%	�2embedding/embeddings
'
30"
trackable_list_wrapper
 "
trackable_list_wrapper
'
30"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
4	variables
�metrics
5regularization_losses
6trainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	�)2dense/kernel
'
80"
trackable_list_wrapper
 "
trackable_list_wrapper
'
80"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
9	variables
�metrics
:regularization_losses
;trainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
):'	�)2embedding_1/embeddings
'
>0"
trackable_list_wrapper
 "
trackable_list_wrapper
'
>0"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
?	variables
�metrics
@regularization_losses
Atrainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
D	variables
�metrics
Eregularization_losses
Ftrainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
(:&�2batch_normalization/gamma
':%�2batch_normalization/beta
0:.� (2batch_normalization/moving_mean
4:2� (2#batch_normalization/moving_variance
<
I0
J1
K2
L3"
trackable_list_wrapper
 "
trackable_list_wrapper
.
I0
J1"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
M	variables
�metrics
Nregularization_losses
Otrainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
": 
��2dense_1/kernel
:�2dense_1/bias
.
Q0
R1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
Q0
R1"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
S	variables
�metrics
Tregularization_losses
Utrainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
W	variables
�metrics
Xregularization_losses
Ytrainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
*:(�2batch_normalization_1/gamma
):'�2batch_normalization_1/beta
2:0� (2!batch_normalization_1/moving_mean
6:4� (2%batch_normalization_1/moving_variance
<
\0
]1
^2
_3"
trackable_list_wrapper
 "
trackable_list_wrapper
.
\0
]1"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
`	variables
�metrics
aregularization_losses
btrainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
": 
��2dense_2/kernel
:�2dense_2/bias
.
d0
e1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
d0
e1"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
f	variables
�metrics
gregularization_losses
htrainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
j	variables
�metrics
kregularization_losses
ltrainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
*:(�2batch_normalization_2/gamma
):'�2batch_normalization_2/beta
2:0� (2!batch_normalization_2/moving_mean
6:4� (2%batch_normalization_2/moving_variance
<
o0
p1
q2
r3"
trackable_list_wrapper
 "
trackable_list_wrapper
.
o0
p1"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
s	variables
�metrics
tregularization_losses
utrainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
!:	�@2dense_3/kernel
:@2dense_3/bias
.
w0
x1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
w0
x1"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
y	variables
�metrics
zregularization_losses
{trainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
}	variables
�metrics
~regularization_losses
trainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 :@2dense_4/kernel
:2dense_4/bias
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
�
�non_trainable_variables
 �layer_regularization_losses
�	variables
�metrics
�regularization_losses
�trainable_variables
�layer_metrics
�layers
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
J
K4
L5
^6
_7
q8
r9"
trackable_list_wrapper
 "
trackable_list_wrapper
8
�0
�1
�2"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
K0
L1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
^0
_1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
q0
r1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
R

�total

�count
�	variables
�	keras_api"
_tf_keras_metric
c

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"
_tf_keras_metric
�
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api"
_tf_keras_metric
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:� (2true_positives
:� (2true_negatives
 :� (2false_positives
 :� (2false_negatives
@
�0
�1
�2
�3"
trackable_list_wrapper
.
�	variables"
_generic_user_object
,:*	�2Adam/embedding/embeddings/m
$:"	�)2Adam/dense/kernel/m
.:,	�)2Adam/embedding_1/embeddings/m
-:+�2 Adam/batch_normalization/gamma/m
,:*�2Adam/batch_normalization/beta/m
':%
��2Adam/dense_1/kernel/m
 :�2Adam/dense_1/bias/m
/:-�2"Adam/batch_normalization_1/gamma/m
.:,�2!Adam/batch_normalization_1/beta/m
':%
��2Adam/dense_2/kernel/m
 :�2Adam/dense_2/bias/m
/:-�2"Adam/batch_normalization_2/gamma/m
.:,�2!Adam/batch_normalization_2/beta/m
&:$	�@2Adam/dense_3/kernel/m
:@2Adam/dense_3/bias/m
%:#@2Adam/dense_4/kernel/m
:2Adam/dense_4/bias/m
,:*	�2Adam/embedding/embeddings/v
$:"	�)2Adam/dense/kernel/v
.:,	�)2Adam/embedding_1/embeddings/v
-:+�2 Adam/batch_normalization/gamma/v
,:*�2Adam/batch_normalization/beta/v
':%
��2Adam/dense_1/kernel/v
 :�2Adam/dense_1/bias/v
/:-�2"Adam/batch_normalization_1/gamma/v
.:,�2!Adam/batch_normalization_1/beta/v
':%
��2Adam/dense_2/kernel/v
 :�2Adam/dense_2/bias/v
/:-�2"Adam/batch_normalization_2/gamma/v
.:,�2!Adam/batch_normalization_2/beta/v
&:$	�@2Adam/dense_3/kernel/v
:@2Adam/dense_3/bias/v
%:#@2Adam/dense_4/kernel/v
:2Adam/dense_4/bias/v
�2�
 __inference__wrapped_model_50206�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *���
���
 
age�
age���������
&
gender�
gender���������
(
genre�
genre���������
-
history"�
history����������
(
item_id�
item_id���������
.

occupation �

occupation���������
2
release_date"�
release_date���������
*
zip_code�
zip_code���������
�2�
?__inference_deep_layer_call_and_return_conditional_losses_52000
?__inference_deep_layer_call_and_return_conditional_losses_52225
?__inference_deep_layer_call_and_return_conditional_losses_51628
?__inference_deep_layer_call_and_return_conditional_losses_51756�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
$__inference_deep_layer_call_fn_51004
$__inference_deep_layer_call_fn_52299
$__inference_deep_layer_call_fn_52373
$__inference_deep_layer_call_fn_51500�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
D__inference_embedding_layer_call_and_return_conditional_losses_52382�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_embedding_layer_call_fn_52389�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
@__inference_dense_layer_call_and_return_conditional_losses_52396�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
%__inference_dense_layer_call_fn_52403�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
F__inference_embedding_1_layer_call_and_return_conditional_losses_52412�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
+__inference_embedding_1_layer_call_fn_52419�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
F__inference_concatenate_layer_call_and_return_conditional_losses_52432�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
+__inference_concatenate_layer_call_fn_52444�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
N__inference_batch_normalization_layer_call_and_return_conditional_losses_52464
N__inference_batch_normalization_layer_call_and_return_conditional_losses_52498�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
3__inference_batch_normalization_layer_call_fn_52511
3__inference_batch_normalization_layer_call_fn_52524�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
B__inference_dense_1_layer_call_and_return_conditional_losses_52535�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_dense_1_layer_call_fn_52544�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_dropout_layer_call_and_return_conditional_losses_52549
B__inference_dropout_layer_call_and_return_conditional_losses_52561�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
'__inference_dropout_layer_call_fn_52566
'__inference_dropout_layer_call_fn_52571�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_52591
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_52625�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
5__inference_batch_normalization_1_layer_call_fn_52638
5__inference_batch_normalization_1_layer_call_fn_52651�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
B__inference_dense_2_layer_call_and_return_conditional_losses_52662�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_dense_2_layer_call_fn_52671�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_dropout_1_layer_call_and_return_conditional_losses_52676
D__inference_dropout_1_layer_call_and_return_conditional_losses_52688�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
)__inference_dropout_1_layer_call_fn_52693
)__inference_dropout_1_layer_call_fn_52698�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_52718
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_52752�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
5__inference_batch_normalization_2_layer_call_fn_52765
5__inference_batch_normalization_2_layer_call_fn_52778�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
B__inference_dense_3_layer_call_and_return_conditional_losses_52789�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_dense_3_layer_call_fn_52798�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_dropout_2_layer_call_and_return_conditional_losses_52803
D__inference_dropout_2_layer_call_and_return_conditional_losses_52815�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
)__inference_dropout_2_layer_call_fn_52820
)__inference_dropout_2_layer_call_fn_52825�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
B__inference_dense_4_layer_call_and_return_conditional_losses_52836�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_dense_4_layer_call_fn_52845�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference_signature_wrapper_51838agegendergenrehistoryitem_id
occupationrelease_datezip_code"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
__inference__creator_52850�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__initializer_52855�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__destroyer_52860�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_save_fn_52924checkpoint_key"�
���
FullArgSpec
args�
jcheckpoint_key
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�	
� 
�B�
__inference_restore_fn_52932restored_tensors_0restored_tensors_1"�
���
FullArgSpec
args� 
varargsjrestored_tensors
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
	�	
�2�
__inference__creator_52865�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__initializer_52870�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__destroyer_52875�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_save_fn_52951checkpoint_key"�
���
FullArgSpec
args�
jcheckpoint_key
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�	
� 
�B�
__inference_restore_fn_52959restored_tensors_0restored_tensors_1"�
���
FullArgSpec
args� 
varargsjrestored_tensors
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�	
	�	
�2�
__inference__creator_52880�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__initializer_52885�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__destroyer_52890�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_save_fn_52978checkpoint_key"�
���
FullArgSpec
args�
jcheckpoint_key
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�	
� 
�B�
__inference_restore_fn_52986restored_tensors_0restored_tensors_1"�
���
FullArgSpec
args� 
varargsjrestored_tensors
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
	�	
�2�
__inference__creator_52895�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__initializer_52900�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__destroyer_52905�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_save_fn_53005checkpoint_key"�
���
FullArgSpec
args�
jcheckpoint_key
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�	
� 
�B�
__inference_restore_fn_53013restored_tensors_0restored_tensors_1"�
���
FullArgSpec
args� 
varargsjrestored_tensors
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
	�	
	J
Const
J	
Const_1
J	
Const_2
J	
Const_36
__inference__creator_52850�

� 
� "� 6
__inference__creator_52865�

� 
� "� 6
__inference__creator_52880�

� 
� "� 6
__inference__creator_52895�

� 
� "� 8
__inference__destroyer_52860�

� 
� "� 8
__inference__destroyer_52875�

� 
� "� 8
__inference__destroyer_52890�

� 
� "� 8
__inference__destroyer_52905�

� 
� "� :
__inference__initializer_52855�

� 
� "� :
__inference__initializer_52870�

� 
� "� :
__inference__initializer_52885�

� 
� "� :
__inference__initializer_52900�

� 
� "� �
 __inference__wrapped_model_50206�%+�(�.�1�38>LIKJQR_\^]deroqpwx�����
���
���
 
age�
age���������
&
gender�
gender���������
(
genre�
genre���������
-
history"�
history����������
(
item_id�
item_id���������
.

occupation �

occupation���������
2
release_date"�
release_date���������
*
zip_code�
zip_code���������
� "1�.
,
dense_4!�
dense_4����������
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_52591d_\^]4�1
*�'
!�
inputs����������
p 
� "&�#
�
0����������
� �
P__inference_batch_normalization_1_layer_call_and_return_conditional_losses_52625d^_\]4�1
*�'
!�
inputs����������
p
� "&�#
�
0����������
� �
5__inference_batch_normalization_1_layer_call_fn_52638W_\^]4�1
*�'
!�
inputs����������
p 
� "������������
5__inference_batch_normalization_1_layer_call_fn_52651W^_\]4�1
*�'
!�
inputs����������
p
� "������������
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_52718droqp4�1
*�'
!�
inputs����������
p 
� "&�#
�
0����������
� �
P__inference_batch_normalization_2_layer_call_and_return_conditional_losses_52752dqrop4�1
*�'
!�
inputs����������
p
� "&�#
�
0����������
� �
5__inference_batch_normalization_2_layer_call_fn_52765Wroqp4�1
*�'
!�
inputs����������
p 
� "������������
5__inference_batch_normalization_2_layer_call_fn_52778Wqrop4�1
*�'
!�
inputs����������
p
� "������������
N__inference_batch_normalization_layer_call_and_return_conditional_losses_52464dLIKJ4�1
*�'
!�
inputs����������
p 
� "&�#
�
0����������
� �
N__inference_batch_normalization_layer_call_and_return_conditional_losses_52498dKLIJ4�1
*�'
!�
inputs����������
p
� "&�#
�
0����������
� �
3__inference_batch_normalization_layer_call_fn_52511WLIKJ4�1
*�'
!�
inputs����������
p 
� "������������
3__inference_batch_normalization_layer_call_fn_52524WKLIJ4�1
*�'
!�
inputs����������
p
� "������������
F__inference_concatenate_layer_call_and_return_conditional_losses_52432����
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
"�
inputs/4���������)
"�
inputs/5���������
"�
inputs/6���������)
"�
inputs/7���������
� "&�#
�
0����������
� �
+__inference_concatenate_layer_call_fn_52444����
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
"�
inputs/4���������)
"�
inputs/5���������
"�
inputs/6���������)
"�
inputs/7���������
� "������������
?__inference_deep_layer_call_and_return_conditional_losses_51628�%+�(�.�1�38>LIKJQR_\^]deroqpwx�����
���
���
 
age�
age���������
&
gender�
gender���������
(
genre�
genre���������
-
history"�
history����������
(
item_id�
item_id���������
.

occupation �

occupation���������
2
release_date"�
release_date���������
*
zip_code�
zip_code���������
p 

 
� "%�"
�
0���������
� �
?__inference_deep_layer_call_and_return_conditional_losses_51756�%+�(�.�1�38>KLIJQR^_\]deqropwx�����
���
���
 
age�
age���������
&
gender�
gender���������
(
genre�
genre���������
-
history"�
history����������
(
item_id�
item_id���������
.

occupation �

occupation���������
2
release_date"�
release_date���������
*
zip_code�
zip_code���������
p

 
� "%�"
�
0���������
� �
?__inference_deep_layer_call_and_return_conditional_losses_52000�%+�(�.�1�38>LIKJQR_\^]deroqpwx�����
���
���
'
age �

inputs/age���������
-
gender#� 
inputs/gender���������
/
genre&�#
inputs/genre���������
4
history)�&
inputs/history����������
/
item_id$�!
inputs/item_id���������
5

occupation'�$
inputs/occupation���������
9
release_date)�&
inputs/release_date���������
1
zip_code%�"
inputs/zip_code���������
p 

 
� "%�"
�
0���������
� �
?__inference_deep_layer_call_and_return_conditional_losses_52225�%+�(�.�1�38>KLIJQR^_\]deqropwx�����
���
���
'
age �

inputs/age���������
-
gender#� 
inputs/gender���������
/
genre&�#
inputs/genre���������
4
history)�&
inputs/history����������
/
item_id$�!
inputs/item_id���������
5

occupation'�$
inputs/occupation���������
9
release_date)�&
inputs/release_date���������
1
zip_code%�"
inputs/zip_code���������
p

 
� "%�"
�
0���������
� �
$__inference_deep_layer_call_fn_51004�%+�(�.�1�38>LIKJQR_\^]deroqpwx�����
���
���
 
age�
age���������
&
gender�
gender���������
(
genre�
genre���������
-
history"�
history����������
(
item_id�
item_id���������
.

occupation �

occupation���������
2
release_date"�
release_date���������
*
zip_code�
zip_code���������
p 

 
� "�����������
$__inference_deep_layer_call_fn_51500�%+�(�.�1�38>KLIJQR^_\]deqropwx�����
���
���
 
age�
age���������
&
gender�
gender���������
(
genre�
genre���������
-
history"�
history����������
(
item_id�
item_id���������
.

occupation �

occupation���������
2
release_date"�
release_date���������
*
zip_code�
zip_code���������
p

 
� "�����������
$__inference_deep_layer_call_fn_52299�%+�(�.�1�38>LIKJQR_\^]deroqpwx�����
���
���
'
age �

inputs/age���������
-
gender#� 
inputs/gender���������
/
genre&�#
inputs/genre���������
4
history)�&
inputs/history����������
/
item_id$�!
inputs/item_id���������
5

occupation'�$
inputs/occupation���������
9
release_date)�&
inputs/release_date���������
1
zip_code%�"
inputs/zip_code���������
p 

 
� "�����������
$__inference_deep_layer_call_fn_52373�%+�(�.�1�38>KLIJQR^_\]deqropwx�����
���
���
'
age �

inputs/age���������
-
gender#� 
inputs/gender���������
/
genre&�#
inputs/genre���������
4
history)�&
inputs/history����������
/
item_id$�!
inputs/item_id���������
5

occupation'�$
inputs/occupation���������
9
release_date)�&
inputs/release_date���������
1
zip_code%�"
inputs/zip_code���������
p

 
� "�����������
B__inference_dense_1_layer_call_and_return_conditional_losses_52535^QR0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� |
'__inference_dense_1_layer_call_fn_52544QQR0�-
&�#
!�
inputs����������
� "������������
B__inference_dense_2_layer_call_and_return_conditional_losses_52662^de0�-
&�#
!�
inputs����������
� "&�#
�
0����������
� |
'__inference_dense_2_layer_call_fn_52671Qde0�-
&�#
!�
inputs����������
� "������������
B__inference_dense_3_layer_call_and_return_conditional_losses_52789]wx0�-
&�#
!�
inputs����������
� "%�"
�
0���������@
� {
'__inference_dense_3_layer_call_fn_52798Pwx0�-
&�#
!�
inputs����������
� "����������@�
B__inference_dense_4_layer_call_and_return_conditional_losses_52836^��/�,
%�"
 �
inputs���������@
� "%�"
�
0���������
� |
'__inference_dense_4_layer_call_fn_52845Q��/�,
%�"
 �
inputs���������@
� "�����������
@__inference_dense_layer_call_and_return_conditional_losses_52396\80�-
&�#
!�
inputs����������
� "%�"
�
0���������)
� x
%__inference_dense_layer_call_fn_52403O80�-
&�#
!�
inputs����������
� "����������)�
D__inference_dropout_1_layer_call_and_return_conditional_losses_52676^4�1
*�'
!�
inputs����������
p 
� "&�#
�
0����������
� �
D__inference_dropout_1_layer_call_and_return_conditional_losses_52688^4�1
*�'
!�
inputs����������
p
� "&�#
�
0����������
� ~
)__inference_dropout_1_layer_call_fn_52693Q4�1
*�'
!�
inputs����������
p 
� "�����������~
)__inference_dropout_1_layer_call_fn_52698Q4�1
*�'
!�
inputs����������
p
� "������������
D__inference_dropout_2_layer_call_and_return_conditional_losses_52803\3�0
)�&
 �
inputs���������@
p 
� "%�"
�
0���������@
� �
D__inference_dropout_2_layer_call_and_return_conditional_losses_52815\3�0
)�&
 �
inputs���������@
p
� "%�"
�
0���������@
� |
)__inference_dropout_2_layer_call_fn_52820O3�0
)�&
 �
inputs���������@
p 
� "����������@|
)__inference_dropout_2_layer_call_fn_52825O3�0
)�&
 �
inputs���������@
p
� "����������@�
B__inference_dropout_layer_call_and_return_conditional_losses_52549^4�1
*�'
!�
inputs����������
p 
� "&�#
�
0����������
� �
B__inference_dropout_layer_call_and_return_conditional_losses_52561^4�1
*�'
!�
inputs����������
p
� "&�#
�
0����������
� |
'__inference_dropout_layer_call_fn_52566Q4�1
*�'
!�
inputs����������
p 
� "�����������|
'__inference_dropout_layer_call_fn_52571Q4�1
*�'
!�
inputs����������
p
� "������������
F__inference_embedding_1_layer_call_and_return_conditional_losses_52412W>+�(
!�
�
inputs���������	
� "%�"
�
0���������)
� y
+__inference_embedding_1_layer_call_fn_52419J>+�(
!�
�
inputs���������	
� "����������)�
D__inference_embedding_layer_call_and_return_conditional_losses_52382W3+�(
!�
�
inputs���������	
� "%�"
�
0���������
� w
)__inference_embedding_layer_call_fn_52389J3+�(
!�
�
inputs���������	
� "����������y
__inference_restore_fn_52932Y(K�H
A�>
�
restored_tensors_0
�
restored_tensors_1	
� "� y
__inference_restore_fn_52959Y+K�H
A�>
�
restored_tensors_0	
�
restored_tensors_1	
� "� y
__inference_restore_fn_52986Y.K�H
A�>
�
restored_tensors_0
�
restored_tensors_1	
� "� y
__inference_restore_fn_53013Y1K�H
A�>
�
restored_tensors_0
�
restored_tensors_1	
� "� �
__inference_save_fn_52924�(&�#
�
�
checkpoint_key 
� "���
`�]

name�
0/name 
#

slice_spec�
0/slice_spec 

tensor�
0/tensor
`�]

name�
1/name 
#

slice_spec�
1/slice_spec 

tensor�
1/tensor	�
__inference_save_fn_52951�+&�#
�
�
checkpoint_key 
� "���
`�]

name�
0/name 
#

slice_spec�
0/slice_spec 

tensor�
0/tensor	
`�]

name�
1/name 
#

slice_spec�
1/slice_spec 

tensor�
1/tensor	�
__inference_save_fn_52978�.&�#
�
�
checkpoint_key 
� "���
`�]

name�
0/name 
#

slice_spec�
0/slice_spec 

tensor�
0/tensor
`�]

name�
1/name 
#

slice_spec�
1/slice_spec 

tensor�
1/tensor	�
__inference_save_fn_53005�1&�#
�
�
checkpoint_key 
� "���
`�]

name�
0/name 
#

slice_spec�
0/slice_spec 

tensor�
0/tensor
`�]

name�
1/name 
#

slice_spec�
1/slice_spec 

tensor�
1/tensor	�
#__inference_signature_wrapper_51838�%+�(�.�1�38>LIKJQR_\^]deroqpwx�����
� 
���
 
age�
age���������
&
gender�
gender���������
(
genre�
genre���������
-
history"�
history����������
(
item_id�
item_id���������
.

occupation �

occupation���������
2
release_date"�
release_date���������
*
zip_code�
zip_code���������"1�.
,
dense_4!�
dense_4���������