# NI-MVI SEMESTRÁLKA - CHECKPOINT

## DEEP & WIDE RECOMMENDATION

Juraj Kmec

---

## Zadanie

Moje zadanie je Deep & Wide Recommendations.
Jedná sa o architektúru neurónových sietí navrhnutú Googlom v článku <https://arxiv.org/pdf/1606.07792.pdf>. Táto architektúra má využitie hlavne v odporúčacích systémoch.  
Zadanie spočíva v zoznámení sa s algoritmom, jeho implementovaní a zhodnotení jeho výkonu na vybranom datasete.

## Prečítané články

Prečítal som vyššie spomínaný hlavný článok od Googlu, kde tento model navrhli. Okrem toho som prečítal aj priložený článok od NVIDIe (<https://developer.nvidia.com/blog/accelerating-wide-deep-recommender-inference-on-gpus/>), kde popisujú zrýchlenie predikcií Deep & Wide modelu za pomoci grafických kariet.

Ďalej som prečítal blogpost od Googlu na rovnakú tému (<https://ai.googleblog.com/2016/06/wide-deep-learning-better-together-with.html>), viaceré príspevky o odporúčacích systémov, najmä z oficiálnych zdrojov TensorFlow a Keras (<https://keras.io/examples/structured_data/wide_deep_cross_networks/> alebo články z <https://www.tensorflow.org/recommenders/examples/quickstart>).  

Tiež som sa zoznámil s princípmi TensorFlow a Keras API.

## Doterajšia práca a výsledky

Do milestone-u sa mi podarilo si zvoliť dataset pre testovanie modelu - MovieLens 100K Dataset (<https://grouplens.org/datasets/movielens/100k/>).

Tento dataset obsahuje 100 000 hodnotení filmov od približne 1000 ľudí a bežne sa používa na benchmarking odporúčacích systémov. Dáta obsahujú základné údaje o používateľoch aj o filmoch a jednotlivé hodnotenia.

Keďže hodnotenia sú v rozmedzí 1 - 5 hviezdičiek, tieto hodnoty som binarizoval tak, že 1 až 3 hviezdičky som nahradil hodnotou 0 a 4 až 5 hviezdičiek som nahradil hodnotou 1, keďže aj Google vo svojom článku skúmajú binárnu klasifikáciu.

Po rozdelení dát na trénovaciu, testovaciu a validačnú množinu som ku každému hodnoteniu ešte pripísal históriu hodnotení daného používateľa z trénovacej množiny ako multi-hot-encoded stĺpce. Teda na vstupe nie sú len údaje o filme a používateľovi, ale aj to, ktoré filmy sa mu páčili. Dôležité je, že do tejto histórie sa berú len filmy z trénovacej množiny, a to aj pri validačnej a testovacej množine. Inak by sa do trénovacej množiny dostali informácie z testovacej množiny.

Nad takto upraveným datasetom som potom postavil "hlboký" a "široký" model po vzoru článku od Googlu.  

Hlboký model má na vstupe všetky použiteľné informácie o filme aj používateľovi, ktoré sú ešte štandardne spracované (kategorické premenné používajú buď one-hot-encoding alebo embedding). Tento model je klasická dopredná neurónka s 3 ReLU, Batch Normalization a Dropout vrstvami, na výstupe je sigmoida ako pravdepodobnosť.  

Široký model obsahuje "Cross Product" transformáciu, tak ako ju popisujú v článku. Na vstupe má index veci (kategorická premenná) a históriu hodnotení používateľa (multi-hot-encoding), na výstupe má široký a riedky vektor, ktorý v zásade realizuje kartézsky súčin a funkciu AND. Výstup celého modelu je lineárna kombinácia dimenzií tohoto širokého vektoru a následná aplikácia sigmoidy. Zatiaľ sa mi nepodarilo tento model urobiť efektívne (tzn. aby používal SparseTensor miesto bežného Tensoru), takže je pomerne náročný na pamäť.

Oba modely dávajú na testovacej množine približne 70% presnosť, hlboký model je o trochu lepší. Pred tým, než ich skombinujem do jedného a dostaneme Deep & Wide model, mám v pláne zefektívniť výkon širokého modelu.

Predspracovanie dát je v notebooku `process_data.ipynb`, aktuálna verzia modelov je v `models.ipynb`. Tiež prikladám obrázky modelov získané s `plot_model()` funkciou a použité datasety.