import json

import tensorflow as tf

# Load the vocabulary (for embedding purposes).
with open('vocabulary.json') as file:
    VOCABULARY = json.load(file)
NUM_ITEMS = len(VOCABULARY['item_id'])

# Define different column groups (which will be processed differently).
TARGET_COL_NAME = 'target'
NUMERIC_COL_NAMES = {'age', 'release_date'}
CATEGORICAL_STRING_COL_NAMES = {'gender', 'occupation', 'zip_code'}
CATEGORICAL_INT_COL_NAMES = {'item_id'}
CATEGORICAL_COL_NAMES = CATEGORICAL_STRING_COL_NAMES | CATEGORICAL_INT_COL_NAMES
MULTI_HOT_COL_NAMES = {'genre', 'history'}
# MULTI_HOT_DENSE_COL_NAMES = {'genre'}
# MULTI_HOT_SPARSE_COL_NAMES = {'history'}
ALL_COL_NAMES = (NUMERIC_COL_NAMES
                 | CATEGORICAL_COL_NAMES
                 | MULTI_HOT_COL_NAMES)

# Define columns that logically belong together (they will be merged into a single tensor).
GENRE_COL_NAMES = {
    'unknown',
    'Action',
    'Adventure',
    'Animation',
    "Children's",
    'Comedy',
    'Crime',
    'Documentary',
    'Drama',
    'Fantasy',
    'Film-Noir',
    'Horror',
    'Musical',
    'Mystery',
    'Romance',
    'Sci-Fi',
    'Thriller',
    'War',
    'Western'
}
HISTORY_COL_NAMES = {
    f'liked_item_{i}' for i in range(NUM_ITEMS)
}
MULTI_HOT_COLS_WIDTH = {
    'genre': len(GENRE_COL_NAMES),
    'history': len(HISTORY_COL_NAMES)
}

# Define which columns will use an embedding and which will simply be encoded.
TO_EMBED_COL_NAMES = {'item_id', 'zip_code', 'history'}
TO_ONE_HOT_COL_NAMES = {'gender', 'occupation'}

# Utility function for reading the CSV datasets and merging columns.
def read_and_map_csv(filename, batch_size):
    def merge_multi_hot_columns(features, label):
        features['genre'] = tf.stack(
            [features[genre] for genre in GENRE_COL_NAMES], axis=1
        )
        features['history'] = tf.stack(
            [features[item] for item in HISTORY_COL_NAMES], axis=1
        )
        return {
            col: tensor
            for col, tensor in features.items()
            if col in ALL_COL_NAMES
        }, label

    dataset = tf.data.experimental.make_csv_dataset(
        filename, batch_size, label_name=TARGET_COL_NAME
    )
    dataset = dataset.map(
        merge_multi_hot_columns, num_parallel_calls=tf.data.AUTOTUNE, deterministic=False
    )
    return dataset.cache()
